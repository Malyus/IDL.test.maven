package com.art.idl.datetime;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class RandomHeight {
    public static String generateRandomFt(){
        String randomFt = "";
        Integer min = 3;
        Integer max = 5;
        Integer randomNumber = min + (int)(Math.random() * max);//ft between 3-7
        randomFt = randomNumber.toString();
        return randomFt;
    }
    public static String generateRandomIn(){
        String randomIn = "";
        Integer min = 0;
        Integer max = 12;
        Integer randomNumber = min + (int)(Math.random() * max);//in between 0-11
        randomIn = randomNumber.toString();
        return randomIn;
    }
}
