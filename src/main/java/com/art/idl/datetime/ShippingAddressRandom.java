package com.art.idl.datetime;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class ShippingAddressRandom {
    static String finland = "FI";
    static String hongKong = "HK";
    static String hungary = "HU";
    static String ireland =  "IE";
    static String italy = "IT";
    static String kyivState = "Kyiv";
    static String zaporishkaState = "Zaporizhia";
    static String dniproState = "Dnipro";
    static String ivanoFrankivskState = "Ivano-Frankivs’ka Oblast’";


    public static String getRandomCountry(){
        String country = "";
        Integer min = 1;
        Integer max = 5;
        Integer randomNum = min + (int)(Math.random() * max);
        if(randomNum.intValue()==1){
            country = finland;
            System.out.println("Finland is chosen");
        }
        else if(randomNum.intValue()==2){
            country = hongKong;
            System.out.println("HongKong is chosen");
        }
        else if(randomNum.intValue()==3){
            country = hungary;
            System.out.println("Hungary is chosen");
        }
        else if(randomNum.intValue()==4){
            country = ireland;
            System.out.println("Ireland is chosen");
        }
        else if(randomNum.intValue()==5){
            country = italy;
            System.out.println("Italy is chosen");
        }
        return country;
    }
    public static String getRandomState(){
        String state = "";
        Integer min = 1;
        Integer max = 4;
        Integer randomNum = min + (int)(Math.random() * max);
        if(randomNum.intValue()==1){
            state = kyivState;
            System.out.println("KyivState is chosen");
        }
        else if(randomNum.intValue()==2){
            state = zaporishkaState;
            System.out.println("ZaporishskaState is chosen");
        }
        else if(randomNum.intValue()==3){
            state = dniproState;
            System.out.println("DniproState is chosen");
        }
        else if(randomNum.intValue()==4){
            state = ivanoFrankivskState;
            System.out.println("IvanoFrankivskState is chosen");
        }

        return state;
    }
    public static String getRandomZCode(){
        Integer min = 10;
        Integer max = 99;
        Integer randomNum = min + (int)(Math.random()* max);
        return randomNum.toString();
    }

}
