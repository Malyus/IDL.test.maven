package com.art.idl.datetime;

import java.lang.reflect.Field;
import java.util.Set;

/**
 * Created by M.Malyus on 12/6/2017.
 */
class Injector {
    public static void inject(Object instance) {
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(CustomParameterAnnotation.class)) {
                CustomParameterAnnotation set = field.getAnnotation(CustomParameterAnnotation.class);
                field.setAccessible(true); // should work on private fields
                try {
                    field.set(instance, set.value());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
