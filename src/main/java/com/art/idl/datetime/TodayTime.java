package com.art.idl.datetime;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by M.Malyus on 11/23/2017.
 */
public class TodayTime {
    int year;
    final static int staticYear = 2000;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public static int getByStaticYear(){
        return staticYear;
    }


    public String getTodayTime(){
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM,d,YYYY");
       String todayMonth = simpleDateFormat.format(new Date());
       return todayMonth;
   }
   public String getDateTimeWithSeconds(){
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
       String todayTimeWithSeconds = simpleDateFormat.format(new Date());
       return todayTimeWithSeconds;
       }

}
