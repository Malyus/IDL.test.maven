package com.art.idl.datetime;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class RandomDateGenerator {
    public String generateRandomDay(){
        String randomDay = "";
        Integer minimum = 1;
        Integer maximumRandomValue = 31; //we want to add a number of days between 1-31
        Integer randomDays = minimum + (int)(Math.random() * maximumRandomValue);
        if(randomDays<10){
            randomDay = "0" + randomDays.toString();
        }
        else {
            randomDay = randomDays.toString();
        }
        return randomDay;
    }
    public String generateRandomMonth(){
        String randomMonth = "";
        Integer min = 1;
        Integer max = 12;
        Integer randomMonths = min + (int)(Math.random() * max);//month between 1-12
        if(randomMonths<10){
            randomMonth = "0" + randomMonths.toString();
        }
        else{
            randomMonth =randomMonths.toString();
        }
        return randomMonth;
    }
    public String generateRandomYear(){
        String randomYear = "";
        Integer min = 1927;
        Integer max = 65;
        Integer randomYears = min + (int)(Math.random() * max);//year between 1927-2000
        randomYear = randomYears.toString();
        return randomYear;
    }
}
