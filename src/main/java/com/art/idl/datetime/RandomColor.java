package com.art.idl.datetime;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class RandomColor {
    static String brown = "Brown";
    static String blue = "Blue";
    static String green = "Green";
    static String lightBlue = "Lightblue";

    public static String getRandomColor(){
        String color = "";
        Integer min = 1;
        Integer max = 4;
        Integer randomNum = min + (int)(Math.random() * max);
        if(randomNum.intValue()==1){
            color = brown;
            System.out.println("Brown is chosen!");
        }
        else if(randomNum.intValue()==2){
            color = blue;
            System.out.println("Blue is chosen!");
        }
        else if(randomNum.intValue()==3){
            color = green;
            System.out.println("Green is chosen!");
        }
        else if(randomNum.intValue()==4){
            color = lightBlue;
            System.out.println("LightBlue is chosen!");
        }
        return color;
    }
}
