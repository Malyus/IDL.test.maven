package com.art.idl.datetime;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class RandomPhone {
    public static String generateRandomTelephone(){
        String randomTelephone = "";
        Integer min = 1;
        Integer max = 9;
        Integer randomYears = min + (int)(Math.random() * max);
        randomTelephone = "123456789" + randomYears.toString();
        return randomTelephone;
    }
}
