package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Set;

/**
 * Created by M.Malyus on 11/28/2017.
 */
public class AdminPanelLoginPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private String parentHandle;
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//input[@id=\"email\"]")
    private WebElement loginInput;

    @FindBy(xpath = "//input[@id=\"password\"]")
    private WebElement passwordInput;

    @FindBy(xpath = "//button[@class=\"btn-main\"]")
    private WebElement submitButton;

    public void openInNewWindow(String url) {
        ((JavascriptExecutor) driver)
                .executeScript("window.open(arguments[0])", url);
    }
    public void switchToANewWindow(){
        Set < String > newHandles = driver.getWindowHandles();
        driver.switchTo().window((String) newHandles.toArray()[1]);
    }
    @Step
    public void writeLoginOfAdmin(String adminLogin){
        wait.until(ExpectedConditions.visibilityOf(loginInput));
        loginInput.sendKeys(adminLogin);
    }
    @Step
    public void writePasswordOfAdmin(String adminPassword){
        passwordInput.sendKeys(adminPassword);
    }
    @Step
    public void clickOnSubmitButton(){
        submitButton.click();
    }
}
