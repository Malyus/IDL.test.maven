package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class AdminEditOrderPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//input[@value='UPLOAD']")
    private WebElement presentsOfEditOrderPage;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[3]")
    private WebElement ordersMenuButton;

    @FindBy(xpath = "(//button[@class='btn-main btn-edit btn-with-icon'])[1]")
    private WebElement editFirstOrderButton;

    @FindBy(xpath = "//button[@class='btn-main btn-save']")
    private WebElement saveEditOrderButton;

    @FindBy(xpath = "//input[@id='first_name']")
    private WebElement firstNameInput;

    @FindBy(xpath = "//input[@id='last_name']")
    private WebElement lastNameInput;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_month']")
    private WebElement monthOfBirthSelectDropdownMenu;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_day']")
    private WebElement dayOfBirthSelectDropdownMenu;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_year']")
    private WebElement yearOfBirthSelectDropdownMenu;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_month']//option[@selected='selected']")
    private WebElement selectedMonthOfBirth;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_day']//option[@selected='selected']")
    private WebElement selectedDayOfBirth;

    @FindBy(xpath = "//select[@id='fiscal_yr_end_year']//option[@selected='selected']")
    private WebElement selectedYearOfBirth;

    @FindBy(xpath = "//label[@for='female']")
    private WebElement femaleButton;

    @FindBy(xpath = "//label[@for='male']")
    private WebElement maleButton;

    @FindBy(xpath = "//select[@id='eye_color']")
    private WebElement eyeColorSelectDropdownMenu;

    @FindBy(xpath = "//select[@id='eye_color']//option[@selected='selected']")
    private WebElement selectedEyeColor;

    @FindBy(xpath = "//label[@for='measure_ft']")
    private WebElement ftButton;

    @FindBy(xpath = "//label[@for='measure_cm']")
    private WebElement cmButton;

    @FindBy(xpath = "//select[@name='ft']")
    private WebElement ftSelectDropdownMenu;

    @FindBy(xpath = "//select[@name='in']")
    private WebElement inSelectDropdownMenu;

    @FindBy(xpath = "//select[@name='ft']//option[@selected='selected']")
    private WebElement selectedFtHeight;

    @FindBy(xpath = "//select[@name='in']//option[@selected='selected']")
    private WebElement selectedInHeight;

    @FindBy(xpath = "//input[@id='phone']")
    private WebElement phoneInput;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//select[@id='address1Country']")
    private WebElement countryShippingDropdownMenuSelect;

    @FindBy(xpath = "//select[@id='address1Country']//option[@selected='selected']")
    private WebElement selectedShippingCountry;

    @FindBy(xpath = "//textarea[@id='address']")
    private WebElement shippingAddressInput;

    @FindBy(xpath = "//input[@id='city']")
    private WebElement shippingCity;

    @FindBy(xpath = "//select[@id='address1State']")
    private WebElement shippingStateSelectDropdownMenu;

    @FindBy(xpath = "//select[@id='address1State']//option[@selected='selected']")
    private WebElement selectedShippingState;

    @FindBy(xpath = "//input[@id='zcode']")
    private WebElement shippingZCodeInput;

    @Step
    public boolean checkPresentsOfEditOrderPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfEditOrderPage));
        return presentsOfEditOrderPage.isDisplayed();
    }
    @Step
    public void clickOnOrdersMenuButton(){
        ordersMenuButton.click();
    }
    @Step
    public void clickOnEditFirstOrderButton(){
        wait.until(ExpectedConditions.visibilityOf(editFirstOrderButton));
        editFirstOrderButton.click();
    }
    @Step
    public void clickOnSaveEditOrderButton(){
        saveEditOrderButton.click();
    }
    @Step
    public void clearFirstNameInput(){
        firstNameInput.clear();
    }
    @Step
    public void writeFirstNameInput(String firstName){
        firstNameInput.sendKeys(firstName);
    }
    @Step
    public String getTextOfFirstNameInput(){
        return firstNameInput.getAttribute("value");
    }
    @Step
    public void clearLastNameInput(){
        lastNameInput.clear();
    }
    @Step
    public void writeLastNameInput(String lastName){
        lastNameInput.sendKeys(lastName);
    }
    @Step
    public String getTextOfLastNameInput(){
        return lastNameInput.getAttribute("value");
    }
    @Step
    public void selectMonthOfBirth(String numberOfMonth){
        Select select = new Select(monthOfBirthSelectDropdownMenu);
        select.selectByValue(numberOfMonth);
    }
    @Step
    public void selectDayOfBirth(String numberOfDay){
        Select select = new Select(dayOfBirthSelectDropdownMenu);
        select.selectByValue(numberOfDay);
    }
    @Step
    public void selectYearOfBirth(String numberOfYear){
        Select select = new Select(yearOfBirthSelectDropdownMenu);
        select.selectByValue(numberOfYear);
    }
    @Step
    public String getSelectedMonthOfBirth(){
        return selectedMonthOfBirth.getAttribute("value");
    }
    @Step
    public String getSelectedDayOfBirth(){
        return selectedDayOfBirth.getAttribute("value");
    }
    @Step
    public String getSelectedYearOfBirth(){
        return selectedYearOfBirth.getAttribute("value");
    }
    @Step
    public void clickOnFemaleButton(){
        femaleButton.click();
    }
    @Step
    public String checkIfMaleIsChosen(){
        return maleButton.getCssValue("background-color");
    }
    @Step
    public void clickOnMaleButton(){
        maleButton.click();
    }
    @Step
    public String checkIfFemaleIsChosen(){
        return femaleButton.getCssValue("background-color");
    }
    @Step
    public void selectEyeColor(String eyeColor){
        Select select = new Select(eyeColorSelectDropdownMenu);
        select.selectByValue(eyeColor);
    }
    @Step
    public String checkIfCorrectEyeColorIsSelected(){
        return selectedEyeColor.getText();
    }
    @Step
    public void clickOnFtButton(){
        ftButton.click();
    }
    @Step
    public void clickOnCmButton(){
        cmButton.click();
    }
    @Step
    public String checkIfFtButtonIsSelected(){
        return ftButton.getCssValue("background-color");
    }
    @Step
    public String checkIfCmButtonIsSelected(){
        return cmButton.getCssValue("background-color");
    }
    @Step
    public void selectHeightInFt(String ftValue){
        Select select = new Select(ftSelectDropdownMenu);
        select.selectByValue(ftValue);
    }
    @Step
    public void selectInOfHeight(String inValue){
        Select select = new Select(inSelectDropdownMenu);
        select.selectByValue(inValue);
    }
    @Step
    public String checkIfFtIsCorrect(){
        return selectedFtHeight.getText();
    }
    @Step
    public String checkIfInIsCorrect(){
        return selectedInHeight.getText();
    }
    @Step
    public void clearPhoneInput(){
        phoneInput.clear();
    }
    @Step
    public void writeTelephoneNumber(String telephoneNumber){
        phoneInput.sendKeys(telephoneNumber);
    }
    @Step
    public String checkIfTelephoneIsCorrect(){
        return phoneInput.getAttribute("value");
    }
    @Step
    public void clearEmailInput(){
        emailInput.clear();
    }
    @Step
    public void writeEmailInput(String email){
        emailInput.sendKeys(email);
    }
    @Step
    public String checkIfEmailIsCorrect(){
        return emailInput.getAttribute("value");
    }
    @Step
    public void selectShippingCountry(String country){
        Select select = new Select(countryShippingDropdownMenuSelect);
        select.selectByValue(country);
    }
    @Step
    public String checkIfShippingCountryIsCorrect(){
        return selectedShippingCountry.getAttribute("value");
    }
    @Step
    public void clearShippingAddressInput(){
        shippingAddressInput.clear();
    }
    @Step
    public void writeShippingAddressInput(String shippingAddress){
        shippingAddressInput.sendKeys(shippingAddress);
    }
    @Step
    public String checkIfShippingAddressIsCorrect(){
        return shippingAddressInput.getText();
    }
    @Step
    public void clearShippingCity(){
        shippingCity.clear();
    }
    @Step
    public void writeShippingCity(String city){
        shippingCity.sendKeys(city);
    }
    @Step
    public String checkIfShippingCityIsCorrect(){
        return shippingCity.getAttribute("value");
    }
    @Step
    public void selectShippingState(String shippingState){
        Select select = new Select(shippingStateSelectDropdownMenu);
        select.selectByValue(shippingState);
    }
    @Step
    public String checkIfShippingStateIsCorrect(){
        return selectedShippingCountry.getAttribute("value");
    }
    @Step
    public void clearShippingZCode(){
        shippingZCodeInput.clear();
    }
    @Step
    public void writeShippingZCode(String shippingZCode){
        shippingZCodeInput.sendKeys(shippingZCode);
    }
    @Step
    public String checkIfShippingZCodeIsCorrect(){
        return shippingZCodeInput.getAttribute("value");
    }

}
