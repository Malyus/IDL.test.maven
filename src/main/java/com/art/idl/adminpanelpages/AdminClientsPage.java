package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 12/4/2017.
 */
public class AdminClientsPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);
    private Select itemsListSelect;
    private Select sortListSelect;

    @FindBy(xpath = "//div[2]/ul/li[2]/a")
    private WebElement clientsPageButton;

    @FindBy(xpath = "//select[@id='limit']//option[@selected = 'selected']")
    private WebElement defaultValueItemList;

    @FindBy(xpath = "//select[@id='limit']/option[2]")
    private WebElement valueOfSecondOptionInItemList;

    @FindBy(xpath = "//select[@id='limit']/option[3]")
    private WebElement valueOfThirdOptionInItemList;

    @FindBy(xpath = "//select[@id='limit']/option[4]")
    private WebElement valueOfFourthOptionInItemList;

    @FindBy(xpath = "//select[@id='limit']/option[5]")
    private WebElement valueOfFifthOptionInItemList;

    @FindBy(xpath = "//select[@id='limit']")
    private WebElement itemsListSelectMenu;

    @FindBy(xpath = "//select[@id='limit']//option[@selected = 'selected']")
    private WebElement selectedOptionOfItemList;

    @FindBy(xpath = "//button[@id='select_all']")
    private WebElement selectAllButton;

    @FindBy(xpath = "//span[@class=\"selected-count\"]")
    private WebElement countOfSelectedAfteSelectAll;

    @FindAll(@FindBy(xpath = "//div[@class='checker']"))
    private List<WebElement> listOfClientsSelectCheckboxes;

    @FindBy(xpath = "//span[@class=\"selected-count\"]")
    private WebElement selectedItemsCount;

    @FindBy(xpath = "(//button[@class=\"btn-main btn-delete btn-with-icon btn-delete-one\"])[1]")
    private WebElement firstDeleteButton;

    @FindBy(xpath = "//button[@class='delete_confirm btn-main btn-yes']")
    private WebElement yesDeleteConfirmButton;

    @FindBy(xpath = "//button[@class='btn-main btn-delete btn-with-icon btn-delete-all']")
    private WebElement deleteAllSelectedButton;

    @FindBy(xpath = "//button[@class='delete_all_confirm btn-main btn-yes']")
    private WebElement yesButtonDeleteAllClients;

    @FindBy(xpath = "//div[@style='display: none;' and @class='info-about-selected']")
    private WebElement presentsOfDeleteButton;

    @FindBy(xpath = "//option[@value='name_az']")
    private WebElement sortByAZValue;

    @FindBy(xpath = "//option[@value='name_za']")
    private WebElement sortByZAValue;

    @FindBy(xpath = "//select[@id='sort']")
    private WebElement sortAZSelectMenuButton;

    @FindBy(xpath = "//option[@value='name_az']")
    private WebElement azOption;

    @FindBy(xpath = "//option[@value='name_za']")
    private WebElement zaOption;

    @FindBy(xpath = "//select[@id='sort']//option[@selected = 'selected']")
    private WebElement selectedSortOption;

    @FindBy(xpath = "//ul[@class='filters-group']//li[2]")
    private WebElement finishedOrdersFilterButton;

    @FindBy(xpath = "//ul[@id='gr-1']//li[3]")
    private WebElement notFinishedOrdersFilterButton;

    @FindBy(xpath = "//ul[@id='gr-1']//li[2]")
    private WebElement finishedOrdersActiveButton;

    @FindBy(xpath = "//ul[@id='gr-1']//li[3]")
    private WebElement notFinishedOrdersActiveButton;

    @FindBy(xpath = "//ul[@id='gr-1']//li[2]")
    private WebElement finishedOrdersButtonNotActivated;

    @FindBy(xpath = "//ul[@id='gr-1']//li[1]")
    private WebElement allFilterButton;

    @FindBy(xpath = "(//div[@class='checker'])[1]")
    private WebElement firstClientCheckbox;

    @FindBy(xpath = "(//li[@class='c-number'])[2]")
    private WebElement firstClientUniqueId;

    @FindBy(xpath = "(//div[@class='checker'])[2]")
    private WebElement secondClientCheckbox;

    @FindBy(xpath = "(//li[@class='c-number'])[3]")
    private WebElement secondClientUniqueId;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[3]")
    private WebElement ordersPageMenuButton;

    @FindBy(xpath = "//a[@rel='next']")
    private WebElement nextPageButton;

    @FindBy(xpath = "//ul[@class='pagination']//li[3]")
    private WebElement presentsOfSecondPage;

    @FindBy(xpath = "//p[@class='text-center text-color-1']")
    private WebElement numberOfPageText;

    @FindBy(xpath = "//li[@class='prev']//a")
    private WebElement previousPageButton;

    @FindBy(xpath = "(//li[@class='c-orders'])[2]")
    private WebElement firstClientOrder;

    @FindBy(xpath = "(//li[@class='c-contact'])[2]")
    private WebElement nameOfFirstUser;

    @FindBy(xpath = "(//li[@class='c-phone'])[1]")
    private WebElement presentsOfClientsPage;

    @FindBy(xpath = "(//div[@class='dashboard-box-footer'])[1]")
    private WebElement presentsOfDashboardPage;

    @FindBy(xpath = "//a[@class='logo']")
    private WebElement logoButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[4]")
    private WebElement emailsMenuButton;

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement presentsOfEmailPage;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[5]")
    private WebElement seoMenuButton;

    @FindBy(xpath = "(//p[@class='symptom-title'])[1]")
    private WebElement presentsOfSeoPage;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[8]")
    private WebElement usersMenuButton;

    @FindBy(xpath = "//label[@for='type_superadmin_1']")
    private WebElement presentsOfUsersPage;

    @FindBy(xpath = "//i[@class='fa fa-fw fa-gear']")
    private WebElement settingsMenuButton;

    @FindBy(xpath = "(//input[@type='password'])[1]")
    private WebElement presentsOfSettingPage;

    @FindBy(xpath = "//select[@id='limit']")
    private WebElement itemsInListOpenButton;

    @FindBy(xpath = "//option[@value='20']")
    private WebElement twentyOptionInItemList;

    @FindBy(xpath = "//option[@value='50']")
    private WebElement fiftyOptionInItemList;

    @FindBy(xpath = "//option[@value='75']")
    private WebElement seventyFiveOptionInItemList;

    @FindBy(xpath = "//option[@value='100']")
    private WebElement oneHundredOptionInItemList;

    @FindBy(xpath = "//option[@value='200']")
    private WebElement twoHundredOptionInItemList;

    @FindBy(xpath = "(//li[@class='c-email'])[1]")
    private WebElement element;

    @FindBy(xpath = "//input[@class='search']")
    private WebElement searchInputField;

    @Step
    public void clickOnClientsButton(){
        clientsPageButton.click();
    }
    @Step
    public String getDefaultValueOfItemList(){
        wait.until(ExpectedConditions.visibilityOf(defaultValueItemList));
        wait.until(ExpectedConditions.visibilityOf(defaultValueItemList));
        return defaultValueItemList.getText();
    }
    @Step
    public String getValueOfSecondOptionOfItemList(){
        return valueOfSecondOptionInItemList.getText();
    }
    @Step
    public String getValueOfThirdOptionOfItemList(){
        return valueOfThirdOptionInItemList.getText();
    }
    @Step
    public String getValueOfFourthOptionOfItemList(){
        return valueOfFourthOptionInItemList.getText();
    }
    @Step
    public String getValueOfFifthOptionOfItemList(){
        return valueOfFifthOptionInItemList.getText();
    }
    @Step
    public void selectSomeOptionOfItemList(int numberOfOption){
        itemsListSelect = new Select(itemsListSelectMenu);
        itemsListSelect.selectByIndex(numberOfOption);
    }
    @Step
    public String checkIfTwentyOptionIsSelected(){
        return selectedOptionOfItemList.getText();
    }

    @Step
    public String checkIfFiftyOptionIsSelected(){
        return selectedOptionOfItemList.getText();
    }
    @Step
    public String checkIfSeventyFiveOptionIsSelected(){
        return selectedOptionOfItemList.getText();
    }
    @Step
    public String checkIfOneHundredOptionIsSelected(){
        return  selectedOptionOfItemList.getText();
    }
    @Step
    public String checkIfTwoHundredOptionIsSelected(){
        return selectedOptionOfItemList.getText();
    }
    @Step
    public void clickOnSelectAllButton(){
        selectAllButton.click();
    }
    @Step
    public String getValueOfSelectedItemsAfterSelectAll(){
        return countOfSelectedAfteSelectAll.getText();
    }
    @Step
    public void selectClientsByCheckBox(int countOfSelectCheckboxes){
        for(int x = 0;x < countOfSelectCheckboxes;x++){
            listOfClientsSelectCheckboxes.get(x).click();
        }
    }
    @Step
    public String getCountOfSelectedItems(){
        return selectedItemsCount.getText();
    }
    @Step
    public void deleteFirstSelectedItem(){
        firstDeleteButton.click();
    }
    @Step
    public void clickOnYesDeleteClientButton(){
        wait.until(ExpectedConditions.visibilityOf(yesDeleteConfirmButton));
        yesDeleteConfirmButton.click();
    }
    @Step
    public void clickOnDeleteAllSelectedButton(){
        deleteAllSelectedButton.click();
    }
    @Step
    public boolean checkPresentsOfDeleteAllButton(){
        return deleteAllSelectedButton.isDisplayed();
    }
    @Step
    public void clickOnYesButtonOfSelectedAllClients(){
        wait.until(ExpectedConditions.visibilityOf(yesButtonDeleteAllClients));
        yesButtonDeleteAllClients.click();
    }
    @Step
    public String getStyleOfDeleteButton(){
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@style='display: none;' and @class='info-about-selected']")));
        return presentsOfDeleteButton.getAttribute("style");
    }
    //This method choose first five odd clients
    @Step
    public void selectFirstFivePairedClients(){
      for(int x = 1;x<6;x++){
          listOfClientsSelectCheckboxes.get(x).click();
      }
    }
    @Step
    public String checkActiveOfFinishedOrdersButton(){
        return finishedOrdersFilterButton.getAttribute("class");
    }
    @Step
    public String getValueOfAZSort(){
        return sortByAZValue.getText();
    }
    @Step
    public String getValueOfZASort(){
        return sortByZAValue.getText();
    }
    @Step
    public void scrollUpDownInAZSelectMenu(){
        sortAZSelectMenuButton.click();
        for(int x = 0;x<10;x++) {
            /*This action make a scroll-down by arrow_down key
            and move to Z-A option*/
            Actions azActions = new Actions(driver);
            azActions.moveToElement(azOption);
            azActions.sendKeys(Keys.ARROW_DOWN);
            azActions.build().perform();
            /*This action make a scroll-up by arrow_up key
            and move to A-Z option */
            Actions zaActions = new Actions(driver);
            zaActions.moveToElement(zaOption);
            zaActions.sendKeys(Keys.ARROW_UP);
           zaActions.build().perform();
        }
        //        // Create instance of Javascript executor
//        JavascriptExecutor je = (JavascriptExecutor) driver;
////Identify the WebElement which will appear after scrolling down
//        WebElement element = driver.findElement(By.xpath("//select[@id='sort']"));
//// now execute query which actually will scroll until that element is not appeared on page.
//        je.executeScript("arguments[0].scrollIntoView(true);",element);
    }
    @Step
    public void selectOptionInSortMenu(int numberOfSortOption){
        sortListSelect = new Select(sortAZSelectMenuButton);
        sortListSelect.selectByIndex(numberOfSortOption);
    }
    @Step
    public String getNameOfSelectedOption(){
      return selectedSortOption.getText();
    }
    @Step
    public void clickOnFinishedOrdersButton(){
        finishedOrdersFilterButton.click();
    }
    @Step
    public void clickOnNotFinishedOrdersButton(){
        notFinishedOrdersFilterButton.click();
    }
    @Step
    public String checkIfFinishedOrdersButtonIsActive(){
        wait.until(ExpectedConditions.visibilityOf(finishedOrdersActiveButton));
        return finishedOrdersActiveButton.getAttribute("class");
    }
    @Step
    public String checkIfFinishedOrdersButtonIsNotActive(){
        wait.until(ExpectedConditions.visibilityOf(finishedOrdersButtonNotActivated));
        return finishedOrdersButtonNotActivated.getAttribute("class");
    }
    @Step
    public String checkIfNotFinishedOrdersButtonIsActive(){
        wait.until(ExpectedConditions.visibilityOf(notFinishedOrdersActiveButton));
        return notFinishedOrdersFilterButton.getAttribute("class");
    }
    @Step
    public void clickOnAllFilterButton(){
        allFilterButton.click();
    }
    @Step
    public String checkActiveOfAllFilterButton(){
        return allFilterButton.getAttribute("class");
    }
    @Step
    public void selectFirstClient(){
        firstClientCheckbox.click();
    }
    @Step
    public String getUniqueIdOfFirstClient(){
        return firstClientUniqueId.getText();
    }
    @Step
    public void selectSecondClientCheckbox(){
        secondClientCheckbox.click();
    }
    @Step
    public String getUniqueIdOfSecondClient(){
        return secondClientUniqueId.getText();
    }
    @Step
    public void clickOnOrdersPageMenuButton(){
        ordersPageMenuButton.click();
    }
    @Step
    public void clickOnNextPageButton(){
        nextPageButton.click();
    }
    @Step
    public String checkPresentsOfSecondPage(){
        return presentsOfSecondPage.getAttribute("class");
    }
    @Step
    public String getNumberOfPageText(){
        return numberOfPageText.getText();
    }
    @Step
    public void clickOnPreviousButton(){
        previousPageButton.click();
    }
    @Step
    public String getOrderNameOfFirstClient(){
        return firstClientOrder.getText();
    }
    @Step
    public String getNameOfFirstUser(){
        return nameOfFirstUser.getText();
    }
    @Step
    public boolean checkIfClientsPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfClientsPage));
        return presentsOfClientsPage.isDisplayed();
    }
    @Step
    public void turnBackToPreviousPage(){
        driver.navigate().back();
    }
    @Step
    public boolean checkIfDashboardPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfDashboardPage));
        return presentsOfDashboardPage.isDisplayed();
    }
    @Step
    public void clickOnLogoButton(){
        logoButton.click();
    }
    @Step
    public void clickOnEmailsButton(){
        emailsMenuButton.click();
    }
    @Step
    public boolean checkIfEmailsPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfEmailPage));
        return presentsOfEmailPage.isDisplayed();
    }
    @Step
    public void clickOnSeoMenuButton(){
        seoMenuButton.click();
    }
    @Step
    public boolean checkIfSeoPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfSeoPage));
        return presentsOfSeoPage.isDisplayed();
    }
    @Step
    public void clickOnUsersMenuButton(){
        usersMenuButton.click();
    }
    @Step
    public boolean checkIfUsersPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfUsersPage));
        return presentsOfUsersPage.isDisplayed();
    }
    @Step
    public void clickOnSettingsMenuButton(){
        settingsMenuButton.click();
    }
    @Step
    public boolean checkIfSettingsPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfSettingPage));
        return presentsOfSettingPage.isDisplayed();
    }
    @Step
    public void scrollItemsInListUpAndDown(){
        itemsInListOpenButton.click();

        for(int x = 0;x<10;x++) {
            /*This action make a scroll-down by arrow_down key
            and move to 50 option*/
            Actions twentyActions = new Actions(driver);
            twentyActions.moveToElement(twentyOptionInItemList);
            twentyActions.sendKeys(Keys.ARROW_DOWN);
            twentyActions.build().perform();
            /*This action make a scroll-down by arrow_down key
            and move to 75 option */
            Actions fiftyActions = new Actions(driver);
            fiftyActions.moveToElement(fiftyOptionInItemList);
            fiftyActions.sendKeys(Keys.ARROW_DOWN);
            fiftyActions.build().perform();
            /*This action make a scroll-down by arrow_down key
            and move to 100 option*/
            Actions oneHundredActions = new Actions(driver);
            oneHundredActions.moveToElement(seventyFiveOptionInItemList);
            oneHundredActions.sendKeys(Keys.ARROW_DOWN);
            oneHundredActions.build().perform();
            /*This action make a scroll-down by arrow_down key
            and move to 200 options*/
            Actions twoHundredActions = new Actions(driver);
            twoHundredActions.moveToElement(oneHundredOptionInItemList);
            twoHundredActions.sendKeys(Keys.ARROW_DOWN);
            twoHundredActions.build().perform();
           /* This action make a scroll-up by arrow_up key
             and move to 100 options*/
           Actions upOne = new Actions(driver);
           upOne.moveToElement(twoHundredOptionInItemList);
           upOne.sendKeys(Keys.ARROW_UP);
           upOne.build().perform();

           Actions upTwo = new Actions(driver);
           upTwo.moveToElement(oneHundredOptionInItemList);
           upTwo.sendKeys(Keys.ARROW_UP);
           upTwo.build().perform();

           Actions upThree = new Actions(driver);
           upThree.moveToElement(seventyFiveOptionInItemList);
           upThree.sendKeys(Keys.ARROW_UP);
           upThree.build().perform();

           Actions upFour = new Actions(driver);
           upFour.moveToElement(fiftyOptionInItemList);
           upFour.sendKeys(Keys.ARROW_UP);
           upFour.build().perform();
        }
    }
    @Step
    public void makeXXSAttack(){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("alert('hello world');");
    }
    @Step
    public void writeIntoSearchInput(String searchInput){
        wait.until(ExpectedConditions.visibilityOf(searchInputField));
        searchInputField.sendKeys(searchInput);
        searchInputField.sendKeys(Keys.ENTER);
    }
}
