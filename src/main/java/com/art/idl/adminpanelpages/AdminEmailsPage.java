package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 12/11/2017.
 */
public class AdminEmailsPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private String parentHandle;
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement presentsOfEmailsPage;

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[4]")
    private WebElement emailsMenuButton;

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement addNewEmailButton;

    @FindBy(xpath = "//div[@id='mceu_21-body']")
    private WebElement presentsOfAddNewEmailPage;

    @FindBy(xpath = "//input[@name='name']")
    private WebElement nameOfEmailInput;

    @FindBy(xpath = "//input[@name = 'subject']")
    private WebElement subjectOfEmailInput;

    @FindBy(xpath = "//iframe[@id='description_ifr']")
    private WebElement iframeOfEmail;

    @FindBy(xpath = "//body[@id='tinymce']//p")
    private WebElement contentOfEmailFirstInput;

    @FindBy(xpath = "//button[@class='btn-main btn-save']")
    private WebElement saveEmailButton;

    @FindAll(@FindBy(xpath = "//button[@class='btn-main btn-delete btn-with-icon']"))
    private List<WebElement> deleteCreatedEmailButton;

    @FindAll(@FindBy(xpath = "//table[@class='standard-table table-editable-data']//td[1]"))
    private List<WebElement> listOfCreatedEmails;

    @FindBy(xpath = "(//button[@class='btn-main btn-edit btn-with-icon'])[1]")
    private WebElement editFirstEmailButton;

    @FindBy(xpath = "//li[@class='active']")
    private WebElement presentsOfEditEmailPage;

    @FindBy(xpath = "//button[@class='btn-main btn-type-3 btn-cancel']")
    private WebElement cancelButton;

    @FindAll(@FindBy(xpath = "//button[@class='btn-main btn-edit btn-with-icon']"))
    private List<WebElement> listOfEditEmailsButtons;

    @FindBy(xpath = "//form/div[3]/div/div")
    private WebElement subjectErrorMessage;

    @FindBy(xpath = "//form/div[2]/div/div")
    private WebElement nameErrorMessage;

    @FindBy(xpath = "//div/form/div[4]/div/div[2]")
    private WebElement descriptionErrorMessage;



    @Step
    public boolean checkPresentsOfEmailsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfEmailsPage));
        return presentsOfEmailsPage.isDisplayed();
    }
    @Step
    public void clickOnEmailsMenuButton(){
        emailsMenuButton.click();
    }
    @Step
    public void clickOnAddNewMessage(){
        wait.until(ExpectedConditions.visibilityOf(addNewEmailButton));
        addNewEmailButton.click();
    }
    @Step
    public boolean checkPresentsOfAddNewEmailPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfAddNewEmailPage));
        return presentsOfAddNewEmailPage.isDisplayed();
    }
    @Step
    public void writeNameOfEmail(String nameOfEmail){
        wait.until(ExpectedConditions.visibilityOf(nameOfEmailInput));
        nameOfEmailInput.sendKeys(nameOfEmail);
    }
    @Step
    public void writeSubjectOfEmail(String subjectOfEmail){
        subjectOfEmailInput.sendKeys(subjectOfEmail);
    }
    @Step
    public void switchToEmailIframe(){
        driver.switchTo().frame("description_ifr");
    }
    @Step
    public void writeContentOfEmail(String contentOfEmail){
        Actions actions = new Actions(driver);
        actions.moveToElement(contentOfEmailFirstInput);
        actions.click();
        actions.sendKeys(contentOfEmail);
        actions.build().perform();
    }
    @Step
    public void clickOnSaveEmailButton(){
        driver.switchTo().defaultContent();
        saveEmailButton.click();
    }
    @Step
    public void deleteCreatedEmail(){
        wait.until(ExpectedConditions.visibilityOf(addNewEmailButton));
        deleteCreatedEmailButton.get(deleteCreatedEmailButton.size()-1).click();
        Alert deleteEmailAlert = driver.switchTo().alert();
        deleteEmailAlert.accept();
    }
    @Step
    public String checkIfDeletedEmailIsDeleted(){
        return listOfCreatedEmails.get(listOfCreatedEmails.size()-1).getText();
    }
    @Step
    public String checkPresentsOfCreatedEmail(){
        return listOfCreatedEmails.get(listOfCreatedEmails.size()-1).getText();
    }
    @Step
    public void clickOnEditEmailButton(){
        editFirstEmailButton.click();
    }
    @Step
    public String checkPresentsOfEditEmailPage(){
        return presentsOfEditEmailPage.getText();
    }
    @Step
    public void clickOnCancelButton(){
        cancelButton.click();
    }
    @Step
    public void clickOnEditButtonOfCreatedEmail(){
        listOfEditEmailsButtons.get(listOfCreatedEmails.size()-1).click();
    }
    @Step
    public String getValueOfSubjectInput(){
        return subjectOfEmailInput.getAttribute("value");
    }
    @Step
    public String getValueOfNameInput(){
        return nameOfEmailInput.getAttribute("value");
    }
    @Step
    public String getValueOfDescription(){
        driver.switchTo().frame("description_ifr");
        return contentOfEmailFirstInput.getText();
    }
    @Step
    public void switchOffFromIFrame(){
        driver.switchTo().defaultContent();
    }
    @Step
    public void clearSubjectInput(){
        subjectOfEmailInput.clear();
    }
    @Step
    public void clearNameInput(){
        nameOfEmailInput.clear();
    }
    @Step
    public void clearDescriptionInput(){
        contentOfEmailFirstInput.clear();
    }
    @Step
    public boolean checkPresentsOfSubjectErrorMessage(){
        return subjectErrorMessage.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfNameErrorMessage(){
        return nameErrorMessage.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfDescriptionErrorMessage(){
        return descriptionErrorMessage.isDisplayed();
    }
    @Step
    public String getTextOfNameErrorMessage(){
        return nameErrorMessage.getText();
    }
    @Step
    public String getTextOfSubjectErrorMessage(){
        return subjectErrorMessage.getText();
    }
    @Step
    public String getTextOfDescriptionErrorMessage(){
        return descriptionErrorMessage.getText();
    }
}
