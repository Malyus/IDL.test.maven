package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 11/29/2017.
 */
public class AdminOrdersPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);
    private  Select selectMenuItems;

    @FindBy(xpath = "//*[@id=\"wrapper\"]/nav/div[2]/ul/li[3]/a")
    private WebElement ordersMenuButton;

    @FindBy(xpath = "//select[@id=\"limit\"]")
    private WebElement selectMenu;

    @FindBy(xpath = "//input[@class=\"date-input date-input_start\"]")
    private WebElement presentsOfOrdersPage;

    @FindBy(xpath = "//input[@class=\"date-input date-input_start\"]")
    private WebElement calendarStartDateOpenButton;

    @FindBy(xpath = "//div[1]/table/tbody/tr[2]/td[6]")
    private WebElement tenthDayOfStartDateMonth;

    @FindBy(xpath = "//div[2]/div[1]/table/tbody/tr[2]/td[7]")
    private WebElement eleventhDayOfStartDateMonth;

    @FindBy(xpath = "//input[@class='date-input date-input_end']")
    private WebElement calendarEndDateOpenButton;

    @FindBy(xpath = "//div[3]/div[1]/table/tbody/tr[2]/td[6]")
    private WebElement tenthDayOfEndDateMonth;

    @FindBy(xpath = "//div[3]/div[1]/table/tbody/tr[2]/td[7]")
    private WebElement eleventhDayOfEndDateMonth;

    @FindBy(xpath = "//input[@id=\"date-input_start\"]")
    private WebElement valueOfStartDay;

    @FindBy(xpath = "//input[@id=\"date-input_end\"]")
    private WebElement valueOfEndDay;

    @FindBy(xpath = "//i[@class=\"fa fa-times-circle btn-remove-calendar\"]")
    private WebElement clearCalendarButton;

    @FindBy(xpath = "//input[@id='date-input_start' and contains (@value, '')]")
    private WebElement clearedStartDay;

    @FindBy(xpath = "//option[@selected = \"selected\" and contains(@value, '20')]")
    private WebElement itemLInListByDefault;

    @FindBy(xpath = "//option[@value = \"20\"]")
    private WebElement twentyValueOfItemList;

    @FindBy(xpath = "//option[@value=\"50\"]")
    private WebElement fiftyValueOfItemList;

    @FindBy(xpath = "//option[@value=\"75\"]")
    private WebElement seventyFiveValueOfItemList;

    @FindBy(xpath = "//option[@value=\"75\"]")
    private WebElement oneHundredValueOfItemList;

    @FindBy(xpath = "//option[@value = \"200\"]")
    private WebElement twoHundredValueOfItemList;

    @FindAll(@FindBy(xpath = "//select[@id=\"limit\"]//option"))
    private List<WebElement> listOfItems;

    @FindBy(xpath = "//option[@selected and contains(@value, '20')]")
    private WebElement chosenTwentyValueOfItem;

    @FindBy(xpath = "//option[@selected and contains(@value, '50')]")
    private WebElement chosenFiftyValueOfItem;

    @FindBy(xpath = "//option[@selected and contains(@value, '75')]")
    private WebElement chosenSeventyFiveValueOfItem;

    @FindBy(xpath = "//option[@selected=\"selected\" and contains(@value, '100')]")
    private WebElement chosenOneHundredValueOfItem;

    @FindBy(xpath = "//option[@selected=\"selected\" and contains(@value, '200')]")
    private WebElement chosenTwoHundredValueOfItem;

    @Step
    public void clickOnOrdersMenuButton(){
        wait.until(ExpectedConditions.visibilityOf(ordersMenuButton));
        ordersMenuButton.click();
    }
    @Step
    public boolean checkPresentsOfOrdersPage(){
        return presentsOfOrdersPage.isEnabled();
    }
    @Step
    public void openCalendarStartDay(){
        calendarStartDateOpenButton.click();
    }
    @Step
    public void clickOnTenthDayOfStartDayMonth(){
        wait.until(ExpectedConditions.visibilityOf(tenthDayOfStartDateMonth));
        tenthDayOfStartDateMonth.click();
    }
    @Step
    public void openCalendarEndDay(){
        calendarEndDateOpenButton.click();
    }
    @Step
    public void clickOnEleventhDayOfStartDayMonth(){
        wait.until(ExpectedConditions.visibilityOf(eleventhDayOfStartDateMonth));
        eleventhDayOfStartDateMonth.click();
    }
    @Step
    public void clickOnEleventhDayOfEndDayMonth(){
     wait.until(ExpectedConditions.visibilityOf(eleventhDayOfEndDateMonth));
     eleventhDayOfEndDateMonth.click();
    }
    @Step
    public void clickOnTenthDayOfEndDayMonth(){
        wait.until(ExpectedConditions.visibilityOf(tenthDayOfEndDateMonth));
        tenthDayOfEndDateMonth.click();
    }
    @Step
    public String getStartDayValue(){
        return valueOfStartDay.getAttribute("value");
    }
    @Step
    public String getEndDayValue(){
        return valueOfEndDay.getAttribute("value");
    }
    @Step
    public void clearCalendarValues(){
        clearCalendarButton.click();
    }
    @Step
    public void waitUntilCalendarIsCleared(){
      wait.until(ExpectedConditions.visibilityOf(clearedStartDay));
    }
    @Step
    public String checkIfItemInListByDefaultIs20(){
        wait.until(ExpectedConditions.visibilityOf(itemLInListByDefault));
        return itemLInListByDefault.getText();
    }
    @Step
    public boolean checkIfTwentyIsInItemList(){
        return twentyValueOfItemList.isEnabled();
    }
    @Step
    public boolean checkIfFiftyIsInItemList(){
        return fiftyValueOfItemList.isEnabled();
    }
    @Step
    public boolean checkIfSeventyFiveIsInItemList(){
        return seventyFiveValueOfItemList.isEnabled();
    }
    @Step
    public boolean checkIfOneHundredIsInItemList(){
        return oneHundredValueOfItemList.isEnabled();
    }
    @Step
    public boolean checkIfTwoHundredIsInItemList(){
        return twoHundredValueOfItemList.isEnabled();
    }
    @Step
    public int checkCountOfItemsList(){
        return listOfItems.size();
    }
    @Step
    public void chooseFiftyInItemListSelect(){
        //Items in list menu select(select 50)
        selectMenuItems = new Select(driver.findElement(By.xpath("//select[@id=\"limit\"]")));
        selectMenuItems.selectByIndex(1);
    }
    /* This method must be call second
        because 20 is chosen by default*/
    @Step
    public void chooseTwentyInItemListSelect(){
        //Item in list menu select(select 20)
        selectMenuItems = new Select(driver.findElement(By.xpath("//select[@id=\"limit\"]")));
        selectMenuItems.selectByIndex(0);
    }
    @Step
    public void chooseSeventyFiveItemListSelect(){
        //Item in list menu select(select 75)
        selectMenuItems = new Select(selectMenu);
        selectMenuItems.selectByIndex(2);
    }
    @Step
    public void chooseOneHundredItemListSelect(){
        //Item in list menu select(select 100)
        selectMenuItems = new Select(selectMenu);
        selectMenuItems.selectByIndex(3);
    }
    @Step
    public void chooseTwoHundredItemLisSelect(){
        //Item in list menu select(select 200)
        selectMenuItems = new Select(selectMenu);
        selectMenuItems.selectByIndex(4);
    }
    @Step
    public boolean checkIfTwentyValueIsChosen(){
        wait.until(ExpectedConditions.visibilityOf(chosenTwentyValueOfItem));
        return chosenTwentyValueOfItem.isEnabled();
    }
    @Step
    public boolean checkIfFiftyValueIsChosen(){
        wait.until(ExpectedConditions.visibilityOf(chosenFiftyValueOfItem));
        return chosenFiftyValueOfItem.isEnabled();
    }
    @Step
    public boolean checkIfSeventyFiveValueIsChosen(){
        wait.until(ExpectedConditions.visibilityOf(chosenSeventyFiveValueOfItem));
        return chosenSeventyFiveValueOfItem.isEnabled();
    }
    @Step
    public boolean checkIfOneHundredValueIsChosen(){
        wait.until(ExpectedConditions.visibilityOf(chosenOneHundredValueOfItem));
        return chosenOneHundredValueOfItem.isEnabled();
    }
    @Step
    public boolean checkIfTwoHundredValueIsChosen(){
        wait.until(ExpectedConditions.visibilityOf(chosenTwoHundredValueOfItem));
        return chosenTwoHundredValueOfItem.isEnabled();
    }

}
