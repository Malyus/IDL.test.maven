package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 12/13/2017.
 */
public class AdminSettingsPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//i[@class='fa fa-fw fa-gear']")
    private WebElement settingButton;

    @FindBy(xpath = "//input[@name='first_name']")
    private WebElement nameOfUserInput;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailOfUserInput;

    @FindBy(xpath = "//input[@name='current_pass']")
    private WebElement currentPasswordInput;

    @FindBy(xpath = "//input[@name='current_pass']")
    private WebElement presentsOfSettingsPage;

    @FindBy(xpath = "//input[@name='new_pass']")
    private WebElement newPasswordInput;

    @FindBy(xpath = "//input[@name='confirm_pass']")
    private WebElement confirmNewPasswordInput;

    @FindBy(xpath = "//button[@class='btn-main btn-save btn-save-personal-data']")
    private WebElement saveChangesButton;

    @FindBy(xpath = "//a[@href='#customization']//li")
    private WebElement customizationSettingButton;

    @FindBy(xpath = "(//div[@class='radio-checker']//span)[1]")
    private WebElement darkBlueColorCheckbox;

    @FindBy(xpath = "//button[@class='btn-main btn-save btn-save-theme']")
    private WebElement saveCustomizationButton;

    @FindBy(xpath = "//div/div[3]/label/span")
    private WebElement blueColor;

    @FindBy(xpath = "//div/div[4]/label/span")
    private WebElement lightBlueColor;

    @FindBy(xpath = "//div/div[5]/label/span")
    private WebElement aquaColor;

    @FindBy(xpath = "//div/div[6]/label/span")
    private WebElement trainBlueColor;

    @FindBy(xpath = "//div/div[7]/label/span")
    private WebElement trainGreenColor;

    @FindBy(xpath = "//div/div[2]/label/span")
    private WebElement indigoColor;

    @Step
    public void clickOnSettingButton(){
        settingButton.click();
    }
    @Step
    public void writeCurrentPassword(String currentPassword){
        currentPasswordInput.sendKeys(currentPassword);
    }
    @Step
    public boolean checkPresentsOfSettingsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfSettingsPage));
        return presentsOfSettingsPage.isDisplayed();
    }
    @Step
    public void writeNewPassword(String newPassword){
        newPasswordInput.sendKeys(newPassword);
    }
    @Step
    public void writeConfirmNewPassword(String confirmNewPassword){
        confirmNewPasswordInput.sendKeys(confirmNewPassword);
    }
    @Step
    public void clickOnSaveChangesButton(){
        saveChangesButton.click();
        driver.navigate().refresh();
    }
    @Step
    public void writeNameOfUser(String nameOfUser){
        nameOfUserInput.sendKeys(nameOfUser);
    }
    @Step
    public void writeEmailOfUser(String emailOfUser){
        emailOfUserInput.sendKeys(emailOfUser);
    }
    @Step
    public void clearEmailOfUser(){
        emailOfUserInput.clear();
    }
    @Step
    public String getNameOfUser(){
        return nameOfUserInput.getAttribute("value");
    }
    @Step
    public void clearNameOfUserField(){
        nameOfUserInput.clear();
    }
    @Step
    public String getEmailOfUser(){
        return emailOfUserInput.getAttribute("value");
    }
    @Step
    public void clickOnCustomizationSettingButton(){
        customizationSettingButton.click();
    }
    @Step
    public void clickOnDarkBlueCheckbox(){
        Actions actions = new Actions(driver);
        actions.moveToElement(darkBlueColorCheckbox);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectBlueColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(blueColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectLightBlueColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(lightBlueColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectAquaColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(aquaColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectTrainBlueColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(trainBlueColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectTrainGreenColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(trainGreenColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void selectIndigoColor(){
        Actions actions = new Actions(driver);
        actions.moveToElement(indigoColor);
        actions.click();
        actions.build().perform();
    }
    @Step
    public void clickOnSaveCustomizationButton(){
        saveCustomizationButton.click();
    }

}
