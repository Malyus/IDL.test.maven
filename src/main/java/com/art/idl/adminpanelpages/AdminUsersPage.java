package com.art.idl.adminpanelpages;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 12/6/2017.
 */
public class AdminUsersPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//ul[@class='nav navbar-nav side-nav']//li[8]")
    private WebElement usersMenuButton;

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement presentsOfUsersPage;

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement createNewUserButton;

    @FindBy(xpath = "//input[@name='first_name']")
    private WebElement presentsOfAddNewUserPage;

    @FindBy(xpath = "//input[@name='first_name']")
    private WebElement nameInput;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//button[@class='btn-main btn-save']")
    private WebElement saveUserButton;

    @FindBy(xpath = "//div[@class='input text required error']//div")
    private WebElement errorMessageOfNameField;

    @FindBy(xpath = "//div[@class='input email required error']//div")
    private WebElement errorMessageOfEmailField;

    @FindBy(xpath = "//div[@class='input password required error']//div")
    private WebElement errorMessageOfPasswordField;

    @FindAll(@FindBy(xpath = "//p[@class='user-name']"))
    private List<WebElement> nameListOfCurrentUsers;

    @FindAll(@FindBy(xpath = "//div[@class='radio-checker'][1]//span"))
    private List<WebElement> listOfSuperAdminCheckboxes;

    @FindAll(@FindBy(xpath = "//div[@class='radio-checker'][2]//span"))
    private List<WebElement> listOfManagerCheckboxes;

    @FindBy(xpath = "(//span[@class='radio-border'])[2]")
    private WebElement managerButtonOnAddNewUserPage;

    @FindAll(@FindBy(xpath = "//button[@class='btn-main btn-delete btn-with-icon']"))
    private List<WebElement> listOfDeleteButtons;

    @FindBy(xpath = "//i[@class='fa fa-pencil']")
    private List<WebElement> listOfEditUserButton;

    @FindAll(@FindBy(xpath = "//p[@class='user-name']"))
    private List<WebElement> emailOfCreatedUser;

    @FindBy(xpath = "//i[@class='fa fa-fw fa-sign-out']")
    private WebElement logOutButton;

    @FindBy(xpath = "(//span[@class='radio-border'])[1]")
    private WebElement adminrButtonOnAddNewUserPage;

    @FindAll(@FindBy(xpath = "//span[@class='radio-border']"))
    private List<WebElement> managerCreatedUserButton;

    @FindAll(@FindBy(xpath = "//span[@class='radio-border']"))
    private List<WebElement> adminCreatedUserButton;

    @Step
    public void clickOnUsersMenuButton(){
        usersMenuButton.click();
    }
    @Step
    public boolean checkIfUsersPageIsPresent(){
        return presentsOfUsersPage.isDisplayed();
    }
    @Step
    public void clickOnCreateNewUserButton(){
        createNewUserButton.click();
    }
    @Step
    public boolean checkIfAddNewUserPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfAddNewUserPage));
        return presentsOfAddNewUserPage.isDisplayed();
    }
    @Step
    public void writeNameOfNewUser(String nameOfUser){
        nameInput.sendKeys(nameOfUser);
    }
    @Step
    public void writeEmailOfNewUser(String emailOfUser){
        emailInput.sendKeys(emailOfUser);
    }
    @Step
    public void writePasswordOfNewUser(String passwordOfUser){
        passwordInput.sendKeys(passwordOfUser);
    }
    @Step
    public void clickOnSaveUserButton(){
        saveUserButton.click();
    }
    @Step
    public String getTextOfErrorMessageOfNameField(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfNameField));
        return errorMessageOfNameField.getText();
    }
    @Step
    public String getTextOfErrorMessageOfEmailField(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfEmailField));
        return errorMessageOfEmailField.getText();
    }
    @Step
    public String getTextOfErrorMessageOfPasswordField(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfPasswordField));
        return errorMessageOfPasswordField.getText();
    }
    @Step
    public boolean checkPresentsOfNameErrorMessage(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfNameField));
        return errorMessageOfNameField.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfEmailErrorMessage(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfEmailField));
        return errorMessageOfEmailField.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfPasswordErrorMessage(){
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfPasswordField));
        return errorMessageOfPasswordField.isDisplayed();
    }
    @Step
    public String getColorOfNameField() throws InterruptedException {
      wait.until(ExpectedConditions.visibilityOf(errorMessageOfNameField));
      Thread.sleep(1000);
      return nameInput.getCssValue("background");
    }
    @Step
    public String getColorOfEmailField() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfEmailField));
        Thread.sleep(1000);
        return emailInput.getCssValue("background");
    }
    @Step
    public String getColorOfPasswordField() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(errorMessageOfPasswordField));
        Thread.sleep(1000);
        return emailInput.getCssValue("background");
    }
    @Step
    public String checkPresentsOfCreatedUserOnUsersPage(String createdUser){
        wait.until(ExpectedConditions.visibilityOf(createNewUserButton));
        for (int i = 0;i<nameListOfCurrentUsers.size();i++){
            System.out.println("Name of user" + "[" +i+"]" + nameListOfCurrentUsers.get(i).getText() );
           if(nameListOfCurrentUsers.get(i).getText().equalsIgnoreCase(createdUser)){
               createdUser = nameListOfCurrentUsers.get(i).getText();
           }
        }
        return createdUser;
    }
    @Step
    public String checkIfCreatedUserIsWithSuperAdminType(){
        wait.until(ExpectedConditions.visibilityOf(createNewUserButton));
        boolean createdUserType = false;
        String adminColor = "";
        adminColor = listOfSuperAdminCheckboxes.get(listOfSuperAdminCheckboxes.size()-1).getCssValue("border-color");
        return adminColor;
    }
    @Step
    public String checkIfCreatedUserIsWithManagerType(){
        wait.until(ExpectedConditions.visibilityOf(createNewUserButton));
        String managerColor = "";
        managerColor = listOfManagerCheckboxes.get(listOfManagerCheckboxes.size()-1).getCssValue("border-color");
        return managerColor;
        }
    @Step
    public void clickOnManagerButtonOnAddUserPage(){
        managerButtonOnAddNewUserPage.click();
    }
    @Step
    public void clickOnAdminButtonOnAddUserPage(){
        adminrButtonOnAddNewUserPage.click();
    }
    @Step
    public void deleteCreatedUser(){
        wait.until(ExpectedConditions.visibilityOf(createNewUserButton));
        listOfDeleteButtons.get(listOfDeleteButtons.size()-1).click();
        driver.switchTo().alert().accept();
    }
    @Step
    public boolean checkIfDeletedUserIsNotDisplayed(String nameOfDeletedUser){
        boolean presentOfDeletedUser = true;
        for(int i = 0;i<nameListOfCurrentUsers.size();i++){
            if(nameListOfCurrentUsers.get(i).getText().equals(nameOfDeletedUser)){
                presentOfDeletedUser = false;
                System.out.println("User is not deleted!");
            }
        }
        return presentOfDeletedUser;
    }
    @Step
    public void clickOnEditCreatedUser(){
        wait.until(ExpectedConditions.visibilityOf(createNewUserButton));
        listOfEditUserButton.get(listOfDeleteButtons.size()-1).click();
    }
    @Step
    public void clearNameOfUser(){
        nameInput.clear();
    }
    @Step
    public String getTextOfUserName(){
        return nameInput.getAttribute("value");
    }
    @Step
    public void clearEmailOfUser(){
        emailInput.clear();
    }
    @Step
    public String getTextOfUserEmail(){
        return emailOfCreatedUser.get(emailOfCreatedUser.size()-1).getText();
    }
    @Step
    public void clickOnLogOutButton(){
        logOutButton.click();
    }
    @Step
    public void clickOnManagerCreatedUserButton(){
        managerCreatedUserButton.get(managerCreatedUserButton.size()-1).click();
    }
    @Step
    public void clickOnAdminCreatedUserButton(){
        adminCreatedUserButton.get(adminCreatedUserButton.size()-1).click();
    }
    @Step
    public String checkIfAdminCheckboxOnEditUserPageIsSelected(){
        return adminrButtonOnAddNewUserPage.getCssValue("border-color");
    }
    @Step
    public String checkIfManagerCheckboxOnEditUserPageIsSelected(){
        return managerButtonOnAddNewUserPage.getCssValue("border-color");
    }
    @Step
    public void refreshPage(){
        driver.navigate().refresh();
    }

//div[@class='radio-checker'][1]//span

}
