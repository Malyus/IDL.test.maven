package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;

/**
 * Created by M.Malyus on 11/10/2017.
 */
public class OriginalDriverLicensePage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private LinkReader linkReader = new LinkReader();
    private String linkOfFirstStep = linkReader.getLinkOfFirstStep();
    private String linkOfThirdStep = linkReader.getLinkOfThirdStep();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//input[@id=\"license-number\"]")
    private WebElement presentsOfOriginalDriverLicenseStepPage;

    @FindBy(xpath = "//div[@class=\"progress-bar\"]")
    private WebElement progressBarLine;

    @FindBy(xpath = "//input[@id=\"license-number\"]")
    private WebElement licenseNumberField;

    @FindBy(xpath = "//div[@id=\"exp-month\"]")
    private WebElement expirationOpenButtonMonth;

    @FindBy(xpath = "//div[@id=\"exp-month\"]//ul//li[1]//a")
    private WebElement firstOfMonthExpiration;

    @FindBy(xpath = "//div[@id=\"exp-day\"]")
    private WebElement expirationOpenButtonDay;

    @FindBy(xpath = "//div[@id=\"exp-day\"]//ul//li[1]//a")
    private WebElement firsOfDayExpiration;

    @FindBy(xpath = "//div[@id=\"exp-year\"]")
    private WebElement expirationOpenButtonYear;

    @FindBy(xpath = "//div[@id=\"exp-year\"]//ul//li[1]//a")
    private WebElement firstOYearExpiration;

    @FindBy(xpath = "//div[@id=\"firt-issue-month\"]")
    private WebElement firstIssueOfMonthOpenButton;

    @FindBy(xpath = "//div[@id=\"firt-issue-month\"]//ul//li[1]//a")
    private WebElement firstOfMonthIssue;

    @FindBy(xpath = "//div[@id=\"firt-issue-day\"]")
    private WebElement firstIssuedOfDayOpenButton;

    @FindBy(xpath = "//div[@id=\"firt-issue-day\"]//ul//li[1]//a")
    private WebElement firsOfDayIssue;

    @FindBy(xpath = "//div[@id=\"firt-issue-year\"]")
    private WebElement firstIssueOfYearOpenButton;

    @FindBy(xpath = "//div[@id=\"firt-issue-year\"]//ul//li[1]//a")
    private WebElement firstOfYearIssue;

    @FindBy(xpath = "//input[@name='autoCountry']")
    private WebElement countryField;

    @FindBy(xpath = "//span[@class=\"highlight\"]")
    private WebElement countryAutoComplete;

    @FindBy(xpath = "//div[6]/label[2]/input")
    private WebElement uploadPhotoInput;

    @FindBy(xpath = "//div[5]/div/label[1]/label")
    private WebElement categoryAButton;

    @FindBy(xpath = "//div[5]/div/label[2]/label")
    private WebElement categoryBButton;

    @FindBy(xpath = "//div[5]/div/label[3]/label")
    private WebElement categoryCButton;

    @FindBy(xpath = "//div[5]/div/label[4]/label")
    private WebElement categoryDButton;

    @FindBy(xpath = "//div[5]/div/label[5]/label")
    private WebElement categoryEButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue btn-top\"]")
    private WebElement countinueButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement countinueButtonForValid;

    @FindBy(xpath = ".//*[@id='ul-8']/li")
    private WebElement invalidFormatIntoCountryField;

    @FindBy(xpath = "//div[6]/div/img")
    private WebElement presentOfJPGFormat;

    @FindBy(xpath = "//div[6]/div/img")
    private WebElement presentOfJPEGFormat;

    @FindBy(xpath = "//label[@class='custom-dropzone']//label")
    private WebElement presentsOfChangePhotoNotUploadPhoto;

    @FindBy(xpath = "//button[@class=\"btn-crop vanilla-result\"]")
    private WebElement presentsOFThirdStepCrop;

    @FindBy(xpath = "//span[@class=\"err-message\"]")
    private WebElement errorMessageWhenNotUploadedPhoto;

    @FindBy(xpath = "//div[6]/div[1]/div")
    private WebElement uploadErrorMessage;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement xButton;

    @Step
    public boolean checkIfOriginalDriverLicenseIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfOriginalDriverLicenseStepPage));
        return presentsOfOriginalDriverLicenseStepPage.isDisplayed();
    }
    @Step
    public String checkProgressBarOfOriginalDriverLicensePage(){
        wait.until(ExpectedConditions.visibilityOf(licenseNumberField));
        return progressBarLine.getAttribute("style");
    }
    @Step
    public void writeLicenseNumber(String licenseNumber){
        wait.until(ExpectedConditions.visibilityOf(licenseNumberField));
        licenseNumberField.sendKeys(licenseNumber);
    }
    @Step
    public void chooseFirstMonthExpirationDate()   {
        expirationOpenButtonMonth.click();
        wait.until(ExpectedConditions.visibilityOf(firstOfMonthExpiration));
        firstOfMonthExpiration.click();
    }
    @Step
    public void chooseFirstDayExpirationDate(){
        expirationOpenButtonDay.click();
        wait.until(ExpectedConditions.visibilityOf(firsOfDayExpiration));
        firsOfDayExpiration.click();
    }
    @Step
    public void chooseFirstYearExpirationDate(){
     expirationOpenButtonYear.click();
     wait.until(ExpectedConditions.visibilityOf(firstOYearExpiration));
     firstOYearExpiration.click();
    }
    @Step
    public void chooseFirstIssueMonth(){
        firstIssueOfMonthOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(firstOfMonthIssue));
        firstOfMonthIssue.click();
    }
    @Step
    public void chooseFirstIssueDate(){
        firstIssuedOfDayOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(firsOfDayIssue));
        firsOfDayIssue.click();
    }
    @Step
    public void chooseFirstIssueYear(){
     firstIssueOfYearOpenButton.click();
     wait.until(ExpectedConditions.visibilityOf(firstIssueOfYearOpenButton));
     firstOfYearIssue.click();
    }
    @Step
    public void writeCountryOfDriverLicense(String countryOfDriverLicense) throws InterruptedException {
        Thread.sleep(3000);
        countryField.sendKeys(countryOfDriverLicense);
        Actions actions = new Actions(driver);
        actions.moveToElement(countryAutoComplete).click().perform();
    }
    @Step
    public void uploadPhotoOfDriverLicense(){
      uploadPhotoInput.sendKeys((new File("./1.jpg").getAbsolutePath()));
    }

    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(licenseNumberField));
        backButton.click();
    }

    @Step
    public void clickOnCountinueButton() throws InterruptedException {
        try {
            wait.until(ExpectedConditions.visibilityOf(countinueButton));
            Thread.sleep(1000);
            Actions actions = new Actions(driver);
            actions.moveToElement(countinueButton).click().perform();
            countinueButton.click();
        }catch (UnhandledAlertException f){
            try {
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("Alert text:" + alertText);
                alert.accept();
            }catch (NoAlertPresentException e){
                e.printStackTrace();
            }
        }
    }
    @Step
    public void clickOnCountinueButtonWhenAllWithValid() throws InterruptedException {
        try {
            wait.until(ExpectedConditions.visibilityOf(countinueButtonForValid));
            Thread.sleep(1000);
            Actions actions = new Actions(driver);
            actions.moveToElement(countinueButtonForValid).click().perform();
        }catch (UnhandledAlertException f){
            try {
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("Alert text:" + alertText);
                alert.accept();
            }catch (NoAlertPresentException e ){
                e.printStackTrace();
            }
        }

    }
    @Step
    public void chooseACategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryAButton).click().perform();
    }
    @Step
    public void disableACategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryAButton).click().perform();
    }
    @Step
    public void chooseBCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryBButton).click().perform();
    }
    @Step
    public void disableBCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryBButton).click().perform();
    }
    @Step
    public void chooseCCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryCButton).click().perform();
    }
    @Step
    public void disableCCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryCButton).click().perform();
    }
    @Step
    public void chooseDCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryDButton).click().perform();
    }
    @Step
    public void disableDCategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryDButton).click().perform();
    }
    @Step
    public void chooseECategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryEButton).click().perform();
    }
    @Step
    public void disableECategory(){
        Actions actions = new Actions(driver);
        actions.moveToElement(categoryEButton).click().perform();
    }
    @Step
    public boolean checkIfInvalidValueCantBeInCountry(){
        wait.until(ExpectedConditions.visibilityOf(invalidFormatIntoCountryField));
        return invalidFormatIntoCountryField.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfJPGFormat(){
        wait.until(ExpectedConditions.visibilityOf(presentOfJPGFormat));
        return presentOfJPGFormat.isDisplayed();
    }
    @Step
    public void uploagJPEGFormat(){
        uploadPhotoInput.sendKeys((new File("./2.jpeg").getAbsolutePath()));
    }
    @Step
    public boolean checkPresentsOfJPEGForamat(){
        wait.until(ExpectedConditions.visibilityOf(presentOfJPEGFormat));
        return presentOfJPEGFormat.isDisplayed();
    }
    @Step
    public String checkIfUploadChangeToChangePhotoButton(){
        /*Wait until will be visible
        * and wait until text will change*/
        wait.until(ExpectedConditions.visibilityOf(presentsOfChangePhotoNotUploadPhoto));
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//label[@class='custom-dropzone']//label"),"Change Photo"));
        return presentsOfChangePhotoNotUploadPhoto.getText();
    }
    @Step
    public boolean checkPresentsOfThirdStep(){
        wait.until(ExpectedConditions.visibilityOf(presentsOFThirdStepCrop));
        return presentsOFThirdStepCrop.isDisplayed();
    }
    @Step
    public String getCssValueOfLicenseNumberField(){
        return licenseNumberField.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfExpirationMonth(){
        return expirationOpenButtonMonth.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfExpirationDay(){
        return expirationOpenButtonDay.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfExpirationYear(){
        return expirationOpenButtonYear.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfLicenseMonth(){
        return firstIssueOfMonthOpenButton.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfLicenseDay(){
        return firstIssuedOfDayOpenButton.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfLicenseYear(){
        return firstIssueOfYearOpenButton.getCssValue("border-color");
    }
    @Step
    public String getTextOfImageUploadErroMessage(){
        return errorMessageWhenNotUploadedPhoto.getText();
    }
    @Step
    public String getCssValueOfCountryInput(){
        return countryField.getCssValue("border-colo");
    }
    @Step
    public String getUploadErrorMessage(){
        return uploadErrorMessage.getText();
    }
    @Step
    public void goToFirstStep(){
        System.out.println("Link of firstStep"+linkOfFirstStep);
       driver.get(linkOfFirstStep);
       driver.navigate().to(linkOfFirstStep);
    }
    @Step
    public void goToThirdStepByURL(){
        driver.navigate().to(linkOfThirdStep);
        /*driver.get(linkOfThirdStep);*/
    }
    @Step
    public String getCurrentURL() throws InterruptedException {
        Thread.sleep(1500);
        return driver.getCurrentUrl();
    }
    @Step
    public void clickOnXButton(){
        wait.until(ExpectedConditions.visibilityOf(licenseNumberField));
        xButton.click();
    }
    @Step
    public void uploadXMLFormat(){
        uploadPhotoInput.sendKeys((new File("./testng.xml").getAbsolutePath()));
    }
    @Step
    public String checkTextOfValidFormats(){
        Alert validFormatsAlert = driver.switchTo().alert();
        return validFormatsAlert.getText();
    }
    @Step
    public void acceptMessageWithValidFormats(){
        Alert validFormatsAlert = driver.switchTo().alert();
        validFormatsAlert.accept();
    }

}
