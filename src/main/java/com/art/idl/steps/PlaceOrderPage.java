package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class PlaceOrderPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private LinkReader linkReader = new LinkReader();
    private String fifthStep = linkReader.getLinkOfFifthStep();
    private String printDetails = linkReader.getLinkOfPrintDetailsStep();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//label[@id=\"expiration-date\"]")
    private WebElement presentsOfPlaceOrderPage;

    @FindBy(xpath = "//div[@class=\"progress-bar\"]")
    private WebElement progressBar;

    @FindBy(xpath = "//label[@id=\"license-number\"]")
    private WebElement licenseNumberValue;

    @FindBy(xpath = "//label[@id=\"expiration-date\"]")
    private WebElement expirationDateValue;

    @FindBy(xpath = "//label[@id=\"first-issue\"]")
    private WebElement firstIssueDateValue;

    @FindBy(xpath = "//label[@id=\"country\"]")
    private WebElement countryValue;

    @FindBy(xpath = "//label[@id=\"categories\"]")
    private WebElement categoriesValue;

    @FindBy(xpath = "//label[@id=\"full-name-p\"]")
    private WebElement fullName;

    @FindBy(xpath = "//label[@id=\"country-p\"]")
    private WebElement personalDetailsCountry;

    @FindBy(xpath = "//label[@id=\"street-addr-p\"]")
    private WebElement streetAddress;

    @FindBy(xpath = "//label[@id=\"city-p\"]")
    private WebElement city;

    @FindBy(xpath = "//label[@id=\"state-p\"]")
    private WebElement state;

    @FindBy(xpath = "//label[@id=\"zip-p\"]")
    private WebElement zip;

    @FindBy(xpath = "//label[@id=\"email-p\"]")
    private WebElement email;

    @FindBy(xpath = "//label[@id=\"full-name-s\"]")
    private WebElement fullNameShipping;

    @FindBy(xpath = "//label[@id='country-s0']")
    private WebElement countryShipping;

    @FindBy(xpath = "//label[@id='street-addr-s0']")
    private WebElement streetAddressShipping;

    @FindBy(xpath = "//label[@id='city-s0']")
    private WebElement cityShipping;

    @FindBy(xpath = "//label[@id='state-s0']")
    private WebElement stateShipping;

    @FindBy(xpath = "//label[@id='zip-s0']")
    private WebElement zipShipping;

    @FindBy(xpath = "//div[4]/div[1]/div/label[1]")
    private WebElement insuranceButton;

    @FindBy(xpath = "//input[@id=\"insurance\"]")
    private WebElement activeOfInsuranceButton;

    @FindBy(xpath = "//div[4]/div[1]/div/label[2]")
    private WebElement additionalLicenseButton;

    @FindBy(xpath = "//input[@id=\"copy-license\"]")
    private WebElement activeOfAdditionalLicenseButton;

    @FindBy(xpath = "//div[4]/div[1]/div/label[3]")
    private WebElement additionalBookletButton;

    @FindBy(xpath = "//input[@id=\"copy-booklet\"]")
    private WebElement activeOfAdditionalBookletButton;

    @FindBy(xpath = "//div[2]/div/label[2]/label/label[1]")
    private WebElement secondCheckbox;

    @FindBy(xpath = "//input[@id=\"2\"]")
    private WebElement activeOfSecondCheckbox;

    @FindBy(xpath = "//input[@id=\"1\"]")
    private WebElement activeOfFirstCheckbox;

    @FindBy(xpath = "//span[@class=\"added-service ng-binding\"]")
    private WebElement presentsOfInsuranseInYourOrder;

    @FindBy(xpath = "//span[@id=\"shipping-price\"]")
    private WebElement presentsOfSecondCheckboxValueInYourOrder;

    @FindBy(xpath = "//div[4]/label/span")
    private WebElement orderTotal;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement continueButton;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement XButton;

    @FindBy(xpath = "//label[@id=\"order-number\"]")
    private WebElement presentsOfPrintDetailsPage;

    @FindAll(@FindBy(xpath = "//label[@class=\"col-xs-12 col-sm-12 ng-hide\"]"))
    private List<WebElement> hiddenInsurancesInYourOrder;

    @Step
    public boolean checkPresentsOfPlaceOrderPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPlaceOrderPage));
        return presentsOfPlaceOrderPage.isDisplayed();
    }
    @Step
    public String checkForValidProgressBar(){
        return progressBar.getAttribute("style");
    }
    @Step
    public String getLicenseNumberValue(){
        return licenseNumberValue.getText();
    }
    @Step
    public String getExpirationDateValue(){
        return expirationDateValue.getText();
    }
    @Step
    public String getFirstIssueDateValue(){
        return firstIssueDateValue.getText();
    }
    @Step
    public String getCountryValue(){
        return countryValue.getText();
    }
    @Step
    public String getCategoriesValue(){
        return categoriesValue.getText();
    }
    @Step
    public String getFullNameValue(){
        return fullName.getText();
    }
    @Step
    public String getPersonalDetailsCountryValue(){
        return personalDetailsCountry.getText();
    }
    @Step
    public String getStreetAddressValue(){
        return streetAddress.getText();
    }
    @Step
    public String getCityValue(){
        return city.getText();
    }
    @Step
    public String getStateValue(){
        return state.getText();
    }
    @Step
    public String getZipValue(){
        return zip.getText();
    }
    @Step
    public String getEmailValue(){
        return email.getText();
    }
    @Step
    public String getFullNameShippingValue(){
        return fullNameShipping.getText();
    }
    @Step
    public String getCountryShippingValue(){
        return countryShipping.getText();
    }
    @Step
    public String getStreetAddressShippingValue(){
        return streetAddressShipping.getText();
    }
    @Step
    public String getCityShippingValue(){
        return cityShipping.getText();
    }
    @Step
    public String getStateShippingValue(){
        return stateShipping.getText();
    }
    @Step
    public String getZipShippingValue(){
        return zipShipping.getText();
    }
    @Step
    public void clickOnInsuranceButton(){
        insuranceButton.click();
    }
    @Step
    public String checkIfInsuranceIsActivated(){
        return activeOfInsuranceButton.getAttribute("ng-checked");
    }
    @Step
    public String checkIfInsuranceIsDeactivated(){
        return activeOfInsuranceButton.getAttribute("ng-checked");
    }
    @Step
    public void clickOnAdditionalLicenseButton(){
        additionalLicenseButton.click();
    }
    @Step
    public String checkIfAdditionalLicenseIsActivated(){
        return activeOfAdditionalLicenseButton.getAttribute("ng-checked");
    }
    @Step
    public String checkIfAdditionalLicenseIsDeactivated(){
        return activeOfAdditionalLicenseButton.getAttribute("ng-checked");
    }
    @Step
    public void clickOnAdditionalBookletButton(){
        additionalBookletButton.click();
    }
    @Step
    public String checkIfAdditionalBookletIsActivated(){
        return activeOfAdditionalBookletButton.getAttribute("ng-checked");
    }
    @Step
    public String checkIfAdditionalBookletIsDeactivated(){
        return activeOfAdditionalBookletButton.getAttribute("ng-checked");
    }
    @Step
    public void clickOnSecondCheckbox(){
        secondCheckbox.click();
    }
    @Step
    public String checkActiveOfSecondCheckbox(){
        return activeOfSecondCheckbox.getAttribute("class");
    }
    @Step
    public String checkActiveOfFirstCheckbox(){
        return activeOfFirstCheckbox.getAttribute("class");
    }
    @Step
    public boolean checkPresentsOfInsuranceInYourOrderAfterItActivating(){
        return presentsOfInsuranseInYourOrder.isDisplayed();
    }
    @Step
    public String getValueFromYourOrderSecondCheckbox(){
        return presentsOfSecondCheckboxValueInYourOrder.getText();
    }
    @Step
    public String getOrderTotalValue(){
        return orderTotal.getText();
    }
    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(insuranceButton));
        backButton.click();
    }
    @Step
    public void clickOnContinueButton(){
        /*try {
            continueButton.click();
        } catch (UnhandledAlertException f) {
            try {
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("UnhadleAlert:" + alertText);
                alert.accept();

            } catch (NoAlertPresentException e) {
                e.printStackTrace();
            }
        }*/
        continueButton.click();
    }
    @Step
    public void clickOnXButton(){
        wait.until(ExpectedConditions.visibilityOf(fullName));
        XButton.click();
    }
    @Step
    public void goToFifthStepByURL(){
        wait.until(ExpectedConditions.visibilityOf(fullName));
        driver.get(fifthStep);
    }
    @Step
    public void goToPrintDetailsStepByURL(){
        wait.until(ExpectedConditions.visibilityOf(fullName));
        driver.get(printDetails);
        driver.navigate().to(printDetails);
    }
    @Step
    public boolean checkPresentsOfPrintDetailsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPrintDetailsPage));
        return presentsOfPrintDetailsPage.isEnabled();
    }
    @Step
    public String getAriaHiddenOfInsurancesOfYourOrder(int index){
        return hiddenInsurancesInYourOrder.get(index).getAttribute("class");
    }
}
