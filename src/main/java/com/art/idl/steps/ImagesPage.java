package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;

/**
 * Created by M.Malyus on 11/13/2017.
 */
public class ImagesPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private LinkReader linkReader = new LinkReader();
    private String fourthStep = linkReader.getLinkOfFourthStep();
    private String secondStep = linkReader.getLinkOfSecondStep();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//button[@class=\"btn-crop vanilla-result\"]")
    private WebElement presentsOfImagesPage;

    @FindBy(xpath = "//div[@class=\"progress-bar\"]")
    private WebElement progressBarLevel;

    @FindBy(xpath = "//input[@id=\"upload\"]")
    private WebElement uploadInput;

    @FindBy(xpath = "//div[@class=\"signature ng-isolate-scope\"]//canvas")
    private WebElement digitalSignature;

    @FindBy(xpath = "//canvas[@class=\"cr-image\"]")
    private WebElement uploadedImage;

    @FindBy(xpath = "//label[@class=\"drop-text\"]")
    private WebElement textOfUploadButton;

    @FindBy(xpath = "//button[@class=\"btn-rotate rotate-left vanilla-rotate\"]")
    private WebElement leftRotateButton;

    @FindBy(xpath = "//button[@class=\"btn-rotate vanilla-rotate\"]")
    private WebElement rightRotateButton;

    @FindBy(xpath = "//canvas[@class=\"cr-image\"]")
    private WebElement stylesOfRotatedPhoto;

    @FindBy(xpath = "//button[@class=\"btn-crop vanilla-result\"]")
    private WebElement cropButton;

    @FindBy(xpath = "//img[@class=\"result-img\"]")
    private WebElement cropPhotoImage;

    @FindBy(xpath = "//button[@class=\"btn-crop btn-sign\"]")
    private WebElement saveYourSignatureButton;

    @FindBy(xpath = "//div[@class=\"result\"]")
    private WebElement presentsOfSaveSignatureImage;

    @FindBy(xpath = "//button[@class=\"btn-clear\"]")
    private WebElement clearButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue btn-top\"]")
    private WebElement countinueButton;

    //Old selector!
    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement con;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement backButton;

    @FindBy(xpath = "//input[@id=\"license-number\"]")
    private WebElement presentsOfPreviousStep;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement countinueButtonForValid;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement XButton;

    @Step
    public boolean checkPresentsOfImagesPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfImagesPage));
        return presentsOfImagesPage.isDisplayed();
    }
    @Step
    public String checckProgressBarLevel(){
        return progressBarLevel.getAttribute("style");
    }
    @Step
    public void uploadJPEGOfYourSelf(){
        uploadInput.sendKeys((new File("./2.jpeg").getAbsolutePath()));
    }
    @Step
    public void uploadJPGOfYourSelf(){
        uploadInput.sendKeys((new File("./3.jpg").getAbsolutePath()));
    }
    @Step
    public boolean checkPresentsOfUploadedImage(){
        return uploadedImage.isDisplayed();
    }
    @Step
    public String getTextFromUploadButton(){
        return textOfUploadButton.getText();
    }
    @Step
    public String checkIfLeftButtonRotateToTheLeft(){
        return stylesOfRotatedPhoto.getAttribute("width");
    }
    @Step
    public String checkIfLeftButtonRotateToTheRight(){
        return stylesOfRotatedPhoto.getAttribute("width");
    }
    @Step
    public void clickOnLeftButton(){
        leftRotateButton.click();
    }
    @Step
    public void clickOnRightButton(){
        rightRotateButton.click();
    }
    @Step
    public void clickOnCropButton() throws InterruptedException {
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].removeAttribute('disabled','disabled')",cropButton);
        //Wait for js execute by Thread.sleep(  :milliseconds) update or reverse!
        //Thread.sleep(2000);

     cropButton.click();
    }
    @Step
    public boolean checkIfCropPhotoPresent(){
        wait.until(ExpectedConditions.visibilityOf(cropPhotoImage));
        return cropPhotoImage.isDisplayed();
    }
    @Step
    public void clickOnSaveYourSignatureButton(){
        saveYourSignatureButton.click();
    }
    @Step
    public boolean checkIfSaveSignatureIsDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfSaveSignatureImage));
        return presentsOfSaveSignatureImage.isDisplayed();
    }
    @Step
    public void clickOnClearButton(){
        clearButton.click();
    }
    @Step
    public boolean checkIfDigitalSignatureBecomeClearAndCantBeSave(){
        countinueButton.click();
        wait.until(ExpectedConditions.visibilityOf(saveYourSignatureButton));
        return saveYourSignatureButton.isDisplayed();
    }
    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfImagesPage));
        backButton.click();
    }
    @Step
    public boolean checkIfBackButtonReturnToPreviousStep(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPreviousStep));
        return presentsOfPreviousStep.isDisplayed();
    }
    //Con
    @Step
    public void clickOnCon(){
        con.click();
    }
    @Step
    public void clickOnValidContinue(){
        try {
            /*Use js click because there are two
            * continue buttons - sometimes steps
            * are not validates and make a waiting
            * because js is very fast*/
            Thread.sleep(1000);
            String script = "var object = arguments[0];"
                    + "var theEvent = document.createEvent(\"MouseEvent\");"
                    + "theEvent.initMouseEvent(\"click\", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
                    + "object.dispatchEvent(theEvent);"
                    ;
            ((JavascriptExecutor)driver).executeScript(script, countinueButtonForValid);
            //countinueButtonForValid.click();
        }catch (UnhandledAlertException f){

        }

        catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            countinueButton.click();
        }
    }
    @Step
    public void clickOnContinueButton(){
        try {
            countinueButton.click();
        }catch (UnhandledAlertException f){
            try {
                Alert  alert = driver.switchTo().alert();
                String alertText = alert.getText();
                System.out.println("UnhandleText:" + alertText);
                alert.accept();
            }catch (NoAlertPresentException e){
                e.printStackTrace();
            }
        }
    }
    @Step
    public void makeADigitalSignature(){
        wait.until(ExpectedConditions.visibilityOf(digitalSignature));
        digitalSignature.click();
//        Actions builder = new Actions(driver);
//        Action drawAction = builder.moveToElement(element,135,15) //start points x axis and y axis.
//                .click()
//                .moveByOffset(200, 60) // 2nd points (x1,y1)
//                .click()
//                .moveByOffset(100, 70)// 3rd points (x2,y2)
//                .doubleClick()
//                .build();
//        drawAction.perform();
    }
    @Step
    public void clickOnXButton(){
        wait.until(ExpectedConditions.visibilityOf(rightRotateButton));
        XButton.click();
    }
    @Step
    public void goToSecondStepByLink(){
        System.out.println("Link:" + secondStep);
        driver.get(secondStep);
        driver.navigate().to(secondStep);

    }
    @Step
    public void goToFourthStepByLink(){
        driver.get(fourthStep);
    }

    @Step
    public String getCurrentURL() throws InterruptedException {
       Thread.sleep(1500);
       return driver.getCurrentUrl();
    }
    @Step
    public void uploadXMLFile() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(saveYourSignatureButton));
        Thread.sleep(1000);
        uploadInput.sendKeys((new File("./testng.xml").getAbsolutePath()));
    }
    @Step
    public void clickOnCropButtonWhenXML() throws InterruptedException {
        Thread.sleep(1000);
        cropButton.click();
    }
    @Step
    public String getMessageWithValidFormatsOfPhoto() {
        Alert validFormatMessageAlert = driver.switchTo().alert();
        return validFormatMessageAlert.getText();
    }
    @Step
    public void acceptValidMessage(){
        Alert validFormatmessageAlert = driver.switchTo().alert();
        validFormatmessageAlert.accept();
    }
}
