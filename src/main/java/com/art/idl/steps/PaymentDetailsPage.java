package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 11/16/2017.
 */
public class PaymentDetailsPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private LinkReader linkReader = new LinkReader();
    private String fourthStep = linkReader.getLinkOfFourthStep();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//label[@for='insurance']")
    private WebElement presentsOfPlaceOrderPage;

    @FindBy(xpath = "//label[@for='insurance']")
    private WebElement presentsOfPlaceOrder;

    @FindBy(xpath = "//div[@class=\"progress-bar\"]")
    private WebElement progressBar;

    @FindBy(xpath = "//form[@id='number-form']//input[1]")
    private WebElement creditCardInput;
    //div[2]/div[3]/div[1]/input[1] /*Old x-path*/
    @FindBy(xpath = "//input[@id='month']")
    private WebElement expDateMonthInput;
    //div[2]/div[3]/div[1]/input[2] /*Old x-path*/
    @FindBy(xpath = "//input[@id='year']")
    private WebElement expDateYearInput;

    @FindBy(xpath = "//input[@id='cvv']")
    private WebElement cvvInput;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement countinueButtonForValid;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue btn-top\"]")
    private WebElement countinueButton;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement XButton;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement con;

    @FindBy(xpath = "//div[@id='spreedly-number']//iframe")
    private WebElement iframe;

    @FindBy(xpath = "//div[@id='spreedly-cvv']//iframe")
    private WebElement cvvIframe;

    @FindBy(xpath = "//div[@style='width:100%']")
    private WebElement presentsOfPage;

    @Step
    public boolean checkPresentsOfPaymentDetailsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPage));
        return presentsOfPage.isDisplayed();
    }
    @Step
    public String checkProgressBarForValid(){
        return progressBar.getAttribute("style");
    }
    @Step
    public void writeCreditCardNumber(String creditCardNumber) throws InterruptedException {
        Thread.sleep(7000);
        driver.switchTo().frame(iframe);
        creditCardInput.sendKeys(creditCardNumber);
        driver.switchTo().defaultContent();
    }
    @Step
    public void writeExpDateMonth(String expDateMonth){
       expDateMonthInput.sendKeys(expDateMonth);
    }
    @Step
    public void writeExpDateYear(String expDateYear){
        expDateYearInput.sendKeys(expDateYear);
    }
    @Step
    public void writeCVV(String cvvValue) throws InterruptedException {
        Thread.sleep(1500);
        driver.switchTo().frame(cvvIframe);
        cvvInput.sendKeys(cvvValue);
        driver.switchTo().defaultContent();
    }
    @Step
    public void clickOnContinue(){
        try {
            wait.until(ExpectedConditions.visibilityOf(countinueButtonForValid));
            wait.until(ExpectedConditions.visibilityOf(countinueButton));
            countinueButton.click();
        }catch (UnhandledAlertException f){
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("UnhandleAlertText:" + alertText);
            alert.accept();
        }catch (NoAlertPresentException e){
            e.printStackTrace();
        }
    }
    @Step
    public void clickOnValidContinue(){
        try {
            wait.until(ExpectedConditions.visibilityOf(countinueButtonForValid));
            countinueButton.click();
        }catch (UnhandledAlertException f){
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("UnhandleAlertText:" + alertText);
            alert.accept();
        }catch (NoAlertPresentException e){
            e.printStackTrace();
        }
    }
    @Step
    public boolean checkPresentsOfPlaceOrderPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPlaceOrderPage));
        return presentsOfPlaceOrderPage.isDisplayed();
    }
    @Step
    public void clickOnBackButton(){
        backButton.click();
    }
    @Step
    public void clickonXButton(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPage));
        XButton.click();
    }
    @Step
    public void goToFourthStepByURL(){
        driver.get(fourthStep);
        driver.navigate().to(fourthStep);
    }
    @Step
    public String getCurrentURL() throws InterruptedException {
        Thread.sleep(1500);
        return driver.getCurrentUrl();
    }
    @Step
    public void switchToIframe(){
        driver.switchTo().frame(iframe);
    }
}
