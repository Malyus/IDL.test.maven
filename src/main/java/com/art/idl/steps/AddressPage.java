package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 11/14/2017.
 */
public class AddressPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private LinkReader linkReader = new LinkReader();
    private String thirdStep = linkReader.getLinkOfThirdStep();
    private String fifthStep = linkReader.getLinkOfFifthStep();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);
    private String firstCountry = "";
    private String secondCountry = "";

    @FindBy(xpath = "//div[@style='width:80%']")
    private WebElement presentsOfAddressPage;

    @FindBy(xpath = "//div[@class=\"progress-bar\"]")
    private WebElement progressBarLevel;

    @FindBy(xpath = "//div[2]/div[1]/span")
    private WebElement shippingCountryErrorMessage;

    @FindBy(xpath = "//md-autocomplete-wrap//input")
    private WebElement countryInput;

    @FindBy(xpath = "//li[@class=\"ng-scope\"]")
    private WebElement countrySelect;

    @FindAll({@FindBy(xpath = "//span[@md-highlight-text=\"ctrl.searchText\"]")})
    private List<WebElement> listOfCountries;

    @FindBy(xpath = "//div[not(@aria-hidden) and @class=\"address-block ng-scope layout-column\"]//input[@id=\"street\"]")
    private WebElement streetAddressFirstInput;

    @FindBy(xpath = "//div[not(@aria-hidden) and @class=\"address-block ng-scope layout-column\"]//input[@id=\"city\"]")
    private WebElement cityFirstInput;

    @FindBy(xpath = "//div[not(@aria-hidden) and @class=\"address-block ng-scope layout-column\"]//div[@layout=\"column\" ]//input")
    private WebElement stateInputFirst;

    @FindBy(xpath = "/html/body/md-virtual-repeat-container[2]/div")
    private WebElement firstStateFromFirstList;

    @FindBy(xpath = "//div[not(@aria-hidden) and @class=\"address-block ng-scope layout-column\"]//input[@id=\"zip-code\"]")
    private WebElement firstZipInput;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue\"]")
    private WebElement countinueButtonForValid;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-continue btn-top\"]")
    private WebElement countinueButton;

    @FindBy(xpath = "//span[@id='card-american']")
    private WebElement presentsOfPaymentDetails;

    @FindBy(xpath = "//label[@for='newaddress']//label")
    private WebElement shippingDetailsCheckbox;
    //aria-hidden
    @FindBy(xpath = "//div[@aria-hidden]")
    private WebElement presenetsOfShiipingDetailsSection;

    @FindBy(xpath = "//input[@tabindex='6']")
    private WebElement countryShippingInput;

    @FindBy(xpath = "html/body/md-virtual-repeat-container[3]/div/div[2]")
    private WebElement countryShippingSelect;

    @FindBy(xpath = "//input[@id='street-address2']")
    private WebElement streetAddressShippingInput;

    @FindBy(xpath = "//input[@id='city2']")
    private WebElement cityShippingInput;

    @FindBy(xpath = "//input[@tabindex='9']")
    private WebElement stateShippingInput;

    @FindBy(xpath = "/html/body/md-virtual-repeat-container[4]/div/div[2]")
    private WebElement stateListShipping;

    @FindBy(xpath = "//input[@id='zip-code2']")
    private WebElement zipShippingInput;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class=\"btn-crop vanilla-result\"]")
    private WebElement presentsOfImagesPage;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement XButton;

    @Step
    public boolean checkIfUserIsOnAddressPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfAddressPage));
        return presentsOfAddressPage.isDisplayed();
    }
    @Step
    public boolean checkIfAddressPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(cityFirstInput));
        return cityFirstInput.isEnabled();
    }
    @Step
    public String checckProgressBarLevel(){
        wait.until(ExpectedConditions.visibilityOf(progressBarLevel));
        return progressBarLevel.getAttribute("style");
    }
    @Step
    public void writeCountryInFirstSection(String firstCountry) throws InterruptedException {
        Thread.sleep(3000);
        wait.until(ExpectedConditions.visibilityOf(countryInput));
        countryInput.sendKeys(firstCountry);
    }
    @Step
    public void chooseCountryFromList(){
        wait.until(ExpectedConditions.visibilityOf(countrySelect));
        Actions actions = new Actions(driver);
        actions.moveToElement(countrySelect).click().perform();
    }
    @Step
    public void getAllCountriesFromList(String firstLetter){
      countryInput.sendKeys(firstLetter);

      for (int i = 0;i<listOfCountries.size();i++){
          System.out.println("Country number "+i+ ":"+listOfCountries.get(i).getText());
           firstCountry = listOfCountries.get(0).getText();
           secondCountry = listOfCountries.get(1).getText();
      }
        System.out.println("1:"+firstCountry);
        System.out.println("2:"+secondCountry);
    }
    public String getFirstCountry(){
        return firstCountry;
    }
    public String getSecondCountry(){
        return secondCountry;
    }
    @Step
    public void writeStreeAddressFirst(String firstAddress){
        streetAddressFirstInput.sendKeys(firstAddress);
    }
    @Step
    public void writeCityFirst(String firstCity){
        cityFirstInput.sendKeys(firstCity);
    }
    @Step
    public void writeFirstState(String firstState) throws InterruptedException {
        Thread.sleep(3000);
        wait.until(ExpectedConditions.visibilityOf(stateInputFirst));
        stateInputFirst.sendKeys(firstState);
    }
    @Step
    public void chooseFirstFromListStates(){
        Actions actions = new Actions(driver);
        actions.moveToElement(firstStateFromFirstList).click().perform();
    }
    @Step
    public void writeFirstZip(String firstZip){
        firstZipInput.sendKeys(firstZip);
    }
    @Step
    public void clickOnValidContinue() throws InterruptedException {
        try {
            countinueButtonForValid.click();
        }catch (UnhandledAlertException f){
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("UnhandleAlertText:" + alertText);
            alert.accept();
        }catch (NoAlertPresentException e){
            e.printStackTrace();
        }
        Thread.sleep(2500);
    }
    @Step
    public void clickOnContinueButton(){
        try {
            Actions actions = new Actions(driver);
            actions.moveToElement(countinueButton).click().perform();
        }catch (UnhandledAlertException f){
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            System.out.println("UnhandleAlertText:" + alertText);
            alert.accept();
        }catch (NoAlertPresentException e){
            e.printStackTrace();
        }
    }
    @Step
    public boolean checkPresentsOfPaymentDetailsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPaymentDetails));
        return presentsOfPaymentDetails.isDisplayed();
    }
    @Step
    public void clickOnShiipingDetailsCheckbox(){
        wait.until(ExpectedConditions.visibilityOf(shippingDetailsCheckbox));
        shippingDetailsCheckbox.click();
    }
    @Step
    public void writeCountryShipping(String countryShipping) throws InterruptedException {
        Thread.sleep(2500);
        countryShippingInput.sendKeys(countryShipping);
    }
    @Step
    public void chooseFirstCountryShippingList(){
        Actions actions = new Actions(driver);
        actions.moveToElement(countryShippingSelect).click().perform();
    }
    @Step
    public void writeStateShipping(String stateShipping) throws InterruptedException {
        Thread.sleep(2500);
        stateShippingInput.sendKeys(stateShipping);
    }
    @Step
    public void chooseFirstStateFromShippingList(){
        Actions actions = new Actions(driver);
        actions.moveToElement(stateListShipping).click().perform();
    }
    @Step
    public void writeCityShipping(String cityShipping){
        cityShippingInput.sendKeys(cityShipping);
    }
    @Step
    public void writeShippingAddress(String shippingAddress){
        streetAddressShippingInput.sendKeys(shippingAddress);
    }
    @Step
    public void writeShippingZip(String shippingZip){
        zipShippingInput.sendKeys(shippingZip);
    }
    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfAddressPage));
        backButton.click();
    }
    @Step
    public boolean checkIfImagesPageIsPresent(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfImagesPage));
        return presentsOfImagesPage.isDisplayed();
    }
    @Step
    public String getCssValueOfStreetAddressField(){
        return streetAddressFirstInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfCityField(){
        return cityFirstInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfCountryInput(){
        return countryInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfStateInput(){
        return stateInputFirst.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfZipInput(){
        return firstZipInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfShippingCountry(){
        return countryShippingInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfShippingStreetAddress() throws InterruptedException {
        Thread.sleep(1000);
        return countryShippingInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfShippingCity(){
        return cityShippingInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfShippingState(){
        return stateShippingInput.getCssValue("border-color");
    }
    @Step
    public String getCssValueOfShippingZip(){
        return zipShippingInput.getCssValue("border-color");
    }
    @Step
    public String getTextOfShippingCountryErrorMessage(){
        return shippingCountryErrorMessage.getText();
    }
    @Step
    public String getTextOfShippingStreetAddress(){
        return streetAddressShippingInput.getText();
    }
    @Step
    public String getShippingCountryInputText(){
        return countryShippingInput.getAttribute("value");
    }
    @Step
    public String getShippingCityInputText(){
        return cityShippingInput.getAttribute("value");
    }
    @Step
    public String getShippingStreetAddressText(){
        return streetAddressShippingInput.getAttribute("value");
    }
    @Step
    public String getShippingZipInputText(){
        return zipShippingInput.getAttribute("value");
    }
    @Step
    public String getShippingStateInpuText(){
        return stateShippingInput.getAttribute("value");
    }
    @Step
    public void clickOnXButton(){
        wait.until(ExpectedConditions.visibilityOf(countryInput));
        XButton.click();
    }
    @Step
    public void goToThirdStepByURL(){
        driver.get(thirdStep);
    }
    @Step
    public void goToFifthStepByURL(){
        driver.get(fifthStep);
    }
    @Step
    public String getCurrentURL() throws InterruptedException {
        Thread.sleep(1500);
        return driver.getCurrentUrl();
    }
}
