package com.art.idl.steps;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;
import com.art.idl.singleton.SingletonDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 11/22/2017.
 */
public class PrintDetailsPage extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//label[@id=\"order-number\"]")
    private WebElement presentsOfPrintDetailsPage;

    @FindBy(xpath = "//label[@id=\"order-number\"]")
    private WebElement orderId;

    @FindBy(xpath = "//label[@id=\"order-date\"]")
    private WebElement orderDateValue;

    @FindBy(xpath = "//label[@id=\"order-status\"]")
    private WebElement orderStatusValue;

    @FindBy(xpath = "//label[@id=\"license-number\"]")
    private WebElement licenseNumberValue;

    @FindBy(xpath = "//label[@id=\"expiration-date\"]")
    private WebElement expirationDateValue;

    @FindBy(xpath = "//label[@id=\"first-issue\"]")
    private WebElement firstIssueDateValue;

    @FindBy(xpath = "//label[@id=\"country\"]")
    private WebElement countryLicenseValue;

    @FindBy(xpath = "//label[@id=\"categories\"]")
    private WebElement categoriesValue;

    @FindBy(xpath = "//label[@id=\"full-name-p\"]")
    private WebElement fullNameValue;

    @FindBy(xpath = "//label[@id=\"country-p\"]")
    private WebElement personalCountryValue;

    @FindBy(xpath = "//label[@id=\"street-addr-p\"]")
    private WebElement personalStreetAddressValue;

    @FindBy(xpath = "//label[@id=\"city-p\"]")
    private WebElement personalCityValue;

    @FindBy(xpath = "//label[@id=\"state-p\"]")
    private WebElement personalStateValue;

    @FindBy(xpath = "//label[@id=\"zip-p\"]")
    private WebElement personalZipValue;

    @FindBy(xpath = "//label[@id=\"email-p\"]")
    private WebElement personalEmailValue;

    @FindBy(xpath = "//label[@id=\"full-name-s\"]")
    private WebElement shippingFullNameValue;

    @FindBy(xpath = "//label[@id=\"country-s\"]")
    private WebElement shippingCountryValue;

    @FindBy(xpath = "//label[@id=\"street-addr-s\"]")
    private WebElement shippingStreetAddressValue;

    @FindBy(xpath = "//label[@id=\"city-s\"]")
    private WebElement shippingCityValue;

    @FindBy(xpath = "//label[@id=\"state-s\"]")
    private WebElement shippingStateValue;

    @FindBy(xpath = "//label[@id=\"zip-s\"]")
    private WebElement shippingZipValue;

    @FindBy(xpath = "//div[5]/div[1]/label[2]/span[2]")
    private WebElement yourOrderInsuranceValue;

    @FindBy(xpath = "//span[@id=\"shipping-price\"]")
    private WebElement yourOrderShippingPriceValue;
    //Waiting for update!
    @FindBy(xpath = "//div[5]/div[1]/label[1]/span[2]")
    private WebElement yourOrderLicensePriceValue;

    @FindBy(xpath = "//div[2]/label/span")
    private WebElement orderTotalValue;

    @FindBy(xpath = "//button[@class=\"btn btn-get btn-back\"]")
    private WebElement goToHomeButton;

    @FindBy(xpath = "//p[@class=\"btn-close\"]")
    private WebElement XButton;

    @Step
    public boolean checkIfUserIsOnPrintDetailsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfPrintDetailsPage));
        return presentsOfPrintDetailsPage.isDisplayed();
    }
    @Step
    public String getOrderIdValue(){
        return orderId.getText();
    }
    @Step
    public String getOrderDateValue(){
        return orderDateValue.getText();
    }
    @Step
    public String getOrderStatusValue(){
        return orderStatusValue.getText();
    }
    @Step
    public String getLicenseNumberValue(){
        return licenseNumberValue.getText();
    }
    @Step
    public String getExpirationDateValue(){
        return expirationDateValue.getText();
    }
    @Step
    public String getFirstIssueDateValue(){
        return firstIssueDateValue.getText();
    }
    @Step
    public String getCountryValue(){
        return countryLicenseValue.getText();
    }
    @Step
    public String getCategoriesValue(){
        return categoriesValue.getText();
    }
    @Step
    public String getFullNameValue(){
        return fullNameValue.getText();
    }
    @Step
    public String getPersonalCountryValue(){
        return personalCountryValue.getText();
    }
    @Step
    public String getPersonalStreetAddressValue(){
        return personalStreetAddressValue.getText();
    }
    @Step
    public String getPersonalCityValue(){
        return  personalCityValue.getText();
    }
    @Step
    public String getPersonalStateValue(){
        return personalStateValue.getText();
    }
    @Step
    public String getPersonalZipValue(){
        return personalZipValue.getText();
    }
    @Step
    public String getPersonalEmailValue(){
        return personalEmailValue.getText();
    }
    @Step
    public String getShippingFullNameValue(){
        return shippingFullNameValue.getText();
    }
    @Step
    public String getShippingCountryValue(){
        return shippingCountryValue.getText();
    }
    @Step
    public String getShippingStreetAddressValue(){
        return shippingStreetAddressValue.getText();
    }
    @Step
    public String getShippingCityValue(){
        return shippingCityValue.getText();
    }
    @Step
    public String getShippingStateValue(){
        return shippingStateValue.getText();
    }
    @Step
    public String getShippingZipValue(){
        return shippingZipValue.getText();
    }
    @Step
    public String getYourOrderInsuranceValue(){
        return yourOrderInsuranceValue.getText();
    }
    @Step
    public String getYourOrderShippingPrice(){
        return yourOrderShippingPriceValue.getText();
    }
    //Waiting for update!
    @Step
    public String getYourOrderLicensePriceValue(){
        return yourOrderLicensePriceValue.getText();
    }
    @Step
    public String getOrderTotalValue(){
        return orderTotalValue.getText();
    }
    @Step
    public void clickOnGoToHomeButton(){
        goToHomeButton.click();
    }
    @Step
    public void clickOnXButton(){
        wait.until(ExpectedConditions.visibilityOf(fullNameValue));
        XButton.click();
    }
}
