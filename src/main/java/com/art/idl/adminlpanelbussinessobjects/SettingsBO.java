package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminSettingsPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 12/13/2017.
 */
public class SettingsBO extends PageObject {
    public void createNewPassword(String currentPassword,String newPassword){
        AdminSettingsPage adminSettingsPage = new AdminSettingsPage();
        adminSettingsPage.writeCurrentPassword(currentPassword);
        adminSettingsPage.writeNewPassword(newPassword);
        adminSettingsPage.writeConfirmNewPassword(newPassword);
        adminSettingsPage.clickOnSaveChangesButton();
    }
    public void createNewEmail(String newEmailOfUser){
        AdminSettingsPage adminSettingsPage = new AdminSettingsPage();
        adminSettingsPage.clearEmailOfUser();
        adminSettingsPage.writeEmailOfUser(newEmailOfUser);
        adminSettingsPage.clickOnSaveChangesButton();
    }
    public void createNewName(String newNameOfUser){
        AdminSettingsPage adminSettingsPage = new AdminSettingsPage();
        adminSettingsPage.clearNameOfUserField();
        adminSettingsPage.writeNameOfUser(newNameOfUser);
        adminSettingsPage.clickOnSaveChangesButton();
    }
    public void changeColorOfAdminPanel(String colorInput){
        AdminSettingsPage adminSettingsPage = new AdminSettingsPage();
        adminSettingsPage.clickOnSettingButton();
        adminSettingsPage.clickOnCustomizationSettingButton();
        switch (colorInput.toLowerCase()){
            case "darkblue":
                adminSettingsPage.clickOnDarkBlueCheckbox();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "blue":
                adminSettingsPage.selectBlueColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "lightblue":
                adminSettingsPage.selectLightBlueColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "aqua":
                adminSettingsPage.selectAquaColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "trainblue":
                adminSettingsPage.selectTrainBlueColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "traingreen":
                adminSettingsPage.selectTrainGreenColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            case "indigo":
                adminSettingsPage.selectIndigoColor();
                adminSettingsPage.clickOnSaveCustomizationButton();
                break;
            default:
                System.out.println("Invalid color!");
        }
    }
}
