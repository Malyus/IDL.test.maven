package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminClientsPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 12/4/2017.
 */
public class AdminClientsBO extends PageObject {
    public void goToClientsPage(){
        AdminPanelLoginBO adminPanelLoginBO = new AdminPanelLoginBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminPanelLoginBO.loginToAdminPanel();
        adminClientsPage.clickOnClientsButton();
    }
}
