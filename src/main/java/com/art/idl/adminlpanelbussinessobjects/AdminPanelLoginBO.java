package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminPanelLoginPage;
import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.EnvProperties;

/**
 * Created by M.Malyus on 11/28/2017.
 */
public class AdminPanelLoginBO extends PageObject {
    private EnvProperties envProperties = new EnvProperties();
    private String adminURL = envProperties.getAdminURL();
    private String adminUsername = "test@test.com";
    private String adminPassword = "pass";

 public void loginToAdminPanel(){
     AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
     adminPanelLoginPage.openInNewWindow(adminURL);
     adminPanelLoginPage.switchToANewWindow();
     adminPanelLoginPage.writeLoginOfAdmin(adminUsername);
     adminPanelLoginPage.writePasswordOfAdmin(adminPassword);
     adminPanelLoginPage.clickOnSubmitButton();
 }
}
