package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminUsersPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 12/7/2017.
 */
public class UsersBO extends PageObject {
    public void createNewUser(){
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        adminUsersPage.clickOnUsersMenuButton();
        adminUsersPage.clickOnCreateNewUserButton();
    }
    public void writeCredentialsOfNewUser(String name,String email, String password){
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        adminUsersPage.writeNameOfNewUser(name);
        adminUsersPage.writeEmailOfNewUser(email);
        adminUsersPage.writePasswordOfNewUser(password);
    }
}
