package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminOrdersPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 11/29/2017.
 */
public class OrdersBO extends PageObject {
    public void chooseInCalendarsTheSameStartAndEndDates(){
        AdminOrdersPage adminOrdersPage = new AdminOrdersPage();
        adminOrdersPage.clickOnOrdersMenuButton();
        adminOrdersPage.openCalendarStartDay();
        adminOrdersPage.clickOnTenthDayOfStartDayMonth();
        adminOrdersPage.openCalendarEndDay();
        adminOrdersPage.clickOnTenthDayOfEndDayMonth();
    }
    public void chooseNewStartAndEndDates(){
        AdminOrdersPage adminOrdersPage = new AdminOrdersPage();
        adminOrdersPage.openCalendarStartDay();
        adminOrdersPage.clickOnEleventhDayOfStartDayMonth();
        adminOrdersPage.openCalendarEndDay();
        adminOrdersPage.clickOnEleventhDayOfEndDayMonth();
    }
}
