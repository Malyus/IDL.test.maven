package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminEmailsPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 12/11/2017.
 */
public class AdminEmailsBO extends PageObject {
    public void createNewEmail(String nameOfEmail,String subjectOfEmail,String contentOfEmail){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsPage.clickOnAddNewMessage();
        adminEmailsPage.writeNameOfEmail(nameOfEmail);
        adminEmailsPage.writeSubjectOfEmail(subjectOfEmail);
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.writeContentOfEmail(contentOfEmail);
        adminEmailsPage.clickOnSaveEmailButton();
    }
}
