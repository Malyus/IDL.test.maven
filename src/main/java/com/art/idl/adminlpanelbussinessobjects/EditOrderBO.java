package com.art.idl.adminlpanelbussinessobjects;

import com.art.idl.adminpanelpages.AdminEditOrderPage;
import com.art.idl.adminpanelpages.AdminUsersPage;
import com.art.idl.pageobject.PageObject;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class EditOrderBO extends PageObject {
    private AdminClientsBO adminClientsBO = new AdminClientsBO();
    private AdminEditOrderPage adminEditOrderPage = new AdminEditOrderPage();

    public void goToEditFirstOrderPage(){
        adminClientsBO.goToClientsPage();
        adminEditOrderPage.clickOnOrdersMenuButton();
        adminEditOrderPage.clickOnEditFirstOrderButton();
    }
    public void writeNewFirstNameLastNameAndDateOfBirthAndSave(String firstName,String lastName, String monthOfBirth, String dayOfBirth,String yearOfBirth){
        adminEditOrderPage.clearFirstNameInput();
        adminEditOrderPage.writeFirstNameInput(firstName);
        adminEditOrderPage.clearLastNameInput();
        adminEditOrderPage.writeLastNameInput(lastName);
        adminEditOrderPage.selectMonthOfBirth(monthOfBirth);
        adminEditOrderPage.selectDayOfBirth(dayOfBirth);
        adminEditOrderPage.selectYearOfBirth(yearOfBirth);
        //adminEditOrderPage.clickOnSaveEditOrderButton();
    }
}
