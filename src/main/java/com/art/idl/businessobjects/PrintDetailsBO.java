package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.properties.PlaceOrderValuesReader;
import com.art.idl.steps.*;
import org.testng.Assert;

/**
 * Created by M.Malyus on 11/22/2017.
 */
public class PrintDetailsBO extends PageObject {
   public void goToPrintDetailsPage() throws InterruptedException {
       PlaceOrderBO placeOrderBO = new PlaceOrderBO();
       PrintDetailsPage printDetailsPage = new PrintDetailsPage();
       PlaceOrderPage placeOrderPage = new PlaceOrderPage();
       placeOrderBO.goToPlaceOrderAndFillInShippingDetails();
       placeOrderPage.clickOnInsuranceButton();
       placeOrderPage.clickOnSecondCheckbox();
       placeOrderPage.clickOnContinueButton();
       Assert.assertTrue(printDetailsPage.checkIfUserIsOnPrintDetailsPage());
   }
   public void makeAnUniqueUser(String firstName, String lastName, String email, String phoneNumber){
     PersonalInformationBO personalInformationBO = new PersonalInformationBO();
     PersonalInformationPage personalInformationPage = new PersonalInformationPage();
     personalInformationBO.goToPersonalInformationPage();
     personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,phoneNumber);
     personalInformationPage.clickOnCountinueButtonWhenAllWithValid();
   }
   public void makeOtherSteps() throws InterruptedException {
       OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
       OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
       ImagesBO imagesBO = new ImagesBO();
       AddressBO addressBO = new AddressBO();
       PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
       PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
       PlaceOrderPage placeOrderPage = new PlaceOrderPage();
       PrintDetailsPage printDetailsPage = new PrintDetailsPage();
       originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("CA3235","Ukraine");
       originalDriverLicensePage.clickOnCountinueButtonWhenAllWithValid();
       imagesBO.makeImagesStep();
       addressBO.makeAddressStepWithoutShipping("Ukraine","a","Chernihiv","a","1");
       paymentDetailsBO.writeFieldsOfPaymentDetails("1111111111111111","12","2020","123");
       paymentDetailsPage.clickOnValidContinue();
       placeOrderPage.checkPresentsOfPlaceOrderPage();
       placeOrderPage.clickOnContinueButton();
       printDetailsPage.checkIfUserIsOnPrintDetailsPage();
       printDetailsPage.clickOnGoToHomeButton();
   }
   public void makeNotFinishedOrder(String firstName, String lastName, String email, String phoneNumber){
       PersonalInformationBO personalInformationBO = new PersonalInformationBO();
       OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
       PersonalInformationPage personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,phoneNumber);
       personalInformationPage.clickOnCountinueButtonWhenAllWithValid();
       originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent();
       originalDriverLicensePage.clickOnXButton();
   }
    public void makeAnUniqueUserWithThreeYearLicense(String firstName, String lastName, String email, String phoneNumber){
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPageWithThreeYearLicense();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,phoneNumber);
        personalInformationPage.clickOnCountinueButtonWhenAllWithValid();
    }
}
