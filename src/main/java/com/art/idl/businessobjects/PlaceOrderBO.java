package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.AddressProperties;
import com.art.idl.singleton.PaymentDetailsProperties;
import com.art.idl.steps.AddressPage;
import com.art.idl.steps.PaymentDetailsPage;
import org.testng.Assert;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class PlaceOrderBO extends PageObject {
    public void goToPlaceOrderPage() throws InterruptedException {
        AddressProperties addressProperties = new AddressProperties();
        PaymentDetailsProperties paymentDetailsProperties = new PaymentDetailsProperties();

        String firstCountry = addressProperties.getFirstCountry();
        String firstAddress = addressProperties.getFirstAddress();
        String firstCity = addressProperties.getFirstCity();
        String firstState = addressProperties.getFirstState();
        String firstZip = addressProperties.getFirstZip();

        AddressBO addressBO = new AddressBO();
        AddressPage addressPage = new AddressPage();
        addressBO.goToAddressPageAndWriteFields(firstCountry,firstAddress,firstCity,firstState, firstZip);
        addressPage.clickOnValidContinue();

        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        String creditCardNumber = paymentDetailsProperties.getCreditCardNumber();
        String month = paymentDetailsProperties.getExpDateMonth();
        String year = paymentDetailsProperties.getExpDateYear();
        String cvv = paymentDetailsProperties.getCvv();

        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,month,year,cvv);
        paymentDetailsPage.clickOnValidContinue();

        /* Assert.assertTrue(paymentDetailsPage.checkPresentsOfPlaceOrderPage(),"User is not on PlaceOrderPage!");*/
    }
    public void goToPlaceOrderAndFillInShippingDetails() throws InterruptedException {
        AddressProperties addressProperties = new AddressProperties();
        PaymentDetailsProperties paymentDetailsProperties = new PaymentDetailsProperties();

        String firstCountry = addressProperties.getFirstCountry();
        String firstAddress = addressProperties.getFirstAddress();
        String firstCity = addressProperties.getFirstCity();
        String firstState = addressProperties.getFirstState();
        String firstZip = addressProperties.getFirstZip();

        String shippingCountry = addressProperties.getShippingCountry();
        String shippingAddress = addressProperties.getShippingStreetAddress();
        String shippingCity = addressProperties.getShippingCity();
        String shippingState = addressProperties.getShippingState();
        String shippingZip = addressProperties.getShippingZip();

        AddressBO addressBO = new AddressBO();
        AddressPage addressPage = new AddressPage();
        addressBO.goToAddressPageAndWriteFields(firstCountry,firstAddress,firstCity,firstState, firstZip);
        addressBO.writeShippingDetailsFields(shippingCountry,shippingAddress,shippingCity,shippingState,shippingZip);
        addressPage.clickOnValidContinue();

        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        String creditCardNumber = paymentDetailsProperties.getCreditCardNumber();
        String month = paymentDetailsProperties.getExpDateMonth();
        String year = paymentDetailsProperties.getExpDateYear();
        String cvv = paymentDetailsProperties.getCvv();

        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,month,year,cvv);
        paymentDetailsPage.clickOnValidContinue();

        //Assert.assertTrue(paymentDetailsPage.checkPresentsOfPlaceOrderPage(),"User is not on PlaceOrderPage!");
    }
}
