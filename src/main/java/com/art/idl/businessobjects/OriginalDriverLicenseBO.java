package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.steps.OriginalDriverLicensePage;
import com.art.idl.steps.PersonalInformationPage;
import org.testng.Assert;

/**
 * Created by M.Malyus on 11/10/2017.
 */
public class OriginalDriverLicenseBO extends PageObject {

    public void goToOriginalDriverLicensePage(){
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage("Alex","Mason","a@gmail.com","0931308310");
        personalInformationPage.clickOnCountinueButtonWhenAllWithValid();
    }

    public void writeToFieldsOfOriginalDriverLicenseStep(String licenseNumber,String countryOfDriverLicense) throws InterruptedException {
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(), "User is not on OriginalDriverLicensePage!");
        originalDriverLicensePage.writeLicenseNumber(licenseNumber);
        originalDriverLicensePage.chooseFirstMonthExpirationDate();
        originalDriverLicensePage.chooseFirstDayExpirationDate();
        originalDriverLicensePage.chooseFirstYearExpirationDate();
        originalDriverLicensePage.chooseFirstIssueMonth();
        originalDriverLicensePage.chooseFirstIssueDate();
        originalDriverLicensePage.chooseFirstIssueYear();
        originalDriverLicensePage.writeCountryOfDriverLicense(countryOfDriverLicense);
        originalDriverLicensePage.chooseACategory();
        originalDriverLicensePage.chooseCCategory();
       originalDriverLicensePage.uploagJPEGFormat();

    }
    public void activateAllDriverLicenseCategoriesButtons(){
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicensePage.chooseACategory();
        originalDriverLicensePage.chooseCCategory();
        originalDriverLicensePage.chooseDCategory();
        originalDriverLicensePage.chooseECategory();
    }
    public void getToThirdStep() throws InterruptedException {
        OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("CA3256","Ukraine");
        originalDriverLicensePage.clickOnCountinueButtonWhenAllWithValid();

    }

    public void writeAllFieldsWithValidFormatAndXMLPhoto(String licenseNumber, String countryOfDriverLicense) throws InterruptedException {
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(), "User is not on OriginalDriverLicensePage!");
        originalDriverLicensePage.writeLicenseNumber(licenseNumber);
        originalDriverLicensePage.chooseFirstMonthExpirationDate();
        originalDriverLicensePage.chooseFirstDayExpirationDate();
        originalDriverLicensePage.chooseFirstYearExpirationDate();
        originalDriverLicensePage.chooseFirstIssueMonth();
        originalDriverLicensePage.chooseFirstIssueDate();
        originalDriverLicensePage.chooseFirstIssueYear();
        originalDriverLicensePage.writeCountryOfDriverLicense(countryOfDriverLicense);
        originalDriverLicensePage.chooseACategory();
        originalDriverLicensePage.chooseCCategory();
        originalDriverLicensePage.uploadXMLFormat();

    }
    public void writeAllFieldsAndDownloadWrongFormat() throws InterruptedException {
        OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeAllFieldsWithValidFormatAndXMLPhoto("CA3256","Ukraine");
        originalDriverLicensePage.clickOnCountinueButton();
    }
    public void writeAllFieldsAndWrongPhotoFormatByChangePhoto() throws InterruptedException {
        OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
        OriginalDriverLicensePage originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.writeAllFieldsWithValidFormatAndXMLPhoto("CA3256","Ukraine");
    }
}
