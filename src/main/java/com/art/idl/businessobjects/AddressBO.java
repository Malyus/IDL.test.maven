package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.steps.AddressPage;

/**
 * Created by M.Malyus on 11/15/2017.
 */
public class AddressBO extends PageObject{
    public void goToAddressPageAndWriteFields(String firstCountry,String firstAddress,String firstCity,String firstState,String firstZip) throws InterruptedException {
        ImagesBO imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.writeCountryInFirstSection(firstCountry);
        addressPage.chooseCountryFromList();
        addressPage.writeCityFirst(firstCity);
        addressPage.writeFirstState(firstState);
        addressPage.chooseFirstFromListStates();
        addressPage.writeStreeAddressFirst(firstAddress);
        addressPage.writeFirstZip(firstZip);
    }
    public void goToAdressPageAndLeaveEmptyCountryAndState(String firstCountry,String firstAddress,String firstCity,String firstState,String firstZip) throws InterruptedException {
        ImagesBO imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.writeCityFirst(firstCity);
        addressPage.writeStreeAddressFirst(firstAddress);
        addressPage.writeFirstZip(firstZip);
    }
    public void goToAddressPageAndWorkWithState(String firstCountry,String firstAddress,String firstCity,String firstState,String firstZip) throws InterruptedException {
        ImagesBO imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.writeCountryInFirstSection(firstCountry);
        addressPage.chooseCountryFromList();
        addressPage.writeFirstState(firstState);
        addressPage.writeCityFirst(firstCity);
        addressPage.writeStreeAddressFirst(firstAddress);
        addressPage.writeFirstZip(firstZip);
    }
    public void writeShippingDetailsFields(String shippingCountry,String shippingAddress,String shippingCity,String shippingState, String shippingZip) throws InterruptedException {
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnShiipingDetailsCheckbox();
        addressPage.writeCountryShipping(shippingCountry);
        addressPage.chooseFirstCountryShippingList();
        addressPage.writeStateShipping(shippingState);
        addressPage.chooseFirstStateFromShippingList();
        addressPage.writeShippingAddress(shippingAddress);
        addressPage.writeCityShipping(shippingCity);
        addressPage.writeShippingZip(shippingZip);
    }
    public void writeEmptyShippingStateAndWriteAllValid(String shippingCountry,String shippingAddress,String shippingCity,String shippingState, String shippingZip) throws InterruptedException {
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnShiipingDetailsCheckbox();
        addressPage.writeCountryShipping(shippingCountry);
        addressPage.chooseFirstCountryShippingList();
        addressPage.writeStateShipping(shippingState);
        addressPage.writeShippingAddress(shippingAddress);
        addressPage.writeCityShipping(shippingCity);
        addressPage.writeShippingZip(shippingZip);
    }
    public void writeInvalidShippingFields(String shippingCountry,String shippingAddress,String shippingCity,String shippingZip) throws InterruptedException {
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnShiipingDetailsCheckbox();
        addressPage.writeCountryShipping(shippingCountry);
        addressPage.writeShippingAddress(shippingAddress);
        addressPage.writeCityShipping(shippingCity);
        addressPage.writeShippingZip(shippingZip);
    }
    public void makeAddressStepWithoutShipping(String firstCountry,String firstCity,String firstState,String firstAddress, String firstZip) throws InterruptedException {
        AddressPage addressPage = new AddressPage();
        addressPage.writeCountryInFirstSection(firstCountry);
        addressPage.chooseCountryFromList();
        addressPage.writeCityFirst(firstCity);
        addressPage.writeFirstState(firstState);
        addressPage.chooseFirstFromListStates();
        addressPage.writeStreeAddressFirst(firstAddress);
        addressPage.writeFirstZip(firstZip);
        addressPage.clickOnValidContinue();
    }

}
