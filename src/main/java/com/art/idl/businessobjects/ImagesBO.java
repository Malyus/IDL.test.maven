package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.ImagesPage;
import org.openqa.selenium.WebDriver;

/**
 * Created by M.Malyus on 11/14/2017.
 */
public class ImagesBO extends PageObject {
    public void goToAddressPage() throws InterruptedException {
        OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
        ImagesPage imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.uploadJPEGOfYourSelf();
        imagesPage.clickOnCropButton();
        imagesPage.makeADigitalSignature();
        imagesPage.clickOnSaveYourSignatureButton();
        imagesPage.clickOnValidContinue();
    }
    public void stayOnTheSameWhenSomeFieldsEmpty() throws InterruptedException {
        OriginalDriverLicenseBO originalDriverLicenseBO = new OriginalDriverLicenseBO();
        ImagesPage imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.uploadJPGOfYourSelf();
        imagesPage.clickOnCropButton();
        imagesPage.makeADigitalSignature();
        imagesPage.clickOnValidContinue();
    }
    public void makeImagesStep() throws InterruptedException {
        ImagesPage imagesPage = new ImagesPage();
        imagesPage.uploadJPGOfYourSelf();
        imagesPage.clickOnCropButton();
        imagesPage.makeADigitalSignature();
        imagesPage.clickOnSaveYourSignatureButton();
        imagesPage.clickOnValidContinue();
    }
}
