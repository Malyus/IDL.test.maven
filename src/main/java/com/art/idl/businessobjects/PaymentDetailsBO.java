package com.art.idl.businessobjects;

import com.art.idl.pageobject.PageObject;
import com.art.idl.steps.AddressPage;
import com.art.idl.steps.PaymentDetailsPage;

/**
 * Created by M.Malyus on 11/16/2017.
 */
public class PaymentDetailsBO extends PageObject {
    public void goToPaymentDetailsPage() throws InterruptedException {
        AddressBO addressBO = new AddressBO();
        AddressPage addressPage = new AddressPage();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        addressPage.clickOnValidContinue();
    }
    public void writeFieldsOfPaymentDetails(String crediCardNumber,String month,String year,String cvv) throws InterruptedException {
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsPage.writeCreditCardNumber(crediCardNumber);
        paymentDetailsPage.writeExpDateMonth(month);
        paymentDetailsPage.writeExpDateYear(year);
        paymentDetailsPage.writeCVV(cvv);
    }
}
