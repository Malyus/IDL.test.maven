package com.art.idl.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class EnvProperties {
    private Properties properties;

    public EnvProperties() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBaseUrl() {
        return properties.getProperty("BaseURL");
    }
    public String getValidFirstName(){
        return properties.getProperty("ValidFirstName");
    }
    public String getValidLastName(){
        return properties.getProperty("ValidLastName");
    }
    public String getValidEmail(){
        return properties.getProperty("ValidEmail");
    }
    public String getValidPhoneNumber(){
        return properties.getProperty("ValidPhoneNumber");
    }
    public String getValidLicenseNumber(){
        return properties.getProperty("ValidLicenseNumber");
    }
    public String getValidStreetAddressFirstSection(){
        return properties.getProperty("ValidStreetAddressFirstSection");
    }
    public String getValidZipFirstSection(){
        return properties.getProperty("ValidZipFirstSection");
    }
    public String getAdminURL(){
        return properties.getProperty("AdminURL");
    }
}

