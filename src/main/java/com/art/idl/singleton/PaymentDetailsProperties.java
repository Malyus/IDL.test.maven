package com.art.idl.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class PaymentDetailsProperties {
    private Properties properties;

    public PaymentDetailsProperties() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/paymentdetailsconfig.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCreditCardNumber() {
        return properties.getProperty("CreditCardNumber");
    }
    public String getExpDateMonth(){
        return properties.getProperty("ExpDateMonth");
    }
    public String getExpDateYear(){
        return properties.getProperty("ExpDateYear");
    }
    public String getCvv(){
        return properties.getProperty("Cvv");
    }
}
