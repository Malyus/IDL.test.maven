package com.art.idl.singleton;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class SingletonDriver {
    private static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<WebDriver>();
    private SingletonDriver() {
    }
    public static WebDriver getInstance() {
        if (webDriverThreadLocal.get() != null) {
            return webDriverThreadLocal.get();
        }
        WebDriver instance;
        /*Make unexpectedAlert IGNORE mode for catching them
        * in try-catch block.Those alerts by DB error
        * after getting to next step*/
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriverv2.36.exe");
        EnvProperties envProperties = new EnvProperties();
        /*Set capability - unexpected alert for Chrome*/
        instance = new ChromeDriver(dc);
        instance.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        /*Update chromedriver for windows maximize*/
        instance.manage().window().maximize();
        // instance.manage().deleteAllCookies();
        webDriverThreadLocal.set(instance);
        webDriverThreadLocal.get().get(envProperties.getBaseUrl());
        return webDriverThreadLocal.get();
    }
    public static void quit(){
        try{
            webDriverThreadLocal.get().quit();
        }
        finally {
            webDriverThreadLocal.remove();
        }
    }
}