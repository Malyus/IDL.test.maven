package com.art.idl.singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class AddressProperties {
    private Properties properties;

    public AddressProperties() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/addressconfig.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFirstCountry() {
        return properties.getProperty("FirstCountry");
    }
    public String getFirstState() {
        return properties.getProperty("FirstState");
    }
    public String  getFirstCity(){
        return properties.getProperty("FirstCity");
    }
    public String getFirstAddress(){
        return properties.getProperty("FirstAddress");
    }
    public String getFirstZip(){
        return properties.getProperty("FirstZip");
    }
    public String getShippingCountry(){
        return properties.getProperty("ShippingCountry");
    }
    public String getShippingState(){
        return properties.getProperty("ShippingState");
    }
    public String getShippingCity(){
        return properties.getProperty("ShippingCity");
    }
    public String getShippingStreetAddress(){
        return properties.getProperty("ShippingStreetAddress");
    }
    public String getShippingZip(){
        return properties.getProperty("ShippingZip");
    }
}
