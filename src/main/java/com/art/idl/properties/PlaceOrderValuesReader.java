package com.art.idl.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class PlaceOrderValuesReader {
    private Properties properties;

    public PlaceOrderValuesReader() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/placeordervalues.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLicenseNumber() {
        return properties.getProperty("LicenseNumber");
    }
    public String getExpirationDate(){
        return properties.getProperty("ExpirationDate");
    }
    public String getFirstIssueDate(){
        return properties.getProperty("FirstIssueDate");
    }
    public String getCountry(){
        return properties.getProperty("Country");
    }
    public String getCategories(){
        return properties.getProperty("Categories");
    }
    public String getFullName(){
        return properties.getProperty("FullName");
    }
    public String getCountryPersonalDetails(){
        return properties.getProperty("CountryPersonalDetails");
    }
    public String getStreetAddress(){
        return properties.getProperty("StreetAddress");
    }
    public String getCity(){
        return properties.getProperty("City");
    }
    public String getState(){
        return properties.getProperty("State");
    }
    public String getZip(){
        return properties.getProperty("Zip");
    }
    public String getEmail(){
        return properties.getProperty("Email");
    }
    public String getShippingFullName(){
        return properties.getProperty("ShippingFullName");
    }
    public String getShippingCountry(){
        return properties.getProperty("ShippingCountry");
    }
    public String getShippingStreetAddress(){
        return properties.getProperty("ShippingStreetAddress");
    }
    public String getShippingCity(){
        return properties.getProperty("ShippingCity");
    }
    public String getShippingState(){
        return properties.getProperty("ShippingState");
    }
    public String getShippingZip(){
        return properties.getProperty("ShippingZip");
    }
}
