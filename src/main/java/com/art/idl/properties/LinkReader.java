package com.art.idl.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/27/2017.
 */
public class LinkReader {

    private Properties properties;

    public LinkReader() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/linkvalues.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getLinkOfFirstStep(){
        return properties.getProperty("FirstStep");
    }
    public String getLinkOfSecondStep() {
        return properties.getProperty("SecondStep");
    }
    public String getLinkOfThirdStep(){
        return properties.getProperty("ThirdStep");
    }
    public String getLinkOfFourthStep(){
        return properties.getProperty("FourthStep");
    }
    public String getLinkOfFifthStep(){
        return properties.getProperty("FifthStep");
    }
    public String getLinkOfHomePage(){
        return properties.getProperty("HomePage");
    }
    public String getLinkOfPrintDetailsStep(){
        return properties.getProperty("PrintDetailsStep");
    }
}
