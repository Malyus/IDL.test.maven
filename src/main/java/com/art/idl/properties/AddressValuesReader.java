package com.art.idl.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/21/2017.
 */
public class AddressValuesReader {
    private Properties properties;

    public AddressValuesReader() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/personalinformationvalues.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getCountry() {
        return properties.getProperty("Country");
    }
    public String getAddress(){
        return properties.getProperty("Address");
    }
    public String getCity(){
        return properties.getProperty("City");
    }
    public String getState(){
        return properties.getProperty("State");
    }
    public String getZip(){
        return properties.getProperty("Zip");
    }
    public String getShippingCountry(){
        return properties.getProperty("ShippingCountry");
    }
    public String getShippingAddress(){
        return properties.getProperty("ShippingAddress");
    }
    public String getShippingCity(){
        return properties.getProperty("ShippingCity");
    }
    public String getShippingState(){
        return properties.getProperty("ShippingState");
    }
    public String getShippingZip(){
        return properties.getProperty("ShippingZip");
    }
}
