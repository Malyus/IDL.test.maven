package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.AddressBO;
import com.art.idl.businessobjects.ImagesBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.AddressPage;
import com.art.idl.steps.PaymentDetailsPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 11/14/2017.
 */
public class AddressPageTest {
    private WebDriver driver;
    private ImagesBO imagesBO;
    private AddressPage addressPage;

    //Id
    //Test 1.531
    @Test
    public void checkProgressBarForValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "User is not on AddressPage!");
        System.out.println("Progress bar level:" + addressPage.checckProgressBarLevel());
        Assert.assertTrue(addressPage.checckProgressBarLevel().contains("width: 80%;"), "Smth with progress-bar!");

    }
    //Id
    //Test 1.544
    @Test
    public void checkIfListOfCountriesMatchedInputtedLetter() throws InterruptedException {
        String first = "United Arab Emirates";
        String second = "United Kingdom";
        imagesBO = new ImagesBO();
        addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.getAllCountriesFromList("U");
        Assert.assertTrue(first.contains(addressPage.getFirstCountry()), "First country is invalid!");
        Assert.assertTrue(second.contains(addressPage.getSecondCountry()), "Second country is invalid!");
    }
    //Id
    //Test 1.546
    @Test
    public void checkIfStreetAddressCantBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAdressPageAndLeaveEmptyCountryAndState("","Z","Chernihiv","","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        //addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "Continue does not work correctly(empty country)!");
    }
    //Id
    //Test 1.557
    @Test
    public void checkIfStreetAddressCantBeWitDigitsOnly() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","1","Chernihiv","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfAddressPageIsPresent(),"Street address can be with digits only");
    }
    //Id
    //Test 1.559
    @Test
    public void checkIfStreetCanBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a a","Chernihiv","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "Continue does not work correctly(street with spaces)!");
    }
    //Id
    //Test 1.566
    @Test
    public void checkIfCityCantBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to leave 'City' an empty!");
    }
    //Id
    //Test 1.576
    @Test
    public void checkIfCityCantBeWithDigits() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","123","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to write digits into 'City'!");
    }
    //For checking!
    //Id
    //Test 1.578
    @Test
    public void checkIfCityCanBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a a","Cher nihiv","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "'City' cant be with spaces!");
    }
    //Id
    //Test 1.584
    @Test
    public void checkIfStateCantBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWorkWithState("Ukraine", "a", "a","","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows leave empty 'State'!");
    }
    //Bug when user enter '1' into state field!
    //Id
    //Test 1.594
    @Test
    public void checkIfStateCantBeWithDigits() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWorkWithState("Ukraine", "a", "a","1","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        //addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to fill in digits in 'State'");
    }
    //Id
    //Test 1.596
    @Test
    public void checkIfStateCanBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a a","Chernihiv","Poltavs’ka Oblast’","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "'State' cant be with spaces!");
    }
    //Id
    //Test 1.601
    @Test
    public void checkIfZipCantBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","City","Chernihiv","");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to leave 'Zip' an empty!");
    }
    //Id
    //Test 1.610
    @Test
    public void checkIfZipAllowsDigits() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","City","Chernihi","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "System allows to write digits into 'Zip'!");
        System.out.println("cause it works bitch!");
    }
    //Id
    //Test 1.613
    @Test
    public void checkIfZipCantBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","City","Chernihiv","1 2 3");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to write spaces into 'Zip'!");
    }
    //Id
    //Test 1.634
    @Test
    public void checkIfStreetShippingCantBeEmpty() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","City","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","","City","Chernihiv","1");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to leave empty  'StreetShipping'!");
    }
    //Id
    //Test 1.645
    @Test
    public void checkIfStreetShippingCantBeWithDigitsOnly() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","City","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","1","City","Chernihiv","1");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"Street shipping cant be with digits!");
    }
    //Id
    //Test 1.647
    @Test
    public void checkIfStreetShippingCanBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a a","C","Chernihiv","1");
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "System does not allow to write with spaces address into 'StreetShipping'!");
    }
    //Id
    //Test 1.654
    @Test
    public void checkIfCityShippingCantBeEmpty() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","","Chernihiv","1");
        //update
        addressPage.clickOnContinueButton();
        //update assert
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "System allows to leave empty 'ShippingCity'!");
    }
    //Id
    //Test 1.664
    @Test
    public void checkIfShippingCityCantBeWithDigitsAndSettingMainCityIsValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","1","Chernihiv","1");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfAddressPageIsPresent(),"System allows to write shipping city with digits only");
    }
    //Id
    //Test 1.666
    @Test
    public void checkIfShippingCityCanBeWithSpaces() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","Los Angeles","Chernihiv","1");
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(),"City shipping cant be with spaces!");
    }

    //Id
    //Test 1.672
    @Test
    public void checkIfShippingStateCanBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeEmptyShippingStateAndWriteAllValid("Ukraine","a","C","","1");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"State shipping cant be empty!");
    }
    //Id
    //Test 1.682
    @Test
    public void checkIfShippingStateCantBeWithDigits() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeEmptyShippingStateAndWriteAllValid("Ukraine","a","C","1","1");
        System.out.println("This is bug, because syste mallows to fill in shipping state with 1digit!");
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"State shipping cant be with digits!");
    }
    //Id
    //Test 1.684
    @Test
    public void checkIfShippingStateCanBeWithSpacesWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","a","Poltavs’ka Oblast","1");
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(),"State shipping can be with spaces!");
    }
    //Id
    //Test 1.689
    @Test
    public void checkIfShippingZipCanBeEmptyWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","L","Chernihiv","");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"Zip shipping cant be empty when all valid!");
    }
    //Id
    //Test 1.701
    @Test
    public void checkIfShippinZipCantBeWithSpace() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","C","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","L","Chernihiv","1 2 3");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"Zip cant be with spaces!");
    }
    //Id
    //Test 1.707-1.708
    @Test
    public void checkIfBackButtonRedirectToImagesPage() throws InterruptedException {
        imagesBO = new ImagesBO();
        addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.clickOnBackButton();
        Assert.assertTrue(addressPage.checkIfImagesPageIsPresent(),"Back button does not redirect user to Images Page");
    }
    //Id
    //Test 1.711-1.713
    @Test
    public void checkIfUserIsNotRedirectedWhenFirstSectionEmptyAndSecondSectionWithAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        AddressPage addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressBO.writeShippingDetailsFields("Ukraine","a","L","Chernihiv","1");
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"System allows to fill only second section and leave empty first section!");
    }
    //Id
    //Test 1.714
    @Test
    public void checkIfUserIsRedirectedToNextStepWhenAllValid() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","a","Chernihiv","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnValidContinue();
        Assert.assertTrue(addressPage.checkPresentsOfPaymentDetailsPage(), "User does not redirected to next stpe when all valid!");
    }
    //Id
    //Test 1.715
    @Test
    public void checkIfUserStaysOnTheAddressPageWhenOneFieldEmpty() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAddressPageAndWriteFields("Ukraine","","Chernihiv","Chernihiv","1");
        AddressPage addressPage = new AddressPage();
        addressPage.clickOnContinueButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(), "User redirected to next step when street field empty!");
    }
    //Id
    //Test 1.547,1.567,1.585,1.602
    @Test
    public void checkIfAddressFieldsAreRedWheItWithInvalidValue() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAdressPageAndLeaveEmptyCountryAndState("1","/","1","","/");
        SoftAssert softAssert = new SoftAssert();
        AddressPage addressPage = new AddressPage();
        System.out.println("StreetAddress before:" + addressPage.getCssValueOfStreetAddressField());
        System.out.println("CountryInput before:" + addressPage.getCssValueOfCountryInput());
        System.out.println("CityField before:" + addressPage.getCssValueOfCityField());
        System.out.println("StateInput before:" + addressPage.getCssValueOfStateInput());
        System.out.println("CityInput before:" + addressPage.getCssValueOfCityField());
        System.out.println("ZipField before:" + addressPage.getCssValueOfZipInput());

        addressPage.clickOnContinueButton();
        String redColor= "rgb(255, 0, 0)";

        System.out.println("StreetAddress after:" + addressPage.getCssValueOfStreetAddressField());
        System.out.println("CountryInput after:" + addressPage.getCssValueOfCountryInput());
        System.out.println("CityField after:" + addressPage.getCssValueOfCityField());
        System.out.println("StateInput after:" + addressPage.getCssValueOfStateInput());
        System.out.println("CityInput after:" + addressPage.getCssValueOfCityField());
        System.out.println("ZipField after:" + addressPage.getCssValueOfZipInput());

        softAssert.assertTrue(addressPage.getCssValueOfStreetAddressField().equals(redColor),"StreetAddress is not marked red when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfCountryInput().equals(redColor),"CountryInput is not marked red when it with invalid value");
        softAssert.assertTrue(addressPage.getCssValueOfCityField().equals(redColor),"CityField in not marked with red color when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfStateInput().equals(redColor),"StateField is not marked with red color when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfCityField().equals(redColor),"CityField is not marked with red color when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfZipInput().equals(redColor),"ZipField is marked with red color when it filled with invalid value!");
    }
    //Id
    //Test 1.548,1.568,1.586,1.603,1.627
    @Test
    public void checkIfAddressFieldsAreRedWhenItEmpty() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressBO addressBO = new AddressBO();
        addressBO.goToAdressPageAndLeaveEmptyCountryAndState("","","","","");
        SoftAssert softAssert = new SoftAssert();
        AddressPage addressPage = new AddressPage();

        System.out.println("StreetAddress before:" + addressPage.getCssValueOfStreetAddressField());
        System.out.println("CountryInput before:" + addressPage.getCssValueOfCountryInput());
        System.out.println("CityField before:" + addressPage.getCssValueOfCityField());
        System.out.println("StateInput before:" + addressPage.getCssValueOfStateInput());
        System.out.println("CityInput before:" + addressPage.getCssValueOfCityField());
        System.out.println("ZipField before:" + addressPage.getCssValueOfZipInput());

        addressPage.clickOnContinueButton();
        String redColor= "rgb(255, 0, 0)";

        System.out.println("StreetAddress after:" + addressPage.getCssValueOfStreetAddressField());
        System.out.println("CountryInput after:" + addressPage.getCssValueOfCountryInput());
        System.out.println("CityField after:" + addressPage.getCssValueOfCityField());
        System.out.println("StateInput after:" + addressPage.getCssValueOfStateInput());
        System.out.println("CityInput after:" + addressPage.getCssValueOfCityField());
        System.out.println("ZipField after:" + addressPage.getCssValueOfZipInput());

        softAssert.assertTrue(addressPage.getCssValueOfStreetAddressField().equals(redColor),"StreetAddress is not marked red when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfCountryInput().equals(redColor),"CountryInput is not marked red when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfCityField().equals(redColor),"CityField in not marked with red color when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfStateInput().equals(redColor),"StateField is not marked with red color when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfCityField().equals(redColor),"CityField is not marked with red color when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfZipInput().equals(redColor),"ZipField is marked with red color when it empty!");

    }
    //Id
    //Test 1.635,1.655,1.673,1.690
    @Test
    public void checkIfSecondSectionAddressFieldsAreRedWhenItWithInvalidValue() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        SoftAssert softAssert = new SoftAssert();
        addressBO.writeInvalidShippingFields("1","/","1","/");

        System.out.println("ShippingStreetAddress before:" + addressPage.getCssValueOfShippingStreetAddress());
        System.out.println("ShippingCity before:" + addressPage.getCssValueOfShippingCity());
        System.out.println("ShippingState before:" + addressPage.getCssValueOfShippingState());
        System.out.println("ShippingZip before:" + addressPage.getCssValueOfShippingZip());

        addressPage.clickOnContinueButton();
        String redColor= "rgb(255, 0, 0)";
        String shippingCountryErrorMessage = "No countries were found. Select from list, please.";
        System.out.println("ShippingCountry ErrorMessage:" + addressPage.getTextOfShippingCountryErrorMessage());
        System.out.println("StreetAddress after:" + addressPage.getCssValueOfShippingCountry());
        System.out.println("ShippingCity after:" + addressPage.getCssValueOfShippingCity());
        System.out.println("ShippingState after:" + addressPage.getCssValueOfShippingState());
        System.out.println("ShippingZip after:" + addressPage.getCssValueOfShippingZip());

        softAssert.assertTrue(addressPage.getTextOfShippingCountryErrorMessage().equals(shippingCountryErrorMessage),"Error message is not shown or is invalid!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingStreetAddress().equals(redColor),"ShippingStreetAddres is not red when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingCity().equals(redColor),"ShippingCity is not red when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingState().equals(redColor),"ShippingState is not red when it with invalid value!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingZip().equals(redColor),"ShippingZip is not red when it with invalid value!");
    }
    //Id
    //Test 1.627,1.636,1.656,1.674,1.691
    @Test
    public void checkIfSecondSectionFieldsAreRedWhenItEmpty() throws InterruptedException {
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        SoftAssert softAssert = new SoftAssert();
        addressBO.writeInvalidShippingFields("","","","");

        System.out.println("ShippingAddress before:" + addressPage.getCssValueOfShippingStreetAddress());
        System.out.println("ShippingCity before:" + addressPage.getCssValueOfShippingCity());
        System.out.println("ShippingState before:" + addressPage.getCssValueOfShippingState());
        System.out.println("ShippingZip before:" + addressPage.getCssValueOfShippingZip());
        addressPage.clickOnContinueButton();
        String redColor = "rgb(255, 0, 0)";
        String shippingCountryErrorMessage = "No countries were found. Select from list, please.";

        System.out.println("ShippingCountry ErrorMessage:" + addressPage.getTextOfShippingCountryErrorMessage());
        System.out.println("ShippingStreetAddress after:" + addressPage.getCssValueOfShippingStreetAddress());
        System.out.println("ShippingCity after:" + addressPage.getCssValueOfShippingCity());
        System.out.println("ShippingState after:" + addressPage.getCssValueOfShippingState());
        System.out.println("ShippingZip after:" + addressPage.getCssValueOfShippingZip());

        softAssert.assertTrue(addressPage.getTextOfShippingCountryErrorMessage().equals(shippingCountryErrorMessage),"ShippingCountryErrorMessage is invalid or it doesn't shown!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingStreetAddress().equals(redColor),"ShippingStreetAddress is not red when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingCity().equals(redColor),"ShippingCity is not red when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingState().equals(redColor),"ShippingState is not red when it empty!");
        softAssert.assertTrue(addressPage.getCssValueOfShippingZip().equals(redColor),"ShippingZip is not red when it empty!");
    }
    //Maybe this test will not be valid because requirements can be changed!
    //Id
    //Test 1.706
    @Test
    public void checkIfValuesOfShippingSectionSaveAfterDisablingShippingCheckbox() throws InterruptedException {
        String shippingCountry = "Ukraine";
        String shippingStreeAddress = "zelena";
        String shippingCity = "a";
        String shippingState = "Chernihiv";
        String shippingZip = "1";
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        SoftAssert softAssert = new SoftAssert();
        addressBO.writeShippingDetailsFields("Ukraine","zelena","a","Chernihiv","1");
        addressPage.clickOnShiipingDetailsCheckbox();
        addressPage.clickOnShiipingDetailsCheckbox();
        System.out.println("Text of ShippingAddress:" + addressPage.getShippingStreetAddressText());
        System.out.println("Text of ShippingCity:" + addressPage.getShippingCityInputText());
        System.out.println("Text of ShippingZip:" + addressPage.getShippingZipInputText());
        //This two waiting for html attribute 'value'
//        softAssert.assertTrue(addressPage.getShippingCountryInputText().equals(shippingCountry),"ShippingCountry is not valid or empty after off/on checkbox!");
//        softAssert.assertTrue(addressPage.getShippingStateInpuText().equals(shippingState),"ShippingState is not valid or empty after off/on checkbox!");
        softAssert.assertTrue(addressPage.getShippingStreetAddressText().equals(shippingStreeAddress),"ShippingStreetAddress is not valid or empty after off/on checkbox!");
        softAssert.assertTrue(addressPage.getShippingCityInputText().equals(shippingCity),"ShippingCity is not valid or empty after off/on checkbox!");
        softAssert.assertTrue(addressPage.getShippingZipInputText().equals(shippingZip),"ShippingZip is not valid or empty after off/on checkbox!!");
    }
    //Id
    //Test 1.719
    @Test
    public void checkIfUserIsRedirectedToHomePageByClickingOnXButton() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        addressPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(),"XButton does not redirect user to HomePage!");
    }
    //Id
    //Test 1.716
    @Test
    public void checkIfUserIsNotRedirectedToFifthStepByURL() throws InterruptedException {
        LinkReader linkReader = new LinkReader();
        String fifthStepLink = linkReader.getLinkOfFifthStep();
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        System.out.println("Before:" + addressPage.getCurrentURL());
        //addressPage.goToFifthStepByURL();
        System.out.println("After:" + addressPage.getCurrentURL());
        Assert.assertFalse(addressPage.getCurrentURL().equals(fifthStepLink),"User can be redirected to fifth step by using it URL!");
    }
    //Id
    //Test 1.717
    @Test
    public void checkIfUserIsRedirectedToThirdStepByURL() throws InterruptedException {
        LinkReader linkReader = new LinkReader();
        String urlOfThirdStep = linkReader.getLinkOfThirdStep();
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        AddressBO addressBO = new AddressBO();
        imagesBO.goToAddressPage();
        System.out.println("Before:" + addressPage.getCurrentURL());
        addressPage.clickOnBackButton();
        System.out.println("After:" + addressPage.getCurrentURL());
        Assert.assertTrue(addressPage.getCurrentURL().equals(urlOfThirdStep),"User is not redirected to third step by url!");

    }
    //Test 1.720
    //Id
    @Test
    public void checkIfContinueButtonIsDisplayedAfterRedirectingToHomePageFromAddressPage() throws InterruptedException {
        String continueButtonText = "Continue";
        IDLHomePage idlHomePage = new IDLHomePage();
        imagesBO = new ImagesBO();
        AddressPage addressPage = new AddressPage();
        imagesBO.goToAddressPage();
        addressPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfContinueButtonIsPresent(), "Continue button is not displayed!");
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(continueButtonText),"Continue button text is not valid!");
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
