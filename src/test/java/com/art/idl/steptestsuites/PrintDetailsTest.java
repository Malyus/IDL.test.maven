package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.PrintDetailsBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.PrintDetailsReader;
import com.art.idl.singleton.PaymentDetailsProperties;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.PrintDetailsPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/22/2017.
 */
public class PrintDetailsTest {
    private WebDriver driver;
    private PaymentDetailsProperties paymentDetailsProperties = new PaymentDetailsProperties();
    private PrintDetailsBO printDetailsBO;
    private PrintDetailsReader printDetailsReader = new PrintDetailsReader();
    private PrintDetailsPage printDetailsPage;
     //Waiting for update!
    //Test
    //Id
    @Test
    public void checkOrderInfoValuesForValid() throws InterruptedException {
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();

        //Generate a random number, check if it have 7 digits!
        String orderID = printDetailsReader.getOrderID();
        //Waiting for update, what date will be chosen!
        String orderDate = printDetailsReader.getOrderDate();
        //Always pending in 'OrderStatus'
        String orderStatus = printDetailsReader.getOrderStatus();

        System.out.println("Order ID:" + printDetailsPage.getOrderIdValue());
        System.out.println("OrderDate value:" + printDetailsPage.getOrderDateValue());
        System.out.println("OrderStatus value:" + printDetailsPage.getOrderStatusValue());

//        Assert.assertTrue(printDetailsPage.getOrderIdValue().equals(orderID),"OrderID does not have 7 digits!");
//        Assert.assertTrue(printDetailsPage.getOrderDateValue().equals(orderDate),"");
        Assert.assertTrue(printDetailsPage.getOrderStatusValue().equals(orderStatus),"Status of Order is  wrong!");

    }
    //Test 1.931-1.939
    //Id
    @Test
    public void checkDriverLicenseValuesForValid() throws InterruptedException {
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();

        String licenseNumber = printDetailsReader.getLicenseNumber();
        String expirationDate = printDetailsReader.getExpirationDate();
        String firstIssueDate = printDetailsReader.getFirstIssueDate();
        String country = printDetailsReader.getCountry();
        String categories = printDetailsReader.getCategories();

        System.out.println("LicenseNumber:" + printDetailsPage.getLicenseNumberValue());
        System.out.println("ExpirationDate:" + printDetailsPage.getExpirationDateValue());
        System.out.println("FirstIssueDate:" + printDetailsPage.getFirstIssueDateValue());
        System.out.println("Country:" + printDetailsPage.getCountryValue());
        System.out.println("Categories:" + printDetailsPage.getCategoriesValue());

        Assert.assertTrue(licenseNumber.equals(printDetailsPage.getLicenseNumberValue()),"LicenseNumber is wrong!");
        Assert.assertTrue(expirationDate.equals(printDetailsPage.getExpirationDateValue()),"ExpirationDate is wrong!");
        Assert.assertTrue(firstIssueDate.equals(printDetailsPage.getFirstIssueDateValue()),"FirstIssueDate is wrong!");
        Assert.assertTrue(country.equals(printDetailsPage.getCountryValue()),"Country is wrong!");
        Assert.assertTrue(categories.equals(printDetailsPage.getCategoriesValue()),"Categories are wrong!");
    }
    //Id
    //Test 1.943-1.951
    @Test
    public void checkPersonalDetailsValuesForValid() throws InterruptedException {
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();

        String fullName = printDetailsReader.getFullName();
        String country = printDetailsReader.getCountry();
        String streetAddress = printDetailsReader.getStreetAddress();
        String city = printDetailsReader.getCity();
        String state = printDetailsReader.getState();
        String zip = printDetailsReader.getZip();
        String email = printDetailsReader.getEmail();

        System.out.println("FullName:" + printDetailsPage.getFullNameValue());
        System.out.println("Country:" + printDetailsPage.getCountryValue());
        System.out.println("StreetAddress:" + printDetailsPage.getPersonalStreetAddressValue());
        System.out.println("City:" + printDetailsPage.getPersonalCityValue());
        System.out.println("State:" + printDetailsPage.getPersonalStateValue());
        System.out.println("Zip:" + printDetailsPage.getPersonalZipValue());
        System.out.println("Email:" + printDetailsPage.getPersonalZipValue());

        Assert.assertTrue(fullName.equals(printDetailsPage.getFullNameValue()), "FullName is wrong!");
        Assert.assertTrue(country.equals(printDetailsPage.getCountryValue()),"Country is wrong!");
        Assert.assertTrue(streetAddress.equals(printDetailsPage.getPersonalStreetAddressValue()),"StreetAddress is wrong");
        Assert.assertTrue(city.equals(printDetailsPage.getPersonalCityValue()),"City is wrong!");
        Assert.assertTrue(state.equals(printDetailsPage.getPersonalStateValue()),"State is wrong!");
        Assert.assertTrue(zip.equals(printDetailsPage.getPersonalZipValue()),"Zip is wrong!");
        Assert.assertTrue(email.equals(printDetailsPage.getPersonalEmailValue()), "Email is wrong!");

    }
    //Test 1.953
    //Id
    @Test
    public void checkShippingDetailsValuesForValidWhenCheckboxIsActivated() throws InterruptedException {
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();

        String shippingFullName = printDetailsReader.getShippingFullName();
        String shippingCountry = printDetailsReader.getShippingCountry();
        String shippingStreetAddress = printDetailsReader.getShippingStreetAddress();
        String shippingCity = printDetailsReader.getShippingCity();
        String shippingState = printDetailsReader.getShippingState();
        String shippingZip = printDetailsReader.getShippingZip();

        System.out.println("ShippingFullName:" + printDetailsPage.getShippingFullNameValue());
        System.out.println("ShippingCountry:" + printDetailsPage.getShippingCountryValue());
        System.out.println("ShippingStreetAddress:" + printDetailsPage.getShippingStreetAddressValue());
        System.out.println("ShippingCity:" + printDetailsPage.getShippingCityValue());
        System.out.println("ShippingState:" + printDetailsPage.getShippingStateValue());
        System.out.println("ShippingZip:" + printDetailsPage.getShippingZipValue());

        Assert.assertTrue(shippingFullName.equals(printDetailsPage.getShippingFullNameValue()),"ShippingFullName is wrong!");
        Assert.assertTrue(shippingCountry.equals(printDetailsPage.getShippingCountryValue()), "ShippingCountry is wrong!");
        Assert.assertTrue(shippingStreetAddress.equals(printDetailsPage.getShippingStreetAddressValue()),"ShippingStreetAddressis wrong!");
        Assert.assertTrue(shippingCity.equals(printDetailsPage.getShippingCityValue()),"ShippingCity is wrong!");
        Assert.assertTrue(shippingState.equals(printDetailsPage.getShippingStateValue()),"ShippingState is wrong!");
        Assert.assertTrue(shippingZip.equals(printDetailsPage.getShippingZipValue()),"ShippingZip is wrong!");
    }
    //Test 1.955-1.957
    //Id
    @Test
    public void checkIfValueOfYourOrderAreValid() throws InterruptedException {
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();

        String licencePrice = printDetailsReader.getYourOrderLicensePrice();
        String insurance = printDetailsReader.getYourOrderInsurance();
        String delivery = printDetailsReader.getDeliveryValue();
        String orderTotal = printDetailsReader.getOrderTotal();

        System.out.println("License price 'YourOrder' value:" + printDetailsPage.getYourOrderLicensePriceValue());
        System.out.println("'YourOrder' insurance value:" + printDetailsPage.getYourOrderInsuranceValue());
        System.out.println("'YourOrder' delivery price value:" + printDetailsPage.getYourOrderShippingPrice());
        System.out.println("OrderTotal value:" + printDetailsPage.getOrderTotalValue());

        Assert.assertTrue(printDetailsPage.getYourOrderLicensePriceValue().equals(licencePrice),"LicensePrice in YourOrder is wrong!");
        Assert.assertTrue(printDetailsPage.getYourOrderInsuranceValue().equals(insurance),"InsurancePrice in YourOrder is wrong!");
        Assert.assertTrue(printDetailsPage.getYourOrderShippingPrice().equals(delivery),"DeliveryPrice in YourOrder is wrong!");
        Assert.assertTrue(printDetailsPage.getOrderTotalValue().equals(orderTotal),"OrderTotal is wrong!");
    }
    //Test 1.959-1.960
    //Id
    @Test
    public void checkGoToHomeButton() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        printDetailsBO = new PrintDetailsBO();
        printDetailsReader = new PrintDetailsReader();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();
        printDetailsPage.clickOnGoToHomeButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(),"GoToHome button does not redirect user to HomePage!");
    }
    //Test 1.963
    //Id
    @Test
    public void checkIfUserIsRedirectedToHomePageAfterClickingOnXButton() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();
        printDetailsPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(), "User is not redirected to HomePage by clicking XButton!");
    }
    //Test
    //Id
    @Test
    public void checkIfGetOneRightButtonIsShownAfterRedirectingToHomePageFromPrintDetailsPage() throws InterruptedException {
        String getOneRightNowButtonText = "Get One Right Now";
        IDLHomePage idlHomePage = new IDLHomePage();
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = new PrintDetailsPage();
        printDetailsBO.goToPrintDetailsPage();
        printDetailsPage.clickOnXButton();
        System.out.println("GetOneRightNow button text:" + idlHomePage.checkContinueButtonForValid());
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(getOneRightNowButtonText),"GetOneRightNow button does not contains text GetOneRightNow!");
    }
    @Test
    public void test() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        idlHomePage.clickOnFAQButton();
        Assert.assertTrue(true);
    }
    @Test
    public void checkIfAddressPageIsPresent() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        idlHomePage.clickOnFAQButton();
        Assert.assertTrue(true);
    }
    @Test
    public void checkIfShippingCityCantContains() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        idlHomePage.clickOnFAQButton();
        Assert.assertTrue(true);
    }
    @Test
    public void testIDL() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        idlHomePage.clickOnFAQButton();
        Assert.assertTrue(true);
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
