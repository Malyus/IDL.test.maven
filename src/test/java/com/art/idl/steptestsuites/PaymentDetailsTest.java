package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.PaymentDetailsBO;
import com.art.idl.businessobjects.PrintDetailsBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.PaymentDetailsProperties;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.AddressPage;
import com.art.idl.steps.PaymentDetailsPage;
import com.art.idl.steps.PrintDetailsPage;
import org.aspectj.lang.annotation.Before;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/16/2017.
 */
public class PaymentDetailsTest {
    private WebDriver driver;
    private PaymentDetailsProperties paymentDetailsProperties = new PaymentDetailsProperties();
    private String creditCardNumber;
    private String expDateMonth;
    private String expDateYear;
    private String cvv;

    @BeforeTest
    public void initValidValuesOfTest(){
        creditCardNumber = paymentDetailsProperties.getCreditCardNumber();
        expDateMonth = paymentDetailsProperties.getExpDateMonth();
        expDateYear = paymentDetailsProperties.getExpDateYear();
        cvv = paymentDetailsProperties.getCvv();
    }
    //Id
    //Test 1.725
    @Test
    public void checkProgressBarForValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        System.out.println("PaymentDetailsPage progress-bar level:" + paymentDetailsPage.checkProgressBarForValid());
        Assert.assertTrue(paymentDetailsPage.checkProgressBarForValid().contains("width: 100%;"),"Something wrong with progress-bar!");
    }
    //Waiting for validation!
//    //Id
//    //Test 1.736
//    @Test
//    public void checkIfCreditCardnumberCantBeEmptyWhenAllValid() throws InterruptedException {
//        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
//        paymentDetailsBO.goToPaymentDetailsPage();
//        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
//        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
//        paymentDetailsBO.writeFieldsOfPaymentDetails("","12","2100","123");
//        //For update continue!
//        paymentDetailsPage.clickOnContinue();
//        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"CreditCard cant be empty!");
//    }
    //Id
    //Test 1.745
    @Test
    public void checkIfCreditCardNumberCanBeWithDigitsWhenAllValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,expDateMonth,expDateYear,cvv);
        //For update credit card number!
        paymentDetailsPage.clickOnValidContinue();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPlaceOrderPage(),"System allows to write digits into CreditCardNumber fields!");
    }
    //Id
    //Test 1.751
    @Test
    public void checkIfCreditCardNumberCantBeWithSpacesWhenAllValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails("1 1 1 1 1 1 1 1 1",expDateMonth,expDateYear,cvv);
        //For update continue!
        paymentDetailsPage.clickOnValidContinue();
       // Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not allow to write spaces into CreditCardNumber fields!");
    }
    //Id
    //Test 1.758
    @Test
    public void checkIfMonthCantBeEmptyWhenAllValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,"",expDateYear,cvv);
        //For update continue!
        //paymentDetailsPage.clickOnValidContinue();
        paymentDetailsPage.clickOnContinue();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not allow to leave empty Month field!");
    }
    //Id
    //Test 1.775
    @Test
    public void checkIFYearCantBeEmptyWhenAllValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,expDateMonth,"",cvv);
        //For update continue!
        paymentDetailsPage.clickOnContinue();
        //paymentDetailsPage.clickOnValidContinue();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not allow to leave empty Year field!");
    }
    //Id
    //Test 1.787
    @Test
    public void checkIfYearCantBeWithYearLessThanCurrent() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,expDateMonth,"2016",cvv);
        //For update continue!
        //paymentDetailsPage.clickOnContinue();
        paymentDetailsPage.clickOnValidContinue();
       // Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not allow to write year less than current into Year field!");
    }
    //Id
    //Test 1.792
    @Test
    public void checkIfCvvCantBeEmptyWhenAllValid() throws InterruptedException {
        /*Wait until validation of CVV */
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,expDateMonth,expDateYear,"");
        //For update continue!
        //paymentDetailsPage.clickOnContinue();
        //paymentDetailsPage.clickOnValidContinue();
        //Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not allow to leave empty CVV field!");
    }
    //Id
    //Test 1.808-1.809
    @Test
    public void checkIfBackButtonRedirectToAddressPage() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        AddressPage addressPage = new AddressPage();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsPage.clickOnBackButton();
        Assert.assertTrue(addressPage.checkIfUserIsOnAddressPage(),"Back button does not redirect user to AddressPage!");
    }
    //Id
    //Test 1.811-1.812
    @Test
    public void checkIfUserRedirectsToPlaceOrderPageWhenAllValid() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,expDateMonth,expDateYear,cvv);
        //For update credit card number!
        paymentDetailsPage.clickOnValidContinue();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPlaceOrderPage(),"User is not redirected to PlaceOrderScreen when all fields valid!");
    }
    //Id
    //Test 1.814
    @Test
    public void checkIfUserIsStayingOnThePaymentDetailsWhenSomeFieldsEmpty() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        paymentDetailsBO.writeFieldsOfPaymentDetails(creditCardNumber,"","",cvv);
        //For update continue button and Assert!
        paymentDetailsPage.clickOnContinue();
        //paymentDetailsPage.clickOnValidContinue();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not redirected to PlaceOrderScreen!");
    }
    //Id
    //Test 1.815
    @Test
    public void checkIfSystemDoesNotAllowToGoToPlaceOrderWithURL() throws InterruptedException {
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not on PaymentDetails page!");
        //SingletonDriver.getInstance().navigate().to("http://devtest93839283829.idlonline.com/order#/place-order");
        //Assert.assertTrue(paymentDetailsPage.checkPresentsOfPlaceOrderPage(),"User cant be redirected to PlaceOrder with URL!");
    }
    //Test 1.818
    //Id
    @Test
    public void checkIfUserIsRedirectedToHomePageByClickingOnXButton() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsPage.clickonXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(),"User is not redirected to HomePage by clicking on XButton!");
    }
    //Test 1.816
    //Id
    @Test
    public void checkIfUserIsRedirectedToFourthStepByURL() throws InterruptedException {
        LinkReader linkReader = new LinkReader();
        String fourthStep = linkReader.getLinkOfFourthStep();
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO.goToPaymentDetailsPage();
        System.out.println("Before:" + paymentDetailsPage.getCurrentURL());
        Thread.sleep(2000);
        paymentDetailsPage.clickOnBackButton();
        System.out.println("After:" + paymentDetailsPage.getCurrentURL());
        Assert.assertTrue(paymentDetailsPage.getCurrentURL().equals(fourthStep),"User is not redirected to FourthStep by using URL!");

    }
    //Test 1.818
    //Id
    @Test
    public void checkIfContinueButtonIsShownWhenUserRedirectedToHomePageFromPaymentDetails() throws InterruptedException {
        String continueButtonName = "Continue";
        IDLHomePage idlHomePage = new IDLHomePage();
        PaymentDetailsBO paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsPage.clickonXButton();
        Assert.assertTrue(idlHomePage.checkIfContinueButtonIsPresent(),"Continue button is not present on HomePage!");
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(continueButtonName),"Continue button does not contains text Continue!");
    }

    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
