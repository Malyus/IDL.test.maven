package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.PlaceOrderBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.PlaceOrderValuesReader;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.PaymentDetailsPage;
import com.art.idl.steps.PlaceOrderPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/20/2017.
 */
public class PlaceOrderTest {
    private WebDriver driver;
    private PlaceOrderBO placeOrderBO;
    private PlaceOrderPage placeOrderPage;
    private PlaceOrderValuesReader placeOrderValuesReader;
    private String licenseNumber;
    private String expirationDate;
    private String firstIssueDate;
    private String country;
    private String categories;

    //Test 1.824
    //Id
    @Test
    public void checkForValidProgressBar() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        System.out.println("PlaceOrderPage progress-bar level:" + placeOrderPage.checkForValidProgressBar());
        Assert.assertTrue(placeOrderPage.checkForValidProgressBar().contains("width: 100%;"),"Something wrong with progress-bar!");
    }

    //Test 1.830-1.838
    //Id
    @Test
    public void checkForValidDriverLicenseValues() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();

        licenseNumber = placeOrderValuesReader.getLicenseNumber();
        expirationDate = placeOrderValuesReader.getExpirationDate();
        firstIssueDate = placeOrderValuesReader.getFirstIssueDate();
        country = placeOrderValuesReader.getCountry();
        categories = placeOrderValuesReader.getCategories();

        System.out.println("License Number:" + placeOrderPage.getLicenseNumberValue());
        System.out.println("Expiration Date:" + placeOrderPage.getExpirationDateValue());
        System.out.println("First Issue Date:" + placeOrderPage.getFirstIssueDateValue());
        System.out.println("Country:" + placeOrderPage.getCountryValue());
        System.out.println("Categories:" + placeOrderPage.getCategoriesValue());

        Assert.assertTrue(placeOrderPage.getLicenseNumberValue().equals(licenseNumber), "LicenseNumber is wrong!");
        Assert.assertTrue(placeOrderPage.getExpirationDateValue().equals(expirationDate),"ExpirationDate is wrong!");
        Assert.assertTrue(placeOrderPage.getFirstIssueDateValue().equals(firstIssueDate),"FirstIssueDate is wrong!");
        Assert.assertTrue(placeOrderPage.getCountryValue().equals(country),"Country is wrong!");
        Assert.assertTrue(placeOrderPage.getCategoriesValue().equals(categories),"Categories are wrong!");
    }
    //Test 1.841 - 1.853
    //Id
    @Test
    public void checkForValidPersonalDetails() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();

        String fullName = placeOrderValuesReader.getFullName();
        String country = placeOrderValuesReader.getCountry();
        String streetAddress = placeOrderValuesReader.getStreetAddress();
        String city = placeOrderValuesReader.getCity();
        String state = placeOrderValuesReader.getState();
        String zip = placeOrderValuesReader.getZip();
        String email = placeOrderValuesReader.getEmail();

        System.out.println("FullName:" + placeOrderPage.getFullNameValue());
        System.out.println("Country:" + placeOrderPage.getCountryValue());
        System.out.println("StreetAddress:" + placeOrderPage.getStreetAddressValue());
        System.out.println("City:" + placeOrderPage.getCityValue());
        System.out.println("State:" + placeOrderPage.getStateValue());
        System.out.println("Zip:" + placeOrderPage.getZipValue());
        System.out.println("Email:" + placeOrderPage.getEmailValue());

        Assert.assertTrue(placeOrderPage.getFullNameValue().equals(fullName),"FullName is wrong!");
        Assert.assertTrue(placeOrderPage.getCountryValue().equals(country),"Country is wrong!");
        Assert.assertTrue(placeOrderPage.getStreetAddressValue().equals(streetAddress),"StreetAddress is Wrong!");
        Assert.assertTrue(placeOrderPage.getCityValue().equals(city),"City is wrong!");
        Assert.assertTrue(placeOrderPage.getStateValue().equals(state),"State is wrong!");
        Assert.assertTrue(placeOrderPage.getZipValue().equals(zip),"Zip is wrong!");
        Assert.assertTrue(placeOrderPage.getEmailValue().equals(email),"Email is wrong!");
    }
    //Test 1.857-1.867
    //Id
    @Test
    public void checkForValidShippingDetails() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderAndFillInShippingDetails();

        String shippingFullName = placeOrderValuesReader.getShippingFullName();
        String shippingCountry = placeOrderValuesReader.getShippingCountry();
        String shippingStreetAddress = placeOrderValuesReader.getShippingStreetAddress();
        String shippingCity =  placeOrderValuesReader.getShippingCity();
        String shippingState = placeOrderValuesReader.getShippingState();
        String shipingZip = placeOrderValuesReader.getShippingZip();

        System.out.println("FullName:" + placeOrderPage.getFullNameShippingValue());
        System.out.println("ShippingCountry:" + placeOrderPage.getCountryShippingValue());
        System.out.println("StreetAddress:" + placeOrderPage.getStreetAddressShippingValue());
        System.out.println("State:" + placeOrderPage.getStateShippingValue());
        System.out.println("City:" + placeOrderPage.getCityShippingValue());
        System.out.println("Zip:" + placeOrderPage.getZipShippingValue());


        Assert.assertTrue(placeOrderPage.getFullNameShippingValue().equals(shippingFullName),"ShippingFullName is wrong!");
        Assert.assertTrue(placeOrderPage.getCountryShippingValue().equals(shippingCountry),"ShippingCountry is wrong!");
        Assert.assertTrue(placeOrderPage.getStreetAddressShippingValue().equals(shippingStreetAddress),"ShippingStreetAddresss is wrong!");
        Assert.assertTrue(placeOrderPage.getStateShippingValue().equals(shippingState),"ShippingState is wrong!");
        Assert.assertTrue(placeOrderPage.getCityShippingValue().equals(shippingCity),"ShippingCity is wrong!");
        Assert.assertTrue(placeOrderPage.getZipShippingValue().equals(shipingZip), "ShippingZip is wrong!");
    }
    //Test 1.855
    //Id
    @Test
    public void checkValuesOfShippingForDuplicateWheItDisabledOnAddressPage() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();

        String fullName = placeOrderValuesReader.getFullName();
        String country = placeOrderValuesReader.getCountry();
        String streetAddress = placeOrderValuesReader.getStreetAddress();
        String city = placeOrderValuesReader.getCity();
        String state = placeOrderValuesReader.getState();
        String zip = placeOrderValuesReader.getZip();

        System.out.println("FullName:" + placeOrderPage.getFullNameShippingValue());
        System.out.println("ShippingCountry:" + placeOrderPage.getCountryShippingValue());
        System.out.println("StreetAddress:" + placeOrderPage.getStreetAddressShippingValue());
        System.out.println("State:" + placeOrderPage.getStateShippingValue());
        System.out.println("City:" + placeOrderPage.getCityShippingValue());
        System.out.println("Zip:" + placeOrderPage.getZipShippingValue());

        Assert.assertTrue(placeOrderPage.getFullNameShippingValue().equals(fullName),"ShippingFullName is wrong!");
        Assert.assertTrue(placeOrderPage.getCountryShippingValue().equals(country),"ShippingCountry is wrong!");
        Assert.assertTrue(placeOrderPage.getStreetAddressShippingValue().equals(streetAddress),"ShippingStreetAddresss is wrong!");
        Assert.assertTrue(placeOrderPage.getStateShippingValue().equals(state),"ShippingState is wrong!");
        Assert.assertTrue(placeOrderPage.getCityShippingValue().equals(city),"ShippingCity is wrong!");
        Assert.assertTrue(placeOrderPage.getZipShippingValue().equals(zip), "ShippingZip is wrong!");
    }
    //Test 1.887-1.878
    //Id
    @Test
    public void checkIfInsuranceButtonIsActivatedAndDeactivated() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("Before activating:" + placeOrderPage.checkIfInsuranceIsDeactivated());
        placeOrderPage.clickOnInsuranceButton();
        System.out.println("After activating:" + placeOrderPage.checkIfInsuranceIsActivated());
        Assert.assertTrue(placeOrderPage.checkIfInsuranceIsActivated().equals("true===true"),"IsnuranceButton is not activated!");
        placeOrderPage.clickOnInsuranceButton();
        System.out.println("After deactivating:" + placeOrderPage.checkIfInsuranceIsDeactivated());
        Assert.assertTrue(placeOrderPage.checkIfInsuranceIsDeactivated().equals("false===true"),"IsnuranceButton is not deactivated!");
    }
    //Test 1.880-1.881
    //Id
    @Test
    public void checkIfAdditionalLicenseIsActivatedAndDeactivate() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("Before activating:" + placeOrderPage.checkIfAdditionalLicenseIsDeactivated());
        placeOrderPage.clickOnAdditionalLicenseButton();
        System.out.println("After activating:" + placeOrderPage.checkIfAdditionalLicenseIsActivated());
        Assert.assertTrue(placeOrderPage.checkIfAdditionalLicenseIsActivated().equals("true===true"),"AdditionalLicenseButton is not activated!");
        placeOrderPage.clickOnAdditionalLicenseButton();
        System.out.println("After deactivating:" + placeOrderPage.checkIfAdditionalLicenseIsDeactivated());
        Assert.assertTrue(placeOrderPage.checkIfAdditionalLicenseIsDeactivated().equals("false===true"),"AdditionalLicenseButton is not deactivated!");
    }
    //Test 1.883,1.885
    //Id
    @Test
    public void checkIFAdditionalBookletIsActivatedAndDeactivated() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("Before acitvating:" + placeOrderPage.checkIfAdditionalBookletIsDeactivated());
        placeOrderPage.clickOnAdditionalBookletButton();
        System.out.println("After activating:" + placeOrderPage.checkIfAdditionalBookletIsActivated());
        Assert.assertTrue(placeOrderPage.checkIfAdditionalBookletIsActivated().equals("true===true"),"AdditionalBooklet is not activated!");
        placeOrderPage.clickOnAdditionalBookletButton();
        System.out.println("After deactivating:" + placeOrderPage.checkIfAdditionalBookletIsDeactivated());
        Assert.assertTrue(placeOrderPage.checkIfAdditionalLicenseIsDeactivated().equals("false===true"),"AdditionalBookletButton is not disabled!");
    }
    //Test 1.884
    //Id
    @Test
    public void checkIfAllThreeButtonsCanBeActivated() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnInsuranceButton();
        placeOrderPage.clickOnAdditionalLicenseButton();
        placeOrderPage.clickOnAdditionalBookletButton();
        Assert.assertTrue(placeOrderPage.checkIfInsuranceIsActivated().equals("true===true"),"IsnuranceButton is not activated!");
        Assert.assertTrue(placeOrderPage.checkIfAdditionalLicenseIsActivated().equals("true===true"),"AdditionalLicenseButton is not activated!");
        Assert.assertTrue(placeOrderPage.checkIfAdditionalBookletIsActivated().equals("true===true"),"AdditionalBooklet is not activated!");

    }
    //Test 1.887
    //Id
    @Test
    public void checkIfFirstCheckboxIsActiveByDefault() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("Active of FirstCheckbox by default:" + placeOrderPage.checkActiveOfFirstCheckbox());
        Assert.assertTrue(placeOrderPage.checkActiveOfFirstCheckbox().equals("ng-pristine ng-untouched ng-valid ng-not-empty"),"FirstCheckbox is not active by default!");
        //ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse
        Assert.assertTrue(true);
    }
    //Test 1.894
    //Id
    @Test
    public void checkIfActiveCheckboxCantBeDeactivated() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("Before activating:" + placeOrderPage.checkActiveOfSecondCheckbox());
        placeOrderPage.clickOnSecondCheckbox();
        System.out.println("After activating:" + placeOrderPage.checkActiveOfSecondCheckbox());
        Assert.assertTrue(placeOrderPage.checkActiveOfSecondCheckbox().equals("ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse"),"Second checkbox is not activated!");
        placeOrderPage.clickOnSecondCheckbox();
        System.out.println("After second clicking:" + placeOrderPage.checkActiveOfSecondCheckbox());
        Assert.assertTrue(placeOrderPage.checkActiveOfSecondCheckbox().equals("ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse"),"Second checkbox is disabled after second clicking!");
    }
    //Test 1.897
    //Id
    @Test
    public void checkPresentsOfActivatedCheckboxInYourOrder() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnSecondCheckbox();
        System.out.println("Value of second checkbox in YourOrder" + placeOrderPage.getValueFromYourOrderSecondCheckbox());
        Assert.assertTrue(placeOrderPage.getValueFromYourOrderSecondCheckbox().equals("$45.00"),"Value of second checkbox in YourOrder is not valid!");
    }

    //Test
    //Id 1.898
    @Test
    public void checkIfInsurancePriceIsShownInYourOrderAfterActivating() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnInsuranceButton();
        Assert.assertTrue(placeOrderPage.checkPresentsOfInsuranceInYourOrderAfterItActivating(),"Isurance is not showing in YourOrder after it activating!");
    }
    //Update order total assert value!
    //Test 1.901
    //Id
    @Test
    public void checkIfOrderTotalContainsValueOfYourOrder() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("OrderTotal value:" + placeOrderPage.getOrderTotalValue());
        Assert.assertTrue(placeOrderPage.getOrderTotalValue().equals("$19.00"),"OrderTotal is wrong");
    }
    //Test 1.905-1.906
    //Id
    @Test
    public void checkIfBackButtonIsRedirectedToPaymentDetailsPage() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderValuesReader = new PlaceOrderValuesReader();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnBackButton();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not redirected to PaymentDetailsPage!");
    }
//    //Test 1.909-1.910
//
//    @Test
//    public void checkIfContinueButtonRedirectedToPrintDetailsPage() throws InterruptedException {
//        placeOrderBO = new PlaceOrderBO();
//        placeOrderPage = new PlaceOrderPage();
//        placeOrderValuesReader = new PlaceOrderValuesReader();
//        placeOrderBO.goToPlaceOrderPage();
//
//    }
    //Test 1.912
    //Id
    @Test
    public void checkIfUserIsRedirectedToFifthStepByURL() throws InterruptedException {
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnBackButton();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"User is not redirected to PaymentDetailsPage by URL!");
    }
    //Bug!
    //Test 1.911
    //Id
    @Test
    public void checkIfUserIsRedirectedToPrintDetailsPage() throws InterruptedException {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderBO.goToPlaceOrderPage();
        //placeOrderPage.goToPrintDetailsStepByURL();
        Assert.assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),"User is not redirected to PrintDetailsPage by URL!");
    }

    //Test 1.914
    //Id
    @Test
    public void checkIfUserIsRedirectedByClickingOnXButtonToHomePage() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(),"User is not redirected to HomePage by clicking on XButton!");
    }
    //Test 1.915
    //Id
    @Test
    public void checkIfContinueButtonIsShownAfterRedirectingToHomePageFromPlaceOrderPage() throws InterruptedException {
        String continueButtonText = "Continue";
        IDLHomePage idlHomePage = new IDLHomePage();
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfContinueButtonIsPresent(),"Continue button is not present on HomePage!");
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(continueButtonText),"Continue button does not contains text Continue!");
    }
    //Test 1.899
    //Id
    @Test
    public void checkIfAdditionalServicesIsNotInYourOrderByDefault() throws InterruptedException {
        String nameOfHiddenClass = "col-xs-12 col-sm-12 ng-hide";
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = new PlaceOrderPage();
        placeOrderBO.goToPlaceOrderPage();
        System.out.println("First:" + placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(0));
        System.out.println("Second:" + placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(1));
        System.out.println("Third:" + placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(2));
        Assert.assertTrue(placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(0).equals(nameOfHiddenClass), "First insurance is not hidden in YourOrder!");
        Assert.assertTrue(placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(1).equals(nameOfHiddenClass),"Second insurance is not hidden in YourOrder!");
        Assert.assertTrue(placeOrderPage.getAriaHiddenOfInsurancesOfYourOrder(2).equals(nameOfHiddenClass),"Third insurance is not hidden in YourOrder!");
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
