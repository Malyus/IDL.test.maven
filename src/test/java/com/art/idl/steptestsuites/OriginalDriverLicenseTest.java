package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.OriginalDriverLicenseBO;
import com.art.idl.businessobjects.PersonalInformationBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.ImagesPage;
import com.art.idl.steps.OriginalDriverLicensePage;
import com.art.idl.steps.PersonalInformationPage;
import com.art.idl.steptestsuites.ImagesPageTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 11/10/2017.
 */
public class OriginalDriverLicenseTest {
    private WebDriver driver;
    private OriginalDriverLicenseBO originalDriverLicenseBO;
    private OriginalDriverLicensePage originalDriverLicensePage;

    //Id
    //Test 1.332
    @Test
    public void checkProgressBarOfOriginalDriverLicense() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        System.out.println("Width:" + originalDriverLicensePage.checkProgressBarOfOriginalDriverLicensePage());
        Assert.assertTrue(originalDriverLicensePage.checkProgressBarOfOriginalDriverLicensePage().contains("width: 40%;"),"Smth with progress bar of OriginalDriverLicense!");
    }
    //Id
    //Test 1.337
    @Test
    public void checkIfLicenseNumberCantBeEmptyWhenAllValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("","Ukraine");
        originalDriverLicensePage.clickOnCountinueButton();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(),"System allow to leave empty LicenseNumber field!");
    }
    //Id
    //Test 1.350
    @Test
    public void checkIfLicenseNumberCantBeWithSpaceOnlyAllValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("   ","Ukraine");
//        originalDriverLicensePage.clickOnCountinueButton();
        //Button does not work
        originalDriverLicensePage.clickOnCountinueButtonWhenAllWithValid();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(),"System allow to paste spaces in LicenseNumber field!");
    }
    //Id
    //Test 1.433-1.435
    @Test
    public void checkACategoryButtonForValid(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.chooseACategory();
        originalDriverLicensePage.disableACategory();
    }
    //Id
    //Test 1.436-1.438
    @Test
    public void checkBCategoryButtonForValid(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        //She is active for default!
        originalDriverLicensePage.disableBCategory();
        originalDriverLicensePage.chooseBCategory();
    }
    //Id
    //Test 1.439-1.441
    @Test
    public void checkCCategoryButtonForValid(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.chooseCCategory();
        originalDriverLicensePage.disableCCategory();
    }
    //Id
    //Test 1.442-1.444
    @Test
    public void checkDCategoryButtonForValid(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.chooseDCategory();
        originalDriverLicensePage.disableDCategory();
    }
    //Id
    //Test 1.445-1.447
    @Test
    public void checkECategoryButtonForValid(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.chooseECategory();
        originalDriverLicensePage.disableECategory();
    }
    //Id
    //Test 1.448
    @Test
    public void checkIfSystemAllowsToActivateAllCategoriesButtons(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.activateAllDriverLicenseCategoriesButtons();
    }
    //Id
    //Test 1.453
    @Test
    public void checkIfSystemAllowsToDownloadJPEGFormat(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.uploagJPEGFormat();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPEGForamat(),"System does not allow to download JPEG format!");
    }

    //Id
    //Test 1.454
    @Test
    public void checkIfSystemAllowsToDownloadJPGFormat(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.uploadPhotoOfDriverLicense();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPGFormat(),"System does not allow to download JPG format!");
    }
    //Id
    //Test 1.457
    @Test
    public void checkIfUploadButtonChangedToChangePhotoButton() {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.uploadPhotoOfDriverLicense();
        System.out.println("Name of Button:" + originalDriverLicensePage.checkIfUploadChangeToChangePhotoButton());
        Assert.assertTrue(originalDriverLicensePage.checkIfUploadChangeToChangePhotoButton().contains("Change Photo"),"UploadPhoto button does not changed to ChangePhoto button!");
    }
    //Id
    //Test 1.458
    @Test
    public void checkIfJPEGCanBeUploadByChangePhotoButton(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.uploadPhotoOfDriverLicense();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPGFormat(),"System does not allow to download JPG format by UploadPhoto button!");
        originalDriverLicensePage.uploagJPEGFormat();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPEGForamat(),"System does not allow to download JPEG format by ChangePhoto button!");
    }
    //Id
    //Test 1.459
    @Test
    public void checkIfJPGCanBeUploadByChangePhotoButton(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.uploagJPEGFormat();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPEGForamat(),"System does not allow to download JPEG format by UploadPhoto button!");
        originalDriverLicensePage.uploadPhotoOfDriverLicense();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfJPGFormat(),"System does not allow to download JPG format by ChangePhoto button!");
    }
    //Id
    //Test 1.462-1.463
    @Test
    public void checkIfBackButtonRedirectToPersonalInformationPage(){
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.clickOnBackButton();
        Assert.assertTrue( personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"),"Back button does not redirect user to PersonalInformation");
    }
    //Id
    //Test 1.466-1.467
    @Test
    public void checkIfCountinueButtonRedirectToNextStepWhenAllValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("CA3256","Ukraine");
        originalDriverLicensePage.clickOnCountinueButtonWhenAllWithValid();
        Assert.assertTrue(originalDriverLicensePage.checkPresentsOfThirdStep());
    }
    //Id
    //Test 1.468
    @Test
    public void checkIfUserDoesNotRdirectedToNextStepWhenSomeEmpty() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("","Ukraine");
        originalDriverLicensePage.clickOnCountinueButton();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(),"User redirected to next step!");
    }
    //Test 1.338
    //Id
    @Test
    public void checkIfLicenseNumberIsRedWhenItWithInvalid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        String colorOfLicenseNumberField = "rgb(255, 0, 0)";
        System.out.println("Before:" + originalDriverLicensePage.getCssValueOfLicenseNumberField());
        originalDriverLicensePage.writeLicenseNumber("/");
        originalDriverLicensePage.clickOnCountinueButton();
        System.out.println("After:" + originalDriverLicensePage.getCssValueOfLicenseNumberField());
        Assert.assertTrue(originalDriverLicensePage.getCssValueOfLicenseNumberField().equals(colorOfLicenseNumberField),"LicenseNumber is not marker red when it with invalid!");
    }
    //Test 1.338
    //Id
    @Test
    public void checkIfLicenseNumberIsRedWhenItEmptyAndAllValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        String colorOfLicenseNumberField = "rgb(255, 0, 0)";
        System.out.println("Before:" + originalDriverLicensePage.getCssValueOfLicenseNumberField());
        originalDriverLicenseBO.writeToFieldsOfOriginalDriverLicenseStep("","Ukraine");
        originalDriverLicensePage.clickOnCountinueButton();
        System.out.println("After:" + originalDriverLicensePage.getCssValueOfLicenseNumberField());
        Assert.assertTrue(originalDriverLicensePage.getCssValueOfLicenseNumberField().equals(colorOfLicenseNumberField),"LicenseNumber is not red when it empty and all valid!");
    }
    //Test 1.358,1.365,1.374,1.385,1.392,1.401,1.411,1.456
    //Id
    @Test
    public void checkIfMonthIsRedWhenNoOneIsSelected() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        SoftAssert softAssert = new SoftAssert();
        String redColor = "rgb(255, 0, 0)";
        String errorMessageText = "Drivers License Image is required.";
        String uploadErrorMessage = "This field is required.";

        System.out.println("ExpMonth before:" + originalDriverLicensePage.getCssValueOfExpirationMonth());
        System.out.println("ExpDay before:" + originalDriverLicensePage.getCssValueOfExpirationDay());
        System.out.println("ExpYear before:" + originalDriverLicensePage.getCssValueOfExpirationYear());

        System.out.println("LicenseMonth before:" + originalDriverLicensePage.getCssValueOfLicenseMonth());
        System.out.println("LicenseDay before:" + originalDriverLicensePage.getCssValueOfLicenseDay());
        System.out.println("LicenseYear before:" + originalDriverLicensePage.getCssValueOfLicenseYear());
        System.out.println("Country before:" + originalDriverLicensePage.getCssValueOfCountryInput());

        originalDriverLicensePage.clickOnCountinueButton();
        System.out.println("ExpMonth after:" + originalDriverLicensePage.getCssValueOfExpirationMonth());
        System.out.println("ExpDay after:" + originalDriverLicensePage.getCssValueOfExpirationDay());
        System.out.println("ExpYear after:" + originalDriverLicensePage.getCssValueOfExpirationDay());
        System.out.println("LicenseMonth after:" + originalDriverLicensePage.getCssValueOfLicenseMonth());
        System.out.println("LicenseDay after:" + originalDriverLicensePage.getCssValueOfLicenseDay());
        System.out.println("LicenseYear after:" + originalDriverLicensePage.getCssValueOfLicenseYear());
        System.out.println("Text of ErrorMessage:" + originalDriverLicensePage.getTextOfImageUploadErroMessage());
        System.out.println("Country after:" + originalDriverLicensePage.getCssValueOfCountryInput());

        softAssert.assertTrue(originalDriverLicensePage.getCssValueOfLicenseMonth().equals(redColor),"LicenseMonth is not marked red when it's not selected!");
        softAssert.assertTrue(originalDriverLicensePage.getCssValueOfLicenseDay().equals(redColor),"LicenseDay is not marked red when it's not selected!");
        softAssert.assertTrue(originalDriverLicensePage.getCssValueOfLicenseYear().equals(redColor),"LicenseYear is not marked red when it's not selected!");
        softAssert.assertTrue(originalDriverLicensePage.getTextOfImageUploadErroMessage().equals(errorMessageText),"Error message is not valid!");
        softAssert.assertTrue(originalDriverLicensePage.getCssValueOfCountryInput().equals(redColor),"Country is not marked with red when no one country is selected!");
        //softAssert.assertTrue(originalDriverLicensePage.getUploadErrorMessage().equals(uploadErrorMessage),"Upload error message is not valid or not shown!");

        Assert.assertTrue(originalDriverLicensePage.getCssValueOfExpirationMonth().equals(redColor),"ExpMonth is not red when it's not selected!");
        Assert.assertTrue(originalDriverLicensePage.getCssValueOfExpirationDay().equals(redColor),"ExpDay is not red when it's not selected!");
        Assert.assertTrue(originalDriverLicensePage.getCssValueOfExpirationYear().equals(redColor),"ExpYear is not red when it's not selected!");
    }
    //Test 1.470
    //Id
    @Test
    public void checkIfUserIsRedirectedToFirstStepByUsingURL() throws InterruptedException {
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.goToFirstStep();
        Assert.assertTrue(personalInformationPage.checkPresentsOfPersonalInformationPage(),"User is not redirected to FirstStep by using URL!");
    }
    //Test 1.472
    //Id
    @Test
    public void checkIfXButtonRedirectUserToHomeScreen(){
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        IDLHomePage idlHomePage = new IDLHomePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(),"User is not on OriginalDriverLicensePage!");
        originalDriverLicensePage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(), "User is not redirected to HomePage by clicking on XButton!");
    }
    //Test 1.469
    //Id
    @Test
    public void checkIfUserCantBeRedirectedToThirdStepByURL() throws InterruptedException {
        LinkReader linkReader = new LinkReader();
        String thirdStepLink = linkReader.getLinkOfThirdStep();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        System.out.println("Before:" + originalDriverLicensePage.getCurrentURL());
        originalDriverLicensePage.clickOnCountinueButton();
        System.out.println("After:" + originalDriverLicensePage.getCurrentURL());
        Thread.sleep(2000);
        Assert.assertFalse(originalDriverLicensePage.getCurrentURL().equals(thirdStepLink),"User cant be redirected to next step by URL!");
    }
    //Test 1.473
    //Id
    @Test
    public void checkIfContinueButtonIsShownAfterPassingFirstStep(){
        String continueButtonText = "Continue";
        IDLHomePage idlHomePage = new IDLHomePage();
        originalDriverLicensePage = new OriginalDriverLicensePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        originalDriverLicenseBO.goToOriginalDriverLicensePage();
        originalDriverLicensePage.clickOnXButton();
        idlHomePage.checkIfHomeScreenIsPresent();
        Assert.assertTrue(idlHomePage.checkIfContinueButtonIsPresent(),"Continue button is not displayed!");
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(continueButtonText),"Continue button is dont contains 'Continue' text!");

    }
//    //Test 1.452
//    //Id
//    @Test
//    public void checkIfMessageWithValidFormatsIsDisplayedAfterDownloadingPhotoWithNotValidFormat(){
//        String validFormatsOfPhotoMessage = "Only jpg/jpeg and png files are allowed!";
//        originalDriverLicensePage = new OriginalDriverLicensePage();
//        originalDriverLicenseBO = new OriginalDriverLicenseBO();
//        originalDriverLicenseBO.goToOriginalDriverLicensePage();
//        originalDriverLicensePage.uploadXMLFormat();
//        System.out.println("Text of ValidFormats:" + originalDriverLicensePage.checkTextOfValidFormats());
//        Assert.assertEquals(originalDriverLicensePage.checkTextOfValidFormats(),validFormatsOfPhotoMessage,"MessageWithValid formats is not shown or text is not valid!");
//    }
//    //Test 1.455
//    //Id
//    @Test
//    public void checkIfSystemDoesNotAllowsToDownloadPhotoWithWrongFormat() throws InterruptedException {
//        originalDriverLicensePage = new OriginalDriverLicensePage();
//        originalDriverLicenseBO = new OriginalDriverLicenseBO();
//        originalDriverLicenseBO.writeAllFieldsAndDownloadWrongFormat();
//        Thread.sleep(1000);
//        Assert.assertTrue(originalDriverLicensePage.checkIfOriginalDriverLicenseIsPresent(),"System allows to download photo with wrong format and go to next step!");
//    }
//    //Test 1.460
//    //Id
//    @Test
//    public void checkIfSystemDoesNotAllowsToDownloadPhotoWithWrongFormatByChangePhotoButton() throws InterruptedException {
//        String validFormatsOfPhotoMessage = "Only jpg/jpeg and png files are allowed!";
//        originalDriverLicenseBO = new OriginalDriverLicenseBO();
//        originalDriverLicensePage = new OriginalDriverLicensePage();
//        originalDriverLicenseBO.goToOriginalDriverLicensePage();
//        originalDriverLicensePage.uploagJPEGFormat();
//        originalDriverLicenseBO.writeAllFieldsAndWrongPhotoFormatByChangePhoto();
//        Thread.sleep(1000);
//        Assert.assertEquals(originalDriverLicensePage.checkTextOfValidFormats(),validFormatsOfPhotoMessage,"System allows to download photo with wrong format by ChangePhoto button!");
//    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
