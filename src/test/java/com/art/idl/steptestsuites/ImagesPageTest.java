package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.ImagesBO;
import com.art.idl.businessobjects.OriginalDriverLicenseBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.LinkReader;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.AddressPage;
import com.art.idl.steps.ImagesPage;
import com.art.idl.steps.OriginalDriverLicensePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/13/2017.
 */
public class ImagesPageTest {
    private WebDriver driver;
    private LinkReader linkReader = new LinkReader();
    private String linkOfFourthStep = linkReader.getLinkOfFourthStep();
    private OriginalDriverLicenseBO originalDriverLicenseBO;
    private OriginalDriverLicensePage originalDriverLicensePage;
    private ImagesPage imagesPage;
    //Id
    //Test 1.478
    @Test
    public void checkProgressBarForValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        Assert.assertTrue(imagesPage.checckProgressBarLevel().contains("width: 60%;"),"Something with progress-bar");
    }
    //Id
    //Test 1.485,1.487
    @Test
    public void checkIfSystemAllowsToDownloadJPEG() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        imagesPage.uploadJPEGOfYourSelf();
        Assert.assertTrue(imagesPage.checkPresentsOfUploadedImage(),"Uploaded photo does not displayed!");
    }
    //Id
    //Test 1.488
    @Test
    public void checkIfSystemAllowsToDownloadJPG() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        imagesPage.uploadJPGOfYourSelf();
        Assert.assertTrue(imagesPage.checkPresentsOfUploadedImage(),"Uploaded photo does not displayed!");
    }
    //Id
    //Test 1.490-1.492
    @Test
    public void checkIfUploadPhotoButtonChangedToChengePhotoButtonAndIfUserCanUploadNewJPEGAndNewJPG() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        imagesPage.uploadJPEGOfYourSelf();
        Assert.assertTrue(imagesPage.getTextFromUploadButton().contains("Change Photo"));
        imagesPage.uploadJPGOfYourSelf();
        Assert.assertTrue(imagesPage.checkPresentsOfUploadedImage(),"New JPG does not displayed!");
        imagesPage.uploadJPEGOfYourSelf();
        Assert.assertTrue(imagesPage.checkPresentsOfUploadedImage(),"New JPEG does not displayed!");
    }
    //Id
    //Test 1.497-1.498
    @Test
    public void checkIfPhotoRotatesToTheLeftByLeftRotateButton() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        imagesPage.uploadJPEGOfYourSelf();
        String widthBeforeRotatesLeft = imagesPage.checkIfLeftButtonRotateToTheLeft();
        System.out.println("Width:" + imagesPage.checkIfLeftButtonRotateToTheLeft());
        imagesPage.clickOnLeftButton();
        Thread.sleep(1000);
        String widthAfterRotatesLeft = imagesPage.checkIfLeftButtonRotateToTheLeft();
        System.out.println("Width after rotates:" + imagesPage.checkIfLeftButtonRotateToTheLeft());
        Assert.assertFalse(widthBeforeRotatesLeft.equals(widthAfterRotatesLeft),"Rotates left does not work correctly!");
    }
    //Id
    //Test 1.500-1.501
    @Test
    public void checkIfPhotoRotatesToTheRightByRightRotateButton() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage");
        imagesPage.uploadJPEGOfYourSelf();
        String widthBeforeRotatesLeft = imagesPage.checkIfLeftButtonRotateToTheLeft();
        System.out.println("Width:" + imagesPage.checkIfLeftButtonRotateToTheLeft());
        imagesPage.clickOnLeftButton();
        Thread.sleep(1000);
        String widthAfterRotatesLeft = imagesPage.checkIfLeftButtonRotateToTheLeft();
        System.out.println("Width after rotates left:" + imagesPage.checkIfLeftButtonRotateToTheLeft());
        Assert.assertFalse(widthBeforeRotatesLeft.equals(widthAfterRotatesLeft),"Rotates left does not work correctly!");
        imagesPage.clickOnRightButton();
        Thread.sleep(1000);
        String widthAfterRotatesRight = imagesPage.checkIfLeftButtonRotateToTheRight();
        System.out.println("Width after rotates right:" + widthAfterRotatesRight);
        Assert.assertTrue(widthAfterRotatesRight.equals(widthBeforeRotatesLeft), "Rotate right does not work correctly!");
    }
    //Id
    //Test 1.502
    @Test
    public void checkIfCropButtonWorkCorrectly() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.uploadJPEGOfYourSelf();
        imagesPage.clickOnCropButton();
        Assert.assertTrue(imagesPage.checkIfCropPhotoPresent(), "Crop button does not work correctly!");
    }
    //Id
    //Test 1.507
    @Test
    public void checkIfUserCanWriteElectronicSignatureByMouse() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.makeADigitalSignature();
        imagesPage.clickOnSaveYourSignatureButton();
        Assert.assertTrue(imagesPage.checkIfSaveSignatureIsDisplayed(),"Digital Signature does not work correctly!");
    }
    //Wait for validation of DigitalSignature!
    //Id
    //Test 1.512-1.513
    @Test
    public void checkIfClearButtonWorkCorrectly() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.makeADigitalSignature();
        imagesPage.clickOnClearButton();
        Thread.sleep(2000);
        Assert.assertTrue(imagesPage.checckProgressBarLevel().equals("width: 60%;"),"Smth with clear button or DigitalSignature!");
    }
    //Id
    //Test 1.514-1.515
    @Test
    public void checkBackButtonForValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.clickOnBackButton();
        Assert.assertTrue(imagesPage.checkIfBackButtonReturnToPreviousStep(),"Back button does not work correctly!");
    }
    //Waiting for validation of ImagesPage
    //Id
    //Test 1.519-1.520
    @Test
    public void checkIfContinueButtonDoesNotRedirectToNextStepWhenAllValid() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        ImagesBO imagesBO = new ImagesBO();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesBO.stayOnTheSameWhenSomeFieldsEmpty();
        Thread.sleep(1500);
        Assert.assertTrue(imagesPage.checckProgressBarLevel().equals("width: 60%;"), "Continue does not work correclty!");
    }

    //Id
    //Test 1.525
    @Test
    public void checkIfXButtonRedirectUserToHomePage() throws InterruptedException {
        IDLHomePage idlHomePage = new IDLHomePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        Assert.assertTrue(imagesPage.checkPresentsOfImagesPage(),"User is not on ImagesPage!");
        imagesPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfHomeScreenIsPresent(),"User is not redirected to HomePage by clicking XButton!");
    }
    //Id
    //Test 1.522
    @Test
    public void checkIfUserDoesNotRedirectToFourthStepByURL() throws InterruptedException {
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        AddressPage addressPage = new AddressPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.clickOnContinueButton();
        Assert.assertFalse(imagesPage.getCurrentURL().equals(linkOfFourthStep),"User is redirected to fourth step by using link of fourth step!");
        System.out.println("Current URL:" + imagesPage.getCurrentURL());
        System.out.println("URL of FourthStep:" + linkOfFourthStep);
    }
    //Id
    //Test 1.523
    @Test
    public void checkIfUserIsRedirectedToSecondStepByURL() throws InterruptedException {
        LinkReader linkReader = new LinkReader();
        String urlOfSecondSection = linkReader.getLinkOfSecondStep();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.goToSecondStepByLink();
        Assert.assertTrue(imagesPage.getCurrentURL().equals(urlOfSecondSection),"User is not redirected to second step by URL!");
    }
    //Id
    //Test 1.526
    @Test
    public void checkIfContinueIsDisplayedAfterReturningBackToHomePageFromImages() throws InterruptedException {
        String continuButtonText = "Continue";
        IDLHomePage idlHomePage = new IDLHomePage();
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.clickOnXButton();
        Assert.assertTrue(idlHomePage.checkIfContinueButtonIsPresent(),"Continue button is not displayed!");
        Assert.assertTrue(idlHomePage.checkContinueButtonForValid().equals(continuButtonText), "Continue button does not include 'Continue' text!");
    }
    //Id
    //Test 1.486
    @Test
    public void checkIfListOfValidFormatsOfPhotoIsDisplayedAfterDownloadingWrongFormat() throws InterruptedException {
        String validFormatMessageText = "Only jpg/jpeg and png files are allowed!";
        originalDriverLicenseBO = new OriginalDriverLicenseBO();
        imagesPage = new ImagesPage();
        originalDriverLicenseBO.getToThirdStep();
        imagesPage.uploadXMLFile();
        System.out.println("Valid format message text:" + imagesPage.getMessageWithValidFormatsOfPhoto());
        Assert.assertEquals(imagesPage.getMessageWithValidFormatsOfPhoto(),validFormatMessageText,"Message with valid formats is not shown or text of message is not valid!");
    }
//    //Id
//    //Test 1.489
//    @Test
//    public void checkIfSystemDoesNotAllowsToDownloadPhotoWithWrongFormatAndGetToNextStep() throws InterruptedException {
//        AddressPage addressPage = new AddressPage();
//        originalDriverLicenseBO = new OriginalDriverLicenseBO();
//        imagesPage = new ImagesPage();
//        originalDriverLicenseBO.getToThirdStep();
//        imagesPage.uploadXMLFile();
//        imagesPage.acceptValidMessage();
//        imagesPage.clickOnCropButton();
//        imagesPage.makeADigitalSignature();
//        imagesPage.clickOnSaveYourSignatureButton();
//        imagesPage.clickOnValidContinue();
//        Assert.assertFalse(addressPage.checkIfUserIsOnAddressPage(),"System allows to download PhotoWith wrong format and get to AddressPage!");
//    }
//    //Id
//    //Test 1.493
//    @Test
//    public void checkIfSystemDoesNotAllowToDownloadWrongFormatOfPhotoByChangePhotoButton() throws InterruptedException {
//        AddressPage addressPage = new AddressPage();
//        originalDriverLicenseBO = new OriginalDriverLicenseBO();
//        originalDriverLicensePage = new OriginalDriverLicensePage();
//        imagesPage = new ImagesPage();
//        originalDriverLicenseBO.getToThirdStep();
//        imagesPage.uploadJPEGOfYourSelf();
//        imagesPage.uploadXMLFile();
////        imagesPage.acceptValidMessage();
//        imagesPage.clickOnCropButton();
//        imagesPage.makeADigitalSignature();
//        imagesPage.clickOnSaveYourSignatureButton();
//        imagesPage.clickOnValidContinue();
//        Assert.assertFalse(addressPage.checkIfUserIsOnAddressPage(),"System allows to download photo with wrong format by ChangePhoto button and get to AddressPage!");
//    }

    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
