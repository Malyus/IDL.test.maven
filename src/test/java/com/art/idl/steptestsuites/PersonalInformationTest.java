package com.art.idl.steptestsuites;

import com.art.idl.businessobjects.PersonalInformationBO;
import com.art.idl.pages.IDLHomePage;
import com.art.idl.properties.LinkReader;
import com.art.idl.properties.PersonalInformationValuesReader;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.steps.PersonalInformationPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/9/2017.
 */
public class PersonalInformationTest {
    private WebDriver driver = SingletonDriver.getInstance();
    private LinkReader linkReader = new LinkReader();
    private PersonalInformationPage personalInformationPage;
    private PersonalInformationValuesReader personalInformationValuesReader = new PersonalInformationValuesReader();
    private String firstName = personalInformationValuesReader.getFirstName();
    private String lastName = personalInformationValuesReader.getLastName();
    private String email = personalInformationValuesReader.getEmail();
    private String telephoneNumber = personalInformationValuesReader.getTelephoneNumber();
    private String secondStepURL = linkReader.getLinkOfSecondStep();
    private String homePageURL = linkReader.getLinkOfHomePage();

    //Id
    //Test 1.187
    @Test
    public void checkProgressBarOfPErsonalInformationPage(){
        personalInformationPage = new PersonalInformationPage();
        personalInformationPage.clickOnPricingButtonOnPricingScreen();
        Assert.assertTrue(personalInformationPage.checkPresentsOfPricingScreen(),"User is not on PricingPage!");
        personalInformationPage.clickOnGetYourIDLButton1yearLicense();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "User is not on PersonalInformationPage!");
        System.out.println("Width:" + personalInformationPage.checkProgressBarOfPersonalInformationPage());
        Assert.assertTrue(personalInformationPage.checkProgressBarOfPersonalInformationPage().contains("width: 20%;"));
    }
    //Id
    //Test 1.191
    @Test
    public void checkIfFirstNameFieldCanBeEmptyWhenOtherWithValidData()   {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage("",lastName,email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to leave epmty FirstName field!");
    }
    //Id
    //Test 1.200
    @Test
    public void checkIfFirstNameFieldCanBeFiillInWithDigits()   {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage("1234",lastName,email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to fill in firstName With digits!");
    }
    //Id
    //Test 1.207
    @Test
    public void checkIfLastNameFieldCantBeEmpty(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,"",email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to leave epmty LastName field!");
    }
    //Id
    //Test 1.215
    @Test
    public void checkIfLastNameFieldCanBeFillInWithDigits(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,"1234",email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to fill in firstName with digits!");
    }
    //Id
    //Test 1.218
    @Test
    public void checkIfLastNameFieldCantBeWithAFewSpaces(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,"          ",email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to fill in firstName with a few spaces!");
    }
    //Id
    //Test 1.270
    @Test
    public void checkIfCmDropdownIsShwonAfterClickOnCmButton(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.selectHeight();
        Assert.assertTrue(personalInformationPage.presentsOfCmDropdown(), "Cm dropdown is not shown!");
    }
    //Id
    //Test 1.281
    @Test
    public void checkIfPhoneFieldCantBeEmptyWheAllValid(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,"");
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to leave epmty Phone field!");
    }
    //Id
    //Test 1.295
    @Test
    public void checkIfPhoneFieldCantBeWithSpaces(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,"    ");
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to fill in Phone field with spaces!");
    }
    //Id
    //Test 1.301
    @Test
    public void checkIfEmailCantBeEmptyWhenAllValid(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,"",telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"), "System allows to leave epmty Email field!");
    }
    //Id
    //Test 1.311
    @Test
    public void checkIfEmailCantBeWithaAFewSpaces(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,"   ",telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"),"System allows to fill in with spaces Email field!");
    }
    //Id
    //Test 1.317-318
    @Test
    public void checkIfBackButtonRediretUserToHomePage(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.clickOnBackButton();
        Assert.assertTrue(personalInformationPage.checkPresentsOfPricingScreen(),"Back button doesnt work correctly!");
    }
    //Id
    //Test 1.323
    @Test
    public void checkIfUserRedirectToNextStepCountinuButtonVailDataAll() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,telephoneNumber);
        personalInformationPage.clickOnCountinueButtonWhenAllWithValid();
        Assert.assertTrue(personalInformationPage.checkIfOriginalDriverLicenseIsPresent(),"Countinu button doesnt work correctly!");
    }
    //Id
    //Test 1.324
    @Test
    public void checkIfUserCantRedirectToNextStepWhenSomeFieldEmpty(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,email,"");
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.presentsOfPersonalInformationPage().contains("PERSONAL INFORMATION"),"System allows user to get to next step!");
    }
    //Test 1.192
    //Id
    @Test
    public void checkIfRedColorIsPresentsOfFirstNameWithInvalidValue() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before writing:" + personalInformationPage.getCssValueOfFirstnameInput());
        personalInformationPage.writeNameToFirstNameField("1");
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After writing invalid:" + personalInformationPage.getCssValueOfFirstnameInput());
        Assert.assertTrue(personalInformationPage.getCssValueOfFirstnameInput().equals("rgb(255, 0, 0)"),"FirstName fields is not marked with red color when it have invalid value!");
    }
    //Test 1.193
    //Id
    @Test
    public void checkIfRedColorWhenFirstNameEmptyAndAllValid() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before writing:" + personalInformationPage.getCssValueOfFirstnameInput());
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage("",lastName,email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After left empty:" + personalInformationPage.getCssValueOfFirstnameInput());
        Assert.assertTrue(personalInformationPage.getCssValueOfFirstnameInput().equals("rgb(255, 0, 0)"),"FirstName fields is not marked with red color when it empty!");
    }
    //Test 1.208
    //Id
    @Test
    public void checkIfLastNameIsMarkedRedWithInvalidValue() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before writing:" + personalInformationPage.getCssValueOfLastNameFieldInput());
        personalInformationPage.writeNameToLastNameField("1");
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After writing invalid:" + personalInformationPage.getCssValueOfLastNameFieldInput());
        Assert.assertTrue(personalInformationPage.getCssValueOfLastNameFieldInput().equals("rgb(255, 0, 0)"),"LastNameField is not marked with red color when it empty!");
    }
    //Test 1.209
    //Id
    @Test
    public void checkIfLastNameFieldIsMarkedWithRedColorWhenItEmptyAndAllValid() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before writing:" + personalInformationPage.getCssValueOfLastNameFieldInput());
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,"",email,telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After writing:" + personalInformationPage.getCssValueOfLastNameFieldInput());
        Assert.assertTrue(personalInformationPage.getCssValueOfLastNameFieldInput().equals("rgb(255, 0, 0)"),"LastNameField is not marked with red color when it empty and all valid!");
    }
    //Test 1.249
    //Id
    @Test
    public void checkIfMaleButtonIsChosenByDefault() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        String activeButtonClass = "ng-pristine ng-untouched ng-valid ng-not-empty";
        System.out.println("Default value of 'Male' button class:" + personalInformationPage.getAttributeValueOfMaleButton());
        Assert.assertTrue(personalInformationPage.getAttributeValueOfMaleButton().equals(activeButtonClass),"Male button is not active by default!");
    }
    //Test 1.251
    //Id
    @Test
    public void checkIfFemaleButtonIsActivatedAfterClickingOnIt(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Color before activating:" + personalInformationPage.getCSSValueOFFemaleButton());
        System.out.println("Value before activating 'Female' button:" + personalInformationPage.getAttributeValueOfFemaleButton());
        personalInformationPage.clickOnFemaleButton();
        String activeClass = "ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse";
        System.out.println("Value after activating 'Female' button:" + personalInformationPage.getAttributeValueOfFemaleButton());
        System.out.println("Color after activating:" + personalInformationPage.getCSSValueOFFemaleButton());
        Assert.assertTrue(personalInformationPage.getAttributeValueOfFemaleButton().equals(activeClass),"FemaleButton is not active after activating!");
    }
    //Test 1.269
    //Id
    @Test
    public void checkIfFtButtonIsActivatedByDefault(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        String activeOfFtButton = "ng-pristine ng-untouched ng-valid ng-not-empty";
        System.out.println("Active of 'Ft' button:" + personalInformationPage.getClassOfFtButton());
        Assert.assertTrue(personalInformationPage.getClassOfFtButton().equals(activeOfFtButton),"Ft button is not active by default!");
    }
    //Test 1.282
    //Id
    @Test
    public void checkIfPhoneIsRedWhenItWithInvalidValue() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before:" + personalInformationPage.getCssValueOfPhoneField());
        personalInformationPage.writePhoneNumber("1");
        String redColorOFPhoneField = "rgb(255, 0, 0)";
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After:" + personalInformationPage.getCssValueOfPhoneField());
        Assert.assertTrue(personalInformationPage.getCssValueOfPhoneField().equals(redColorOFPhoneField), "PhoneField does not marked red color when it invalid!");
    }
    //Test 1.283
    //Id
    @Test
    public void checkIfPhoneIsRedWhenItEmptyAndAllValid() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before:" + personalInformationPage.getCssValueOfPhoneField());
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName, lastName, email, "");
        String redColorOFPhoneField = "rgb(255, 0, 0)";
        personalInformationPage.clickOnCountinueButton();
        System.out.println("After:" + personalInformationPage.getCssValueOfPhoneField());
        Assert.assertTrue(personalInformationPage.getCssValueOfPhoneField().equals(redColorOFPhoneField), "PhoneField does not marked red color when it empty and all valid!");
    }
    //Test 1.315
    //Id
    @Test
    public void checkIfEmailIsRedWhenItWithInvalidValue() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before:" + personalInformationPage.getCssValueOfEmailField());
        personalInformationPage.writeEmail("a");
        personalInformationPage.clickOnCountinueButton();
        String redColorOfEmailField = "rgb(255, 0, 0)";
        System.out.println("After:" + personalInformationPage.getCssValueOfEmailField());
        Assert.assertTrue(personalInformationPage.getCssValueOfEmailField().equals(redColorOfEmailField),"Email is not marked red when it with invalid value!");
    }
    //Test 1.316
    //Id
    @Test
    public void checkIFEmailIsRedWhenItEmptyAndValid() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before:" + personalInformationPage.getCssValueOfEmailField());
        personalInformationBO.writeInfoToAllFieldsOfPersonalInformationPage(firstName,lastName,"",telephoneNumber);
        personalInformationPage.clickOnCountinueButton();
        String redColorOfEmailField = "rgb(255, 0, 0)";
        System.out.println("After:" + personalInformationPage.getCssValueOfEmailField());
        Assert.assertTrue(personalInformationPage.getCssValueOfEmailField().equals(redColorOfEmailField),"Email is not marked red when it with empty and all is valid!");
    }
    //Test 1.325
    //Id
    @Test
    public void checkIfUserCantBeRedirectedToNextStepByUsingItURL(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.goToSecondStepByUsingLink();
        Assert.assertFalse(personalInformationPage.getURLOfPricingScreen().equals(secondStepURL),"User does not redirect to pricing screen, he is redirected to second step!");
    }
    //Test 1.326
    //Id
    @Test
    public void checkIfXButtonRedirectUserToHomeScreen(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.clickOnXButton();
        IDLHomePage homePage = new IDLHomePage();
        Assert.assertTrue(homePage.checkIfHomeScreenIsPresent(),"User does not redirected to HomePage by clicking XButton");
        System.out.println("Given:" + personalInformationPage.getURLOfHomeScreen());
        System.out.println("Then:" + homePageURL);
        Assert.assertTrue(personalInformationPage.getURLOfHomeScreen().equals(homePageURL),"Smth with URL of HomePAge!");
    }
    //Test 1.225,1.231,1.240
    //Id
    @Test
    public void checkIfDateOfBirthMenusIsMarkedWithRedColor() throws InterruptedException {
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        System.out.println("Before Month:" + personalInformationPage.getCssValueOfMonth());
        System.out.println("Before Day:" + personalInformationPage.getCssValueOfDay());
        System.out.println("Before Year:" + personalInformationPage.getCssValueOfYear());
        personalInformationPage.clickOnCountinueButton();
        String redColor = "rgb(255, 0, 0)";
        System.out.println("After Month:" + personalInformationPage.getCssValueOfMonth());
        System.out.println("After Day:" + personalInformationPage.getCssValueOfDay());
        System.out.println("After Year:" + personalInformationPage.getCssValueOfYear());
        Assert.assertTrue(personalInformationPage.getCssValueOfMonth().equals(redColor),"Month is not red when it empty!");
        Assert.assertTrue(personalInformationPage.getCssValueOfDay().equals(redColor), "Day is not red when it empty!");
        Assert.assertTrue(personalInformationPage.getCssValueOfYear().equals(redColor),"Year is not red when it empty!");
    }
    //Test 1.202
    //Id
    @Test
    public void checkIfSystemDoesNotAllowToWriteSpacesOnlyInFirstName(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.writeNameToFirstNameField("    ");
        personalInformationPage.clickOnCountinueButton();
        Assert.assertTrue(personalInformationPage.checkPresentsOfPersonalInformationPage(),"User is redirected to OriginalDriverLicense with spaces only in FirstName field!");
    }
    @Test
    public void checkXSS(){
        personalInformationPage = new PersonalInformationPage();
        PersonalInformationBO personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.makeXXSAttack();
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}

