package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.EditOrderBO;
import com.art.idl.adminpanelpages.AdminEditOrderPage;
import com.art.idl.datetime.*;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.singleton.UserRandomGenerator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 12/20/2017.
 */
public class EditOrderTest {
    private UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
    private RandomDateGenerator randomDateGenerator = new RandomDateGenerator();
    private AdminEditOrderPage adminEditOrderPage = new AdminEditOrderPage();
    private EditOrderBO editOrderBO = new EditOrderBO();

    //Wait until save button will be correct!
    //Id
    //Test 1.680-1.684
    @Test
    public void checkIfUserCanEditFirstNameLastNameAndDateOfBirth(){
        String firstName = "name" + userRandomGenerator.generateRandomUser();
        String lastName = "name" + userRandomGenerator.generateRandomUser();
        String monthOfBirth = randomDateGenerator.generateRandomMonth();
        String dayOfBirth = randomDateGenerator.generateRandomDay();
        String yearOfBirth = randomDateGenerator.generateRandomYear();
        editOrderBO.goToEditFirstOrderPage();
        editOrderBO.writeNewFirstNameLastNameAndDateOfBirthAndSave(firstName,lastName,monthOfBirth,dayOfBirth,yearOfBirth);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminEditOrderPage.getTextOfFirstNameInput(),firstName,"FirstName is not saved after editing!");
        softAssert.assertEquals(adminEditOrderPage.getTextOfLastNameInput(),lastName,"LastName is not saved after editing!");
        softAssert.assertEquals(adminEditOrderPage.getSelectedMonthOfBirth(),monthOfBirth,"SelectedMonth is not saved after editing!");
        softAssert.assertEquals(adminEditOrderPage.getSelectedDayOfBirth(),dayOfBirth,"SelectedDay is not saved after editing!");
        softAssert.assertEquals(adminEditOrderPage.getSelectedYearOfBirth(),yearOfBirth,"SelectedYear is not saved after editing!");
        softAssert.assertAll();
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.685-1.686
    @Test
    public void checkIfUserCanEditMaleToFemaleAndTurnItBackAndEyeColorAndCmToFtAndTurnBack(){
        String colorOfSelectedGender = "rgba(46, 69, 84, 1)";
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clickOnFemaleButton();
        adminEditOrderPage.clickOnSaveEditOrderButton();
        System.out.println("Color:" + adminEditOrderPage.checkIfFemaleIsChosen());
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminEditOrderPage.checkIfFemaleIsChosen(),colorOfSelectedGender,"Female is not saved after selecting!");
        /*Dont know where user is after save button
        so edit it after */
        adminEditOrderPage.clickOnMaleButton();
        adminEditOrderPage.clickOnSaveEditOrderButton();
        softAssert.assertEquals(adminEditOrderPage.checkIfMaleIsChosen(),colorOfSelectedGender,"Male is not saved after selecting!");
        softAssert.assertAll();
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.687
    @Test
    public void checkIfUserCanChangeEyeColor(){
      String randomColor = RandomColor.getRandomColor();
      editOrderBO.goToEditFirstOrderPage();
      adminEditOrderPage.selectEyeColor(randomColor);
      adminEditOrderPage.clickOnSaveEditOrderButton();
      Assert.assertEquals(adminEditOrderPage.checkIfCorrectEyeColorIsSelected(),randomColor,"Color is not saved after editing!");
    }
    //Call saveButton method!
    //Id
    //Test 1.688-1.689
    @Test
    public void checkIfUserCanChangeFromFtToCmAndCmToFt(){
        String heightSelectedColor = "rgba(46, 69, 84, 1)";
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clickOnFtButton();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminEditOrderPage.checkIfFtButtonIsSelected(),heightSelectedColor,"Ft button is not saved after selecting!");
        adminEditOrderPage.clickOnCmButton();
        softAssert.assertEquals(adminEditOrderPage.checkIfCmButtonIsSelected(),heightSelectedColor,"Cm button is not saved after selecting!");
    }
    /*Wait until saveButton will be correct and
    selected attribute will be added*/
    //Id
    //Test 1.690-1.691
    @Test
    public void checkIfUserCanEditFtAndInHeight(){
        String randomFt = RandomHeight.generateRandomFt();
        String randomIn = RandomHeight.generateRandomIn();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clickOnFtButton();
        adminEditOrderPage.selectHeightInFt(randomFt);
        adminEditOrderPage.selectInOfHeight(randomIn);
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminEditOrderPage.checkIfFtIsCorrect(),randomFt,"Ft value is not saved after editing!");
        softAssert.assertEquals(adminEditOrderPage.checkIfInIsCorrect(),randomIn,"In value is not saved after editing!");
        softAssert.assertAll();
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.692
    @Test
    public void checkIfUserCanEditPhoneWithTenNumbers(){
        String randomTelephone = RandomPhone.generateRandomTelephone();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clearPhoneInput();
        adminEditOrderPage.writeTelephoneNumber(randomTelephone);
        Assert.assertEquals(adminEditOrderPage.checkIfTelephoneIsCorrect(),randomTelephone,"Telephone with 10 digits is not saved after editing!");
    }
    //Id
    //Test 1.693
    @Test
    public void checkIfUserCanEditEmail(){
        String randomEmail = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clearEmailInput();
        adminEditOrderPage.writeEmailInput(randomEmail);
        Assert.assertEquals(adminEditOrderPage.checkIfEmailIsCorrect(),randomEmail,"Email is not saved after editing!");
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.705
    @Test
    public void checkIfUserCanChangeCountry(){
        String randomCountry = ShippingAddressRandom.getRandomCountry();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.selectShippingCountry(randomCountry);
        Assert.assertEquals(adminEditOrderPage.checkIfShippingCountryIsCorrect(),randomCountry,"ShippingCountry is not save after editing!");
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.706
    @Test
    public void checkIfUserCanChangeShippingAddress(){
        String randomShippingAddress = userRandomGenerator.generateRandomUser();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clearShippingAddressInput();
        adminEditOrderPage.writeShippingAddressInput(randomShippingAddress);
        Assert.assertEquals(adminEditOrderPage.checkIfShippingAddressIsCorrect(),randomShippingAddress,"ShippingAddress is not saved after editing!");
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.711
    @Test
    public void checkIfUserCanEditShippingCity(){
        String randomCity = userRandomGenerator.generateRandomUser();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clearShippingCity();
        adminEditOrderPage.writeShippingCity(randomCity);
        Assert.assertEquals(adminEditOrderPage.checkIfShippingCityIsCorrect(),randomCity,"ShippingCity is not saved after editing!");
    }
    //Wait until save button will be correct!
    //Id
    //Test 1.716
    @Test
    public void checkIfUserCanChangeShippingState(){
        String randomState = ShippingAddressRandom.getRandomState();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.selectShippingCountry("UA");
        adminEditOrderPage.selectShippingState(randomState);
        Assert.assertEquals(adminEditOrderPage.checkIfShippingStateIsCorrect(),randomState,"ShippingState is not saved after editing!");
    }
    //Id
    //Test 1.717
    @Test
    public void checkIfUserCanEditZCode(){
        String randomZCode = ShippingAddressRandom.getRandomZCode();
        editOrderBO.goToEditFirstOrderPage();
        adminEditOrderPage.clearShippingZCode();
        adminEditOrderPage.writeShippingZCode(randomZCode);
        Assert.assertEquals(adminEditOrderPage.checkIfShippingZCodeIsCorrect(),randomZCode,"ShippingZCode is not saved after editing!");
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
