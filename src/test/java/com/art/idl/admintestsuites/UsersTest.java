package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.AdminClientsBO;
import com.art.idl.adminlpanelbussinessobjects.UsersBO;
import com.art.idl.adminpanelpages.AdminClientsPage;
import com.art.idl.adminpanelpages.AdminPanelLoginPage;
import com.art.idl.adminpanelpages.AdminUsersPage;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.singleton.UserRandomGenerator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 12/7/2017.
 */
public class UsersTest {
    //Id
    //Test + 1
    @Test
    public void checkIfUserIsRedirectedToAddNewUserPageByAddNewUserButton(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"User is not redirected to AddNewUserPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfSystemDoesNotAllowToSaveUserWithAllEmptyFields(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser("","","");
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"User cant be created when all fields are empty!");
    }

    //Id
    //Test + 2
    @Test
    public void checkIfEmptyFieldsAreRedAndErrorMessageIsDisplayed() throws InterruptedException {
        String nameErrorMessage = "This field cannot be left empty";
        String emailErrorMessage = "This field cannot be left empty";
        String passwordErrorMessage = "Need enter password";
        String redColor = "rgb(221, 71, 71)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        UsersBO usersBO = new UsersBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        //Write empty fields and try to save it
        usersBO.writeCredentialsOfNewUser("","","");
        adminUsersPage.clickOnSaveUserButton();
        System.out.println("Name:" + adminUsersPage.getTextOfErrorMessageOfNameField());
        System.out.println("Email:" + adminUsersPage.getTextOfErrorMessageOfEmailField());
        System.out.println("Password:" + adminUsersPage.getTextOfErrorMessageOfPasswordField());
        SoftAssert softAssert = new SoftAssert();
        //Check for valid,color and presents of NameErrorMessage
        softAssert.assertTrue(adminUsersPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        softAssert.assertEquals(adminUsersPage.getTextOfErrorMessageOfNameField(),nameErrorMessage,"NameErrorMessage is invalid!");
        softAssert.assertTrue(adminUsersPage.getColorOfNameField().contains(redColor),"NameField is not red!");
        //Check for valid,color and presents of EmailErrorMessage
        softAssert.assertTrue(adminUsersPage.checkPresentsOfEmailErrorMessage(), "EmailErrorMessage is not displayed!!");
        softAssert.assertEquals(adminUsersPage.getTextOfErrorMessageOfEmailField(), emailErrorMessage, "EmailErrorMessage is invalid!");
        softAssert.assertTrue(adminUsersPage.getColorOfEmailField().contains(redColor), "EmailField is not red!");
        //Check for valid,color and presents of PasswordErrorMessage
        softAssert.assertTrue(adminUsersPage.checkPresentsOfPasswordErrorMessage(),"PasswordErrorMessage is not displayed!");
        softAssert.assertEquals(adminUsersPage.getTextOfErrorMessageOfPasswordField(),passwordErrorMessage,"PasswordErrorMessage is invalid!");
        softAssert.assertTrue(adminUsersPage.getColorOfPasswordField().contains(redColor),"PasswordField is not red!");
        softAssert.assertAll();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidNameAndEmptyOtherFieldsCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfNewUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfNewUser,"","");
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfEmailErrorMessage(),"EmailErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"System allows to save user with empty email and password fields!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidEmailAndEmptyNameAndPasswordsFieldsCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmai.com";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser("",emailOfUser,"");
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"NameErrorMessage is not displayed!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidPasswordAndEmptyNameAndEmailFieldsCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser("","",passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"System allows to save user when name and email fields are empty!");
    }
    //Today

    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidNameAndEmailFieldsAndEmptyPasswordFieldCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,"");
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfPasswordErrorMessage(),"PasswordErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"System allows to save user when password field is empty!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidNameAndPasswordFieldsAndEmptyEmailFieldCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,"",passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfEmailErrorMessage(),"EmailErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"System allows to save user when email field is empty!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserWithValidEmailAndPasswordFieldsAndEmptyNameFieldCantBeSaved(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser("",emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertTrue(adminUsersPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"System allows to save user with empty name field!");
    }
    //Id
    //Test + 3
    @Test
    public void checkIfUserWithAllValidFieldsCanBeSavedAndDisplayedOnUsersPage(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkPresentsOfCreatedUserOnUsersPage(emailOfUser),emailOfUser,"User with all valid fields is not saved!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserTypeSuperAdminIsShownOnUsersPageAfterCreatingUserWithSuperAdminType(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String superAdminChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //SuperAdmin type is by default
        adminUsersPage.clickOnSaveUserButton();
        //Selected user have a special rgb color
        System.out.println("CreatedUser select color:" + adminUsersPage.checkIfCreatedUserIsWithSuperAdminType());
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithSuperAdminType(),superAdminChecked,"Created user with SuperAdmin type is not selected on UsersPage!");
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserTypeManagerIsShownOnUsersPageAftreCreatingUserWithManagerType(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String managerChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        System.out.println("ManagerCreatedUser select color:" +adminUsersPage.checkIfCreatedUserIsWithManagerType());
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithManagerType(),managerChecked,"CreatedManagerUser is not selected on UsersPage!");
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminCanDeleteUserWithAdminType(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //Admin type is by default
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.deleteCreatedUser();
        /*Check that deleted admin is deleted
        and he is not on UsersPage*/
        Assert.assertTrue(adminUsersPage.checkIfDeletedUserIsNotDisplayed(emailOfUser),"Admin cant delete other admin by DeleteButton!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminCanDeleteManager(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //Make user a manager
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.deleteCreatedUser();
        /*Check that deleted manager is deleted
        and he is not on UsersPage*/
        Assert.assertTrue(adminUsersPage.checkIfDeletedUserIsNotDisplayed(emailOfUser),"Admin cant delete user ");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfEditUserPageIsPresentAfterClickingOnEditButton(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnUsersMenuButton();
        adminUsersPage.clickOnEditCreatedUser();
        Assert.assertTrue(adminUsersPage.checkIfAddNewUserPageIsPresent(),"User is not redirected to EditUserPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfNewNameOfUserIsSavedAfterEditing(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        System.out.println("Name of user:" + nameOfUser);
        System.out.println("Before deleting:" + adminUsersPage.getTextOfUserName());
        adminUsersPage.clearNameOfUser();
        System.out.println("After deleting:" + adminUsersPage.getTextOfUserName());
        String newNameOfUser = "user" + userRandomGenerator.generateRandomUser();
        adminUsersPage.writeNameOfNewUser(newNameOfUser);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        System.out.println("Name of new user:" + newNameOfUser);
        System.out.println("After editing:" + adminUsersPage.getTextOfUserName());
        Assert.assertEquals(newNameOfUser,adminUsersPage.getTextOfUserName(),"After editing, name of user is not saved!");
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfNewEmailOfUserIsSavedAfterEditing(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        System.out.println("Before deleting:" + emailOfUser);
        adminUsersPage.clearEmailOfUser();
        String newEmailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        adminUsersPage.writeEmailOfNewUser(newEmailOfUser);
        adminUsersPage.clickOnSaveUserButton();
        System.out.println("After editing:" + adminUsersPage.getTextOfUserEmail());
        Assert.assertEquals(newEmailOfUser,adminUsersPage.getTextOfUserEmail(),"After editing email, new email is not saved!");
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAfterEditingEmailUserCanLogInWithNewEmail(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        adminUsersPage.clearEmailOfUser();
        String newUserEmail = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        adminUsersPage.writeEmailOfNewUser(newUserEmail);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnLogOutButton();
        adminPanelLoginPage.writeLoginOfAdmin(newUserEmail);
        adminPanelLoginPage.writePasswordOfAdmin(passwordOfUser);
        adminPanelLoginPage.clickOnSubmitButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User with new email cant log in!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAfterEditingPasswordUserCanLogInWithNewPassword(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        String newUserPassword = userRandomGenerator.generateRandomUser();
        System.out.println("New user password:" + userRandomGenerator.generateRandomUser());
        adminUsersPage.writePasswordOfNewUser(newUserPassword);
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnLogOutButton();
        adminPanelLoginPage.writeLoginOfAdmin(emailOfUser);
        adminPanelLoginPage.writePasswordOfAdmin(newUserPassword);
        adminPanelLoginPage.clickOnSubmitButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User with new password cant log in!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminCanChangeUserTypeToSuperAdminOnEditUserPage(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String adminChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        adminUsersPage.clickOnAdminButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithSuperAdminType(), adminChecked, "Admin cant change type of user from Manager to Admin!");
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminCanChangeUserTypeToManagerOnEditUserPage(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String managerChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //Admin is by default
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithManagerType(),managerChecked,"Admin cant change type of user from Admin to Manager!");
        adminUsersPage.deleteCreatedUser();
    }
    //Id
    //Test + 1 bug
    @Test
    public void checkIfAdminCanChangeToAdminTypeOnUsersPage() throws InterruptedException {
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String adminChecked = "rgb(121, 121, 121)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        Thread.sleep(2000);
        adminUsersPage.refreshPage();
        adminUsersPage.clickOnAdminCreatedUserButton();
        Thread.sleep(2000);
        adminUsersPage.refreshPage();
        adminUsersPage.clickOnEditCreatedUser();
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithSuperAdminType(),adminChecked, "Admin cant change to admin type on UsersPage by ManagerButton!");
    }
    //Id
    //Test + 1 bug
    @Test
    public void checkIfAdminCanChangeToManagerOnUsersPage() throws InterruptedException {
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String managerChecked = "rgb(121, 121, 121)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnSaveUserButton();
        Thread.sleep(2000);
        adminUsersPage.refreshPage();
        adminUsersPage.clickOnManagerCreatedUserButton();
        Thread.sleep(2000);
        adminUsersPage.refreshPage();
        adminUsersPage.clickOnEditCreatedUser();
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithManagerType(),managerChecked,"Admin cant change to admin type on UsersPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminTypeIsDisplayedOnUsersScreenAfterChoosingItOnEditUserPage(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String adminChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //Admin is by default
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithSuperAdminType(),adminChecked,"CreatedUser is not with AdminType!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfManagerTypeIsDisplayedOnUsersScreenAfterChoosingItOnEditUserPage(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String managerChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        adminUsersPage.clickOnManagerButtonOnAddUserPage();
        adminUsersPage.clickOnSaveUserButton();
        Assert.assertEquals(adminUsersPage.checkIfCreatedUserIsWithManagerType(),managerChecked,"CreatedUser is not with ManagerType");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfAdminTypeOnEdiUserPageIsSelectedAfterCreatingAdminUser(){
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String nameOfUser = "user" + userRandomGenerator.generateRandomUser();
        String emailOfUser = "m" + userRandomGenerator.generateRandomUser() + "@gmail.com";
        String passwordOfUser = userRandomGenerator.generateRandomUser();
        String adminChecked = "rgb(22, 160, 133)";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminUsersPage adminUsersPage = new AdminUsersPage();
        AdminPanelLoginPage adminPanelLoginPage = new AdminPanelLoginPage();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UsersBO usersBO = new UsersBO();
        adminClientsBO.goToClientsPage();
        usersBO.createNewUser();
        usersBO.writeCredentialsOfNewUser(nameOfUser,emailOfUser,passwordOfUser);
        //Admin is by default
        adminUsersPage.clickOnSaveUserButton();
        adminUsersPage.clickOnEditCreatedUser();
        Assert.assertEquals(adminUsersPage.checkIfAdminCheckboxOnEditUserPageIsSelected(),adminChecked,"Admin typ is not selected on EditUsersPage after choosing Admin type!");
    }

    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
