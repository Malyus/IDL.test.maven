package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.AdminClientsBO;
import com.art.idl.adminlpanelbussinessobjects.AdminEmailsBO;
import com.art.idl.adminpanelpages.AdminEmailsPage;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.singleton.UserRandomGenerator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 12/11/2017.
 */
public class EmailsTest {
    UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
    String subjectOfEmail = "TestSubject";
    String contentOfEmail = "TestContent";

    //Id
    //Test + 1
    @Test
    public void checkIfUserIsRedirectedToAddNewEmailsPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfEmailsPage(),"User is not on EmailsPage!");
        adminEmailsPage.clickOnAddNewMessage();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfAddNewEmailPage(),"User is not on AddNewEmailPage");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCanDeleteMessage(){
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        adminClientsBO.goToClientsPage();
        adminEmailsBO.createNewEmail(nameOfEmail,subjectOfEmail,contentOfEmail);
        adminEmailsPage.deleteCreatedEmail();
        Assert.assertTrue(adminEmailsPage.checkIfDeletedEmailIsDeleted()!=nameOfEmail,"Email is not deleted!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserIsRedirectedToEditEmailPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsPage.clickOnEditEmailButton();
        Assert.assertEquals(adminEmailsPage.checkPresentsOfEditEmailPage(),"Edit","User is not redirected to EditEmailPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveEmailWithEmptyNameFieldAndCorrectSubjectAndDescriptionFields(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail("",subjectOfEmail,contentOfEmail);
        /*We write twice for checking of AddNewEmailPage
        this is give us 100 percents of true, because if email will save
        driver will not found an element and will fail*/
        adminEmailsPage.writeNameOfEmail("Test");
        Assert.assertTrue(adminEmailsPage.checkPresentsOfAddNewEmailPage(),"User can save email with empty name when subject and content are correct!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveEmailWithEmptySubjectFieldAnCorrectNameAndDescriptionFields(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminEmailsBO.createNewEmail(nameOfEmail,"",contentOfEmail);
        /*We write twice for checking of AddNewEmailPage
        this is give us 100 percents of true, because if email will save
        driver will not found an element and will fail*/
        adminEmailsPage.writeSubjectOfEmail("TestSubject");
        Assert.assertTrue(adminEmailsPage.checkPresentsOfAddNewEmailPage(),"User can save email with empty subject when name and description are correct!");
        adminEmailsPage.clickOnCancelButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfCreatedEmail()!= nameOfEmail,"User can save email with empty subject when name and description are correct!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveEmailWithEmptyDescriptionAndNameFieldsWhenSubjectFieldIsCorrect(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail("",subjectOfEmail,"");
        /*We write twice for checking of AddNewEmailPage
        this is give us 100 percents of true, because if email will save
        driver will not found an element and will fail*/
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminEmailsPage.writeNameOfEmail(nameOfEmail);
        Assert.assertTrue(adminEmailsPage.checkPresentsOfAddNewEmailPage(),"User can save email with empty subject when name and description are correct!");
        adminEmailsPage.clickOnCancelButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfCreatedEmail()!= nameOfEmail,"User can save email with empty subject when name and description are correct!");

    }
    //Id
    //Test + 1
    @Test
    public void checkIfCreatedEmailIsDisplayedOnEmialsPage() {
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        Assert.assertTrue(adminEmailsPage.checkPresentsOfCreatedEmail().contains(nameOfEmail), "Created email is not displayed!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCanSaveChangesInSubjectFieldOnEditEmailPage(){
        String newSubjectOfEmail = "New Subject";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        System.out.println("Before editing:" + adminEmailsPage.getValueOfSubjectInput());
        adminEmailsPage.clearSubjectInput();
        adminEmailsPage.writeSubjectOfEmail(newSubjectOfEmail);
        adminEmailsPage.clickOnSaveEmailButton();
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        System.out.println("After editing:" + adminEmailsPage.getValueOfSubjectInput());
        Assert.assertEquals(adminEmailsPage.getValueOfSubjectInput(),newSubjectOfEmail,"User cant save a new subject of email after editing!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCanSaveChangesInNameFieldOnEditEmailPage(){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        System.out.println("Before editing:" + adminEmailsPage.getValueOfNameInput());
        adminEmailsPage.clearNameInput();
        String newNameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        System.out.println("New namef:" + newNameOfEmail);
        adminEmailsPage.writeNameOfEmail(newNameOfEmail);
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfCreatedEmail().contains(newNameOfEmail),"User cant save a new name after editing!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCanSaveChangesInDectiptionOnEditEmailsPage(){
        String newContentOfEmail = "New Content";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        System.out.println("Before:" + contentOfEmail);
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.clearDescriptionInput();
        adminEmailsPage.switchOffFromIFrame();
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.writeContentOfEmail(newContentOfEmail);
        adminEmailsPage.clickOnSaveEmailButton();
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        Assert.assertTrue(adminEmailsPage.getValueOfDescription().contains(newContentOfEmail),"User cant save a new description after editing!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveSubjectFieldEmptyOnEditEmailsPage(){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearSubjectInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfSubjectErrorMessage(),"User cant save empty subject field on EditEmailsPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveNameFieldEmptyOnEditEmailsPage(){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearNameInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfNameErrorMessage(),"User cant save empty name filed on EditEmailsPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfUserCantSaveEmptyDescriptionFieldOnEditEmailsPage(){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        /*Dont forget to switch to iframe and
        swtich off after working*/
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.clearDescriptionInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfDescriptionErrorMessage(),"User cant save empty email field on EditEmailsPage!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfNewEmailAfterWritingAllFieldsAndClickOnContinueButtonWillNotBeSaved(){
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsPage.clickOnAddNewMessage();
        adminEmailsPage.writeNameOfEmail(nameOfEmail);
        adminEmailsPage.writeSubjectOfEmail(contentOfEmail);
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.writeContentOfEmail(contentOfEmail);
        adminEmailsPage.switchOffFromIFrame();
        adminEmailsPage.clickOnCancelButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfCreatedEmail()!=nameOfEmail,"Email is saved after clicking on cancel button!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfChangesInSubjectFieldOnEditEmailPageIsNotSavedAfterClickingOnCancelButton(){
        String newSubjectOfEmail = "This is new subject!";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearSubjectInput();
        adminEmailsPage.writeSubjectOfEmail(newSubjectOfEmail);
        adminEmailsPage.clickOnCancelButton();
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        Assert.assertTrue(adminEmailsPage.getValueOfSubjectInput()!=newSubjectOfEmail,"New subject is saved after editing and clicking on CancelButton!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfChangesInNameFieldOnEditEmailPageIsNotSavedAfterClickingOnCancelButton(){
        String newSubjectOfEmail = "This is new subject!";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearNameInput();
        String newNameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminEmailsPage.writeNameOfEmail(newNameOfEmail);
        adminEmailsPage.clickOnCancelButton();
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        Assert.assertTrue(adminEmailsPage.getValueOfNameInput()!=newNameOfEmail,"New name is saved after editing and clicking on CancelButton!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfChangesInDescriptionFieldOnEmailsPageIsNotSavedAfterClickingOnCancelButton(){
        String newDescriptionOfEmail = "New Description!";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail, subjectOfEmail, contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.clearDescriptionInput();
        adminEmailsPage.writeContentOfEmail(newDescriptionOfEmail);
        adminEmailsPage.switchOffFromIFrame();
        adminEmailsPage.clickOnCancelButton();
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        Assert.assertTrue(adminEmailsPage.getValueOfDescription()!=newDescriptionOfEmail,"New description is saved after editing and clicking on CancelButton!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfErrorMessageIsDisplayedWhenUserLeavesEmptyNameField(){
        String nameErrorMessageText = "This field cannot be left empty";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail,subjectOfEmail,contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearNameInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        Assert.assertEquals(adminEmailsPage.getTextOfNameErrorMessage(),nameErrorMessageText,"NameErrorMessage is not valid!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfErrorMessageIsDisplayedWhenUserLeavesEmptySubjectField(){
        String subjectErrorMessageText = "This field cannot be left empty";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail,subjectOfEmail,contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.clearSubjectInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfSubjectErrorMessage(),"SubjectErrorMessage is not displayed!");
        Assert.assertEquals(adminEmailsPage.getTextOfSubjectErrorMessage(),subjectErrorMessageText,"SubjectErrorMessage is not valid!");
    }
    //Id
    //Test + 1
    @Test
    public void checkIfErrorMessageIsDisplayedWhenUserLeaveEmptyDescriptionField(){
        String descriptionErrorMessage = "This field cannot be left empty";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsBO.createNewEmail(nameOfEmail,subjectOfEmail,contentOfEmail);
        adminEmailsPage.clickOnEditButtonOfCreatedEmail();
        adminEmailsPage.switchToEmailIframe();
        adminEmailsPage.clearDescriptionInput();
        adminEmailsPage.clickOnSaveEmailButton();
        Assert.assertTrue(adminEmailsPage.checkPresentsOfDescriptionErrorMessage(),"DescriptionErrorMessage is not displayed!");
        Assert.assertEquals(adminEmailsPage.getTextOfDescriptionErrorMessage(),descriptionErrorMessage,"DescriptionErrorMessage is not valid!");
    }
    //Id
    //Test + 3
    @Test
    public void checkIfErrorMessageIsDisplayedWhenAllFieldsAreEmptyOnAddNewUserPage(){
        String errorMessageText = "This field cannot be left empty";
        AdminEmailsPage adminEmailsPage = new AdminEmailsPage();
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminEmailsBO adminEmailsBO = new AdminEmailsBO();
        String nameOfEmail = "Test:" + userRandomGenerator.generateRandomUser();
        adminClientsBO.goToClientsPage();
        adminEmailsPage.clickOnEmailsMenuButton();
        adminEmailsPage.clickOnAddNewMessage();
        adminEmailsPage.clickOnSaveEmailButton();
        SoftAssert softAssert = new SoftAssert();
        //First, check that error messages is presents
        softAssert.assertTrue(adminEmailsPage.checkPresentsOfNameErrorMessage(),"NameErrorMessage is not displayed!");
        softAssert.assertTrue(adminEmailsPage.checkPresentsOfSubjectErrorMessage(),"SubjectErrorMessage is not displayed!");
        softAssert.assertTrue(adminEmailsPage.checkPresentsOfDescriptionErrorMessage(),"DescriptionErrorMessage is not displayed!");
        //Second, check error messages for valid
        softAssert.assertEquals(adminEmailsPage.getTextOfNameErrorMessage(),errorMessageText,"NameErrorMessageText is not valid!");
        softAssert.assertEquals(adminEmailsPage.getTextOfSubjectErrorMessage(),errorMessageText,"SubjectErrorMessageText is not valid!");
        softAssert.assertEquals(adminEmailsPage.getTextOfDescriptionErrorMessage(),errorMessageText,"DescriptionErrorMessageText is not valid!");
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
