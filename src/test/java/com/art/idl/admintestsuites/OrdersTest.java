package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.AdminPanelLoginBO;
import com.art.idl.adminlpanelbussinessobjects.OrdersBO;
import com.art.idl.adminpanelpages.AdminOrdersPage;
import com.art.idl.adminpanelpages.AdminPanelLoginPage;
import com.art.idl.singleton.SingletonDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/29/2017.
 */
public class OrdersTest {
    private String daysValue = "03/09/2018";
    private String newDaysValue = "11/11/2017";
    private int countOfItemInList = 5;
    private String valueOfDefaultItemInItemList = "20";//This is String because method return us String for comparing
    private String clearedCalendarValues = "";
    //Id
    //Test
    @Test
    public void checkIfStartAndEndDateCanBeTheSame(){
        AdminPanelLoginBO adminPanelLoginBO = new AdminPanelLoginBO();
        AdminOrdersPage adminOrdersPage = new AdminOrdersPage();
        OrdersBO ordersBO = new OrdersBO();
        adminPanelLoginBO.loginToAdminPanel();
        Assert.assertTrue(adminOrdersPage.checkPresentsOfOrdersPage());
        ordersBO.chooseInCalendarsTheSameStartAndEndDates();
        System.out.println("Value of start day:" + adminOrdersPage.getStartDayValue());
        System.out.println("Value of end day:" + adminOrdersPage.getEndDayValue());
        Assert.assertEquals(adminOrdersPage.getStartDayValue(), daysValue,"StartDayValue is not valid!");
        Assert.assertEquals(adminOrdersPage.getEndDayValue(),daysValue,"EndDayValue is not valid!");
        adminOrdersPage.clearCalendarValues();
        adminOrdersPage.waitUntilCalendarIsCleared();
        System.out.println("Value of start day after clearing:" + adminOrdersPage.getStartDayValue());
        System.out.println("Value of end day after clearing:" + adminOrdersPage.getEndDayValue());
        Assert.assertEquals(adminOrdersPage.getStartDayValue(),clearedCalendarValues,"StarDay is not empty after clearing!");
        Assert.assertEquals(adminOrdersPage.getEndDayValue(),clearedCalendarValues,"EndDay is not empty after clearing!");
        //Select a new date after clearing and than once a new
        ordersBO.chooseNewStartAndEndDates();
    }
    //Id
    //Test 1.258-1.259,1.266-1.270
    @Test
    public void checkItemListForValid(){
     AdminPanelLoginBO adminPanelLoginBO = new AdminPanelLoginBO();
     AdminOrdersPage adminOrdersPage = new AdminOrdersPage();
     OrdersBO ordersBO = new OrdersBO();
     adminPanelLoginBO.loginToAdminPanel();
     adminOrdersPage.clickOnOrdersMenuButton();

     Assert.assertEquals(adminOrdersPage.checkIfItemInListByDefaultIs20(),valueOfDefaultItemInItemList, "Default item is not 20!");
     Assert.assertEquals(adminOrdersPage.checkCountOfItemsList(), countOfItemInList,  "Items in list is not 5!");
     Assert.assertTrue(adminOrdersPage.checkIfTwentyIsInItemList(),"Twenty is not in item list!");
     Assert.assertTrue(adminOrdersPage.checkIfFiftyIsInItemList(),"Fifty is not in item list!");
     Assert.assertTrue(adminOrdersPage.checkIfSeventyFiveIsInItemList(),"SeventyFive is not in item list!");
     Assert.assertTrue(adminOrdersPage.checkIfOneHundredIsInItemList(),"OneHundred is not in item list!");
     Assert.assertTrue(adminOrdersPage.checkIfTwoHundredIsInItemList(),"TwoHundred is not in item list!");

     adminOrdersPage.chooseFiftyInItemListSelect();
     Assert.assertTrue(adminOrdersPage.checkIfFiftyValueIsChosen(),"FiftyValue is not selected in item list!");

     adminOrdersPage.chooseTwentyInItemListSelect();
     Assert.assertTrue(adminOrdersPage.checkIfTwentyValueIsChosen(),"TwentyValue is not selected in item list!");

     adminOrdersPage.chooseSeventyFiveItemListSelect();
     Assert.assertTrue(adminOrdersPage.checkIfSeventyFiveValueIsChosen(), "SeventyFive is not selected in item list!");

     adminOrdersPage.chooseOneHundredItemListSelect();
     Assert.assertTrue(adminOrdersPage.checkIfOneHundredValueIsChosen(), "OneHundred is not selected in item list!");

     adminOrdersPage.chooseTwoHundredItemLisSelect();
     Assert.assertTrue(adminOrdersPage.checkIfTwoHundredValueIsChosen(), "TwoHundred value is not selected in item list!");
    }

    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
