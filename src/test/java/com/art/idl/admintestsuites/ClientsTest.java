package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.AdminClientsBO;
import com.art.idl.adminpanelpages.AdminClientsPage;
import com.art.idl.businessobjects.PrintDetailsBO;
import com.art.idl.singleton.SingletonDriver;
import com.art.idl.singleton.UserRandomGenerator;
import com.art.idl.steps.PrintDetailsPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by M.Malyus on 12/4/2017.
 */
public class ClientsTest {
    private String defaultValueOfItemList = "20";
    private String secondValueOfItemList = "50";
    private String thirdValueOfItemList = "75";
    private String fourthValueOfItemList = "100";
    private String fifthValueOfItemList = "200";

    //Id
    //Test 1.882,1.883,1.890-1.894
    @Test
    public void checkItemListForValid() {
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        System.out.println("Default value of item list:" + adminClientsPage.getDefaultValueOfItemList());
        /*This assert check that 20 option in item list is by default selected
        and also that he is present*/
        Assert.assertEquals(adminClientsPage.getDefaultValueOfItemList(), defaultValueOfItemList, "Default value of ItemList is not 20!");

        Assert.assertEquals(adminClientsPage.getValueOfSecondOptionOfItemList(), secondValueOfItemList, "Second value of item list is not 50!");
        Assert.assertEquals(adminClientsPage.getValueOfThirdOptionOfItemList(), thirdValueOfItemList, "Third value of item list is not 75!");
        Assert.assertEquals(adminClientsPage.getValueOfFourthOptionOfItemList(), fourthValueOfItemList, "Fourth value of item list is not 100!");
        Assert.assertEquals(adminClientsPage.getValueOfFifthOptionOfItemList(), fifthValueOfItemList, "Fifth value of item list is not 200!");

        //This asserts for checking if selected option valid displayed in ItemList
        adminClientsPage.selectSomeOptionOfItemList(1);
        Assert.assertEquals(adminClientsPage.checkIfFiftyOptionIsSelected(), secondValueOfItemList, "Fifty is not selected in item list!");
        adminClientsPage.selectSomeOptionOfItemList(2);
        Assert.assertEquals(adminClientsPage.checkIfSeventyFiveOptionIsSelected(), thirdValueOfItemList, "SeventyFive is not selected in item list!");
        adminClientsPage.selectSomeOptionOfItemList(3);
        Assert.assertEquals(adminClientsPage.checkIfOneHundredOptionIsSelected(), fourthValueOfItemList, "OneHundred is not selected in item list!");
        adminClientsPage.selectSomeOptionOfItemList(4);
        Assert.assertEquals(adminClientsPage.checkIfTwoHundredOptionIsSelected(), fifthValueOfItemList, "TwoHundred is not selected in item list!");
        adminClientsPage.selectSomeOptionOfItemList(0);
        Assert.assertEquals(adminClientsPage.checkIfTwentyOptionIsSelected(), defaultValueOfItemList, "Twenty is not selected in item list!");
    }
    //Id
    //Test(near 1.890)
    @Test
    public void checkIfItemsInListCanBeScrolledUpAndDown(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.scrollItemsInListUpAndDown();
    }

    //Comment some asserts because need DB for creating 200 clients every time
    //Id
    //Test 1.895-1.900
    @Test
    public void checkIfSelectAllWorkValidWithItemInListSelecMenu() {
        String selectedTwenty = "20";
        String selectedFifty = "50";
        String selectedSeventyFive = "75";
        String selectedOneHundred = "100";
        String selectedTwoHundred = "200";

        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        /*We choose 5 different options in items in list select menu, then click on selectAll button
         and check if it select valid count of clients*/
        adminClientsPage.clickOnSelectAllButton();
        Assert.assertEquals(adminClientsPage.getValueOfSelectedItemsAfterSelectAll(), selectedTwenty, "SelectAll does not select 20 clients!");
        adminClientsPage.selectSomeOptionOfItemList(1);
        adminClientsPage.clickOnSelectAllButton();
        Assert.assertEquals(adminClientsPage.getValueOfSelectedItemsAfterSelectAll(), selectedFifty, "SelectAll does not select 50 clients!");
        //Need  200 clients
//        adminClientsPage.selectSomeOptionOfItemList(2);
//        adminClientsPage.clickOnSelectAllButton();
//        Assert.assertEquals(adminClientsPage.getValueOfSelectedItemsAfterSelectAll(),selectedSeventyFive, "SelectAll does not select 100 clients!");
//        adminClientsPage.selectSomeOptionOfItemList(3);
//        adminClientsPage.clickOnSelectAllButton();
//        Assert.assertEquals(adminClientsPage.getValueOfSelectedItemsAfterSelectAll(), selectedOneHundred,"SelectAll does not select 100 clients!");
//        adminClientsPage.selectSomeOptionOfItemList(4);
//        adminClientsPage.clickOnSelectAllButton();
//        Assert.assertEquals(adminClientsPage.getValueOfSelectedItemsAfterSelectAll(), selectedTwoHundred, "SelectAll does not select 200 clients!" );
    }

    //Id
    //Test 1.907-1.909
    @Test
    public void checkSelectButtonForValidCountOfSelectedClients() {
        String oneSelected = "1";
        String twoSelected = "2";
        String tenSelected = "10";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.selectClientsByCheckBox(1);
        Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(), oneSelected, "One is not selected!");
        //Select + 1
        adminClientsPage.selectClientsByCheckBox(1);
        Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(), twoSelected, "Two is not selected");
        //Select + 8
        //adminClientsPage.selectClientsByCheckBox(8);
        //Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(), tenSelected, "Ten is not selected!");
    }

    //Id
    //Test 1.910
    @Test
    public void checkIfAfterSelecting3ClientsAndDeleteThemSelectedIsNotDisplayed() throws InterruptedException {
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        String styleOfDeleteButtonAfterDeleting = "display: none;";
        adminClientsPage.selectClientsByCheckBox(1);
        adminClientsPage.clickOnDeleteAllSelectedButton();
        adminClientsPage.clickOnYesButtonOfSelectedAllClients();
        System.out.println("Style:" + adminClientsPage.getStyleOfDeleteButton());
        Assert.assertEquals(adminClientsPage.getStyleOfDeleteButton(), styleOfDeleteButtonAfterDeleting, "DeleteButton is displayed after deleting!");
    }

    //Bug!
    //Id
    //Test 1.911
    @Test
    public void checkSelectAndDeleteButtonForValid() {
        String countOfSelectedAfterDeletingOne = "2";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.selectClientsByCheckBox(3);
        adminClientsPage.deleteFirstSelectedItem();
        adminClientsPage.clickOnYesDeleteClientButton();
        Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(), countOfSelectedAfterDeletingOne, "CountOfSelected after deleting 1 from 3 is not 2!");
    }

    //Id
    //Test 1.914
    @Test
    public void checkIfSelectedIsFiveAfterSelectingFiveOddsNumberOfClients() {
        String countOfSelectedItems = "5";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        /*This method choose in list only paired number of clients
         because for odd number of clients is another x-path*/
        adminClientsPage.selectClientsByCheckBox(5);
        System.out.println("Count of selected clients:" + adminClientsPage.getCountOfSelectedItems());
        Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(), countOfSelectedItems, "Count of selected paired clients is not 5!");
    }
    //Id
    //Test 1.915
    @Test
    public void checkIfAfterSelectingFivePairedClientsSelectedNumberIsValid(){
        String countOfSelectedClients = "5";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        //This method select first five odd clients
        adminClientsPage.selectFirstFivePairedClients();
        System.out.println("Count of selected clients:" + adminClientsPage.getCountOfSelectedItems());
        Assert.assertEquals(adminClientsPage.getCountOfSelectedItems(),countOfSelectedClients,"Count after selecting paired clients id not 5!");
    }
    //Id
    //Test 1.944
    @Test
    public void checkSortAZListForValid(){
     String azSortValue = "Name A-Z";
     String zaSortValue = "Name Z-A";
     AdminClientsBO adminClientsBO = new AdminClientsBO();
     AdminClientsPage adminClientsPage = new AdminClientsPage();
     adminClientsBO.goToClientsPage();
     System.out.println("AZ sort:" + adminClientsPage.getValueOfAZSort());
     System.out.println("ZA sort:" + adminClientsPage.getValueOfZASort());
     Assert.assertEquals(adminClientsPage.getValueOfAZSort(),azSortValue,"There is not A-Z sort option!");
     Assert.assertEquals(adminClientsPage.getValueOfZASort(), zaSortValue,"There is not Z-A sort option!");
    }
    //Id
    //Test (have not number, near 1.947)
    @Test
    public void checkIfAZCanBeScrolled(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.scrollUpDownInAZSelectMenu();
    }
    //Id
    //Test 1.948-1.949
    @Test
    public void checkIfOptionInSortByMenuCanBeSelected(){
        String azSortValue = "Name A-Z";
        String zaSortValue = "Name Z-A";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        //Select Z-A option, because A-Z is selected by default
        adminClientsPage.selectOptionInSortMenu(1);
        //Check that Z-A option is selected
        Assert.assertEquals(adminClientsPage.getNameOfSelectedOption(),zaSortValue, "Z-A option is not selected!");
        //Select A-Z option
        adminClientsPage.selectOptionInSortMenu(0);
        //Check that A-Z option is selected
        Assert.assertEquals(adminClientsPage.getNameOfSelectedOption(),azSortValue,"A-Z option is not selected!");
    }
    //Id
    //Test 1.962
    @Test
    public void checkFinishedOrderAndNotFinishedOrdersFilterButtonsCantBeSelectedInTheSameTime(){
        String activeElement = "filter-item filter-active";
        String notActiveElement = "filter-item";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnFinishedOrdersButton();
        //Check that finished orders button is activated
        Assert.assertEquals(adminClientsPage.checkIfFinishedOrdersButtonIsActive(), activeElement, "FinishedOrders filter is not activated!");
        adminClientsPage.clickOnNotFinishedOrdersButton();
        //Check that not finished orders button is activated
        Assert.assertEquals(adminClientsPage.checkIfNotFinishedOrdersButtonIsActive(), activeElement,"NotFinishedOrders filter is not activated!");
        //Check tha finished orders button is not activated
        Assert.assertEquals(adminClientsPage.checkIfFinishedOrdersButtonIsNotActive(), notActiveElement,"FinishedOrders filter is active after choosing NotFinishedOrders filter!");
    }
    //Bug!
    //Id
    //Test 1.963-1.968
    @Test
    public void checkIfAllButtonAndFinishedOrdersButtonAndAllButtonAndNotFinshedButtonCantBeSelectedInTheSameTime(){
        SoftAssert softAssert = new SoftAssert();
        String activeElement = "filter-item filter-active";
        String notActiveElement = "filter-item";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        //All filter button is active by default, check it
        Assert.assertEquals(adminClientsPage.checkActiveOfAllFilterButton(),activeElement, "All filter button is not active by default!");
        adminClientsPage.clickOnFinishedOrdersButton();
        //Check if FinishedOrders filter button is active
        Assert.assertEquals(adminClientsPage.checkIfFinishedOrdersButtonIsActive(), activeElement, "FinishedOrders filter button is not activated!");
        //Check if All filter button becomes not active
        softAssert.assertEquals(adminClientsPage.checkActiveOfAllFilterButton(),notActiveElement, "All filter button is activated after activating FinishedOrders button!");
        //Activate All filter button
        adminClientsPage.clickOnAllFilterButton();
        //Activate NotFinishedOrdersButton
        adminClientsPage.clickOnNotFinishedOrdersButton();
        //Check if NotFinishedButton is activated
        softAssert.assertEquals(adminClientsPage.checkIfNotFinishedOrdersButtonIsActive(),activeElement,"NotFinishedButton is not activated!");
        //Check if All filter button is not activated
        softAssert.assertEquals(adminClientsPage.checkActiveOfAllFilterButton(), notActiveElement,"All button is activated after activating NotFinishedOrders filter button!");
        softAssert.assertAll();
    }
    //Id
    //Test 1.965
    @Test
    public void checkIfFinishedButtonBecomeNotSelectedAfterActivatingAllButton(){
        String notActiveElement = "filter-item";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnFinishedOrdersButton();
        adminClientsPage.clickOnAllFilterButton();
        Assert.assertEquals(adminClientsPage.checkIfFinishedOrdersButtonIsNotActive(),notActiveElement,"FinishedButton is activated after activating All filter button!");
    }
    //Id
    //Test 1.966
    @Test
    public void checkIfNotFinisishedButtonBecomesNotSelectedAfterActivatingAllButton(){
        String notActiveElement = "filter-item";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnNotFinishedOrdersButton();
        adminClientsPage.clickOnAllFilterButton();
        Assert.assertEquals(adminClientsPage.checkIfFinishedOrdersButtonIsNotActive(),notActiveElement,"NotFinishedButton is activated after activating All filter button!");
    }
    //Id
    //Test 1.969
    @Test
    public void checkIfClientsCanBeDeletedByDeleteButton() throws InterruptedException {
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        String firstClientUniqueId = adminClientsPage.getUniqueIdOfFirstClient();
        System.out.println("Before deleting:" + firstClientUniqueId);
        adminClientsPage.selectFirstClient();
        adminClientsPage.clickOnDeleteAllSelectedButton();
        adminClientsPage.clickOnYesButtonOfSelectedAllClients();
        //This is need because deleting is very slowly
        Thread.sleep(1000);
        System.out.println("After deleting:" + adminClientsPage.getUniqueIdOfFirstClient());
        //Check if user with deleted id is not present
        Assert.assertTrue(firstClientUniqueId != adminClientsPage.getUniqueIdOfFirstClient(),"User is not deleted by DeleteButton!");
    }
    //Id
    //Test 1.970
    @Test
    public void checkIfClientCanBeDeletedByClientDeleteButton() throws InterruptedException {
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        String firstClientUniqueId = adminClientsPage.getUniqueIdOfFirstClient();
        System.out.println("Before deleting:" + firstClientUniqueId);
        adminClientsPage.deleteFirstSelectedItem();
        adminClientsPage.clickOnYesDeleteClientButton();
        //This is need because deleting is very slowly
        Thread.sleep(1000);
        System.out.println("After deleting:" + adminClientsPage.getUniqueIdOfFirstClient());
        //Check if user with deleted id is not present
        Assert.assertTrue(firstClientUniqueId != adminClientsPage.getUniqueIdOfFirstClient(),"User is not deleted by DeleteClient button!");
    }
    //Id
    //Test 1.971
    @Test
    public void checkIfTwoSelectedClientsCanBeDeletedByDeleteButton() throws InterruptedException {
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        String firstClientUniqueId = adminClientsPage.getUniqueIdOfFirstClient();
        String secondClientUniqueId = adminClientsPage.getUniqueIdOfSecondClient();
        System.out.println("Before deleting first user:" + firstClientUniqueId);
        System.out.println("Before deleting second user:" + secondClientUniqueId);
        adminClientsPage.selectFirstClient();
        adminClientsPage.selectSecondClientCheckbox();
        adminClientsPage.clickOnDeleteAllSelectedButton();
        adminClientsPage.clickOnYesButtonOfSelectedAllClients();
        Thread.sleep(1000);
        System.out.println("After deleting first user:" + adminClientsPage.getUniqueIdOfFirstClient());
        System.out.println("After deleting second user:" + adminClientsPage.getUniqueIdOfSecondClient());
        Assert.assertTrue(firstClientUniqueId != adminClientsPage.getUniqueIdOfFirstClient(),"FirstClient is not deleted by DeleteButton!");
        Assert.assertTrue(secondClientUniqueId != adminClientsPage.getUniqueIdOfSecondClient(),"SecondClient is not deleted by DeleteButton!");
    }
    //Id
    //Test 1.985-1.989
    @Test
    public void checkIfSelectedOptionIsShowingAfterRedirectingToAnotherPageAndForwardBack(){
        String selectedTwenty = "20";
        String selectedFifty = "50";
        String selectedSeventyFive = "75";
        String selectedOneHundred = "100";
        String selectedTwoHundred = "200";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        //Select 50
        adminClientsPage.selectSomeOptionOfItemList(1);
        //Go to OrdersPage
        adminClientsPage.clickOnOrdersPageMenuButton();
        adminClientsPage.clickOnClientsButton();
        Assert.assertEquals(adminClientsPage.checkIfFiftyOptionIsSelected(),selectedFifty,"Fifty is not selected after redirecting!");
        //Select 20
        adminClientsPage.selectSomeOptionOfItemList(0);
        adminClientsPage.clickOnOrdersPageMenuButton();
        adminClientsPage.clickOnClientsButton();
        Assert.assertEquals(adminClientsPage.checkIfTwentyOptionIsSelected(), selectedTwenty,"Twenty is not selected after redirecting!");
        //Select 75
        adminClientsPage.selectSomeOptionOfItemList(2);
        adminClientsPage.clickOnOrdersPageMenuButton();
        adminClientsPage.clickOnClientsButton();
        Assert.assertEquals(adminClientsPage.checkIfSeventyFiveOptionIsSelected(), selectedSeventyFive,"SeventyFive is not selected after redirecting!");
        //Select 100
        adminClientsPage.selectSomeOptionOfItemList(3);
        adminClientsPage.clickOnOrdersPageMenuButton();
        adminClientsPage.clickOnClientsButton();
        Assert.assertEquals(adminClientsPage.checkIfOneHundredOptionIsSelected(),selectedOneHundred,"OneHundred is not selected after redirecting!");
        //Select 200
        adminClientsPage.selectSomeOptionOfItemList(4);
        adminClientsPage.clickOnOrdersPageMenuButton();
        adminClientsPage.clickOnClientsButton();
        Assert.assertEquals(adminClientsPage.checkIfTwoHundredOptionIsSelected(),selectedTwoHundred,"TwoHundred is not selected after redirecting!");

    }
    //Id
    //Test (after 1.989 - two test-cases)
    @Test
    public void checkIfSecondPageIsOpenedAfterClickingNextButtonAndTurnBackByPreviousButton(){
        String secondPage = "active";
        String textOfFirstPage = "Page 1";
        String textOfSecondPage = "Page 2";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        System.out.println("Number of page text(1):" + adminClientsPage.getNumberOfPageText());
        adminClientsPage.clickOnNextPageButton();
        Assert.assertEquals(adminClientsPage.checkPresentsOfSecondPage(), secondPage,"User is not redirecting to second page!");
        System.out.println("Number of page text(2):" + adminClientsPage.getNumberOfPageText());
        Assert.assertTrue(adminClientsPage.getNumberOfPageText().contains(textOfSecondPage), "User is not redirecting to second page!");
        adminClientsPage.clickOnPreviousButton();
        System.out.println("Number of page text(1):" + adminClientsPage.getNumberOfPageText());
        Assert.assertTrue(adminClientsPage.getNumberOfPageText().contains(textOfFirstPage),"User is not redirecting to first Page!");
    }
    //Id
    //Test 1.959
    @Test
    public void checkIfUserWithOneFinishedAndOneNotFinishedOrderIsDisplayedByFilterFinishedOrders() throws InterruptedException {
        String nameOfOrder = "1 YEAR LICENSE x1\n" +
                "Not finished";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String unique = userRandomGenerator.generateRandomUser();
        final String uniqueFirstName = "aaaaa" + unique;
        final String fullName = uniqueFirstName + " " + uniqueFirstName;
        System.out.println("Unique username:" + uniqueFirstName);
        String email = "m@gmail.com";
        String phoneNumber = "0931308310";
        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        printDetailsBO.makeOtherSteps();
        printDetailsBO.makeNotFinishedOrder(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        adminClientsBO.goToClientsPage();
        adminClientsPage.writeIntoSearchInput(fullName);
        //Finished filter button
        adminClientsPage.clickOnFinishedOrdersButton();
        System.out.println("Name of order:" + adminClientsPage.getOrderNameOfFirstClient());
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminClientsPage.getOrderNameOfFirstClient(),nameOfOrder, "User with one finished and one not finished orders is not shown by FinishedOrders button!");
        System.out.println("Name of user:" + adminClientsPage.getNameOfFirstUser());
        softAssert.assertEquals(adminClientsPage.getNameOfFirstUser(),fullName,"User with one finished and one not finished orders is not displayed!");
        softAssert.assertAll();
        adminClientsPage.deleteFirstSelectedItem();
        adminClientsPage.clickOnYesDeleteClientButton();
    }
    //Id
    //Test 1.960
    @Test
    public void checkIfUserWithOneFinishedAndOneNotFinishedOrderIsDisplayedByFilterNotFinishedOrders() throws InterruptedException {
        String nameOfOrder = "1 YEAR LICENSE x1\n" +
                "Not finished";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String unique = userRandomGenerator.generateRandomUser();
        final String uniqueFirstName = "aaaaa" + unique;
        final String fullName = uniqueFirstName + " " + uniqueFirstName;
        System.out.println("Unique username:" + uniqueFirstName);
        String email = "m@gmail.com";
        String phoneNumber = "0931308310";
        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        printDetailsBO.makeOtherSteps();
        printDetailsBO.makeNotFinishedOrder(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        adminClientsBO.goToClientsPage();
        adminClientsPage.writeIntoSearchInput(fullName);
        //NotFinishedFilter button
        adminClientsPage.clickOnNotFinishedOrdersButton();
        System.out.println("Name of order:" + adminClientsPage.getOrderNameOfFirstClient());
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminClientsPage.getOrderNameOfFirstClient(),nameOfOrder, "User with one finished and one not finished orders is not shown by FinishedOrders button!");
        System.out.println("Name of user:" + adminClientsPage.getNameOfFirstUser());
        softAssert.assertEquals(adminClientsPage.getNameOfFirstUser(),fullName,"User with one finished and one not finished orders is not displayed!");
        softAssert.assertAll();
        adminClientsPage.deleteFirstSelectedItem();
        adminClientsPage.clickOnYesDeleteClientButton();
    }
    //Id
    //Test 1.961
    @Test
    public void checkIfUserWithOneFinshedAndOneNotFinishedOrderIsDisplayedByAllFilterButton() throws InterruptedException {
        String nameOfOrder = "1 YEAR LICENSE x1\n" +
                "Not finished";
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
        String unique = userRandomGenerator.generateRandomUser();
        final String uniqueFirstName = "aaaaa" + unique;
        final String fullName = uniqueFirstName + " " + uniqueFirstName;
        System.out.println("Unique username:" + uniqueFirstName);
        String email = "m@gmail.com";
        String phoneNumber = "0931308310";
        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        printDetailsBO.makeOtherSteps();
        printDetailsBO.makeNotFinishedOrder(uniqueFirstName,uniqueFirstName,email,phoneNumber);
        adminClientsBO.goToClientsPage();
        adminClientsPage.writeIntoSearchInput(fullName);
        //All filter button
        adminClientsPage.clickOnAllFilterButton();
        System.out.println("Name of order:" + adminClientsPage.getOrderNameOfFirstClient());
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(adminClientsPage.getOrderNameOfFirstClient(),nameOfOrder, "User with one finished and one not finished orders is not shown by FinishedOrders button!");
        System.out.println("Name of user:" + adminClientsPage.getNameOfFirstUser());
        softAssert.assertEquals(adminClientsPage.getNameOfFirstUser(),fullName,"User with one finished and one not finished orders is not displayed!");
        softAssert.assertAll();
        adminClientsPage.deleteFirstSelectedItem();
        adminClientsPage.clickOnYesDeleteClientButton();
    }
//    //Id
//    //Test 1.972
//    @Test
//    public void checkIfUserWithFinishedOrderCanBeDeleted() throws InterruptedException {
//        AdminClientsBO adminClientsBO = new AdminClientsBO();
//        AdminClientsPage adminClientsPage = new AdminClientsPage();
//        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
//        String unique = userRandomGenerator.generateRandomUser();
//        final String uniqueFirstName = "aaaaa" + unique;
//        final String fullName = uniqueFirstName + " " + uniqueFirstName;
//        System.out.println("Unique username:" + uniqueFirstName);
//        String email = "m@gmail.com";
//        String phoneNumber = "0931308310";
//        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
//        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        adminClientsBO.goToClientsPage();
//        adminClientsPage.writeIntoSearchInput(fullName);
//        //Deleting user with finished order
//        adminClientsPage.deleteFirstSelectedItem();
//        adminClientsPage.clickOnYesDeleteClientButton();
//        //Check that user is deleted
//        Thread.sleep(1000);
//        System.out.println("Name of user after deleting:" + adminClientsPage.getNameOfFirstUser());
//        Assert.assertTrue(adminClientsPage.getNameOfFirstUser() != fullName, "User with finished order is not deleted!");
//    }
//    //Id
//    //Test 1.973
//    @Test
//    public void checkIfUserWithNotFinishedOrderCanBeDeleted() throws InterruptedException {
//        AdminClientsBO adminClientsBO = new AdminClientsBO();
//        AdminClientsPage adminClientsPage = new AdminClientsPage();
//        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
//        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
//        String unique = userRandomGenerator.generateRandomUser();
//        final String uniqueFirstName = "aaaaa" + unique;
//        final String fullName = uniqueFirstName + " " + uniqueFirstName;
//        System.out.println("Unique username:" + uniqueFirstName);
//        String email = "m@gmail.com";
//        String phoneNumber = "0931308310";
//        printDetailsBO.makeNotFinishedOrder(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        adminClientsBO.goToClientsPage();
//        //Deleting user with not finished order
//        System.out.println("Name of user with not finished order:" + adminClientsPage.getNameOfFirstUser());
//        adminClientsPage.deleteFirstSelectedItem();
//        adminClientsPage.clickOnYesDeleteClientButton();
//        Thread.sleep(1000);
//        System.out.println("Name of user after deleting:" + adminClientsPage.getNameOfFirstUser());
//        Assert.assertTrue(adminClientsPage.getNameOfFirstUser() != fullName, "User with not finished order is not deleted!");
//    }
//    //Id
//    //Test 1.974
//    @Test
//    public void checkIfUserWithTwoSameOrdersCanBeDeleted() throws InterruptedException {
//        AdminClientsBO adminClientsBO = new AdminClientsBO();
//        AdminClientsPage adminClientsPage = new AdminClientsPage();
//        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
//        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
//        PrintDetailsPage printDetailsPage = new PrintDetailsPage();
//        String unique = userRandomGenerator.generateRandomUser();
//        final String uniqueFirstName = "aaaaa" + unique;
//        final String fullName = uniqueFirstName + " " + uniqueFirstName;
//        System.out.println("Unique username:" + uniqueFirstName);
//        String email = "m@gmail.com";
//        String phoneNumber = "0931308310";
//        //First order
//        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        //Second order with the same user and same order x2
//        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        //Admin panel
//        adminClientsBO.goToClientsPage();
//        //Delete user with two same orders
//        System.out.println("Name of user with x2 orders:" + adminClientsPage.getNameOfFirstUser());
//        adminClientsPage.deleteFirstSelectedItem();
//        adminClientsPage.clickOnYesDeleteClientButton();
//        //Check that user is deleted
//        Thread.sleep(1000);
//        System.out.println("Name of user after deleting:" + adminClientsPage.getNameOfFirstUser());
//        Assert.assertTrue(adminClientsPage.getNameOfFirstUser() != fullName, "User with x2 orders is not deleted!");
//    }
//    //Id
//    //Test 1.975
//    @Test
//    public void checkIfUserWitTwoDifferentOrdersCanBeDeleted() throws InterruptedException {
//        AdminClientsBO adminClientsBO = new AdminClientsBO();
//        AdminClientsPage adminClientsPage = new AdminClientsPage();
//        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
//        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
//        String unique = userRandomGenerator.generateRandomUser();
//        final String uniqueFirstName = "aaaaa" + unique;
//        final String fullName = uniqueFirstName + " " + uniqueFirstName;
//        System.out.println("Unique username:" + uniqueFirstName);
//        String email = "m" + unique + "@gmail.com";
//        String phoneNumber = "0931308310";
//        //Make order 1 Year License
//        printDetailsBO.makeAnUniqueUser(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        //Make order 3 Year License
//        printDetailsBO.makeAnUniqueUserWithThreeYearLicense(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        //Go to admin panel
//        adminClientsBO.goToClientsPage();
//        System.out.println("Orders of first user:" + adminClientsPage.getOrderNameOfFirstClient());
//        adminClientsPage.deleteFirstSelectedItem();
//        adminClientsPage.clickOnYesDeleteClientButton();
//        Thread.sleep(1000);
//        System.out.println("Name of user after deleting:" + adminClientsPage.getNameOfFirstUser());
//        Assert.assertTrue(adminClientsPage.getNameOfFirstUser() != fullName,"User with two different orders is not deleted!");
//    }
//    //Id
//    //Test 1.976
//    @Test
//    public void checkIfUserWithOneFinishedAndOneNotFinishedOrdersCanBeDeleted() throws InterruptedException {
//        AdminClientsBO adminClientsBO = new AdminClientsBO();
//        AdminClientsPage adminClientsPage = new AdminClientsPage();
//        UserRandomGenerator userRandomGenerator = new UserRandomGenerator();
//        PrintDetailsBO printDetailsBO = new PrintDetailsBO();
//        String unique = userRandomGenerator.generateRandomUser();
//        final String uniqueFirstName = "aaaaa" + unique;
//        final String fullName = uniqueFirstName + " " + uniqueFirstName;
//        System.out.println("Unique username:" + uniqueFirstName);
//        String email = "m" + unique + "@gmail.com";
//        String phoneNumber = "0931308310";
//        //Make finished 3 Year License order
//        printDetailsBO.makeAnUniqueUserWithThreeYearLicense(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        printDetailsBO.makeOtherSteps();
//        //Make one not finished order
//        printDetailsBO.makeNotFinishedOrder(uniqueFirstName,uniqueFirstName,email,phoneNumber);
//        //Go to admin panel
//        adminClientsBO.goToClientsPage();
//        System.out.println("Name of user with one finished and one not finished order:" + adminClientsPage.getNameOfFirstUser());
//        //Delete user with one finished and one not finished orders
//        adminClientsPage.deleteFirstSelectedItem();
//        adminClientsPage.clickOnYesDeleteClientButton();
//        //Check that user is deleted
//        Thread.sleep(1000);
//        System.out.println("Name of user after deleting:" + adminClientsPage.getNameOfFirstUser());
//        Assert.assertTrue(adminClientsPage.getNameOfFirstUser() != fullName,"User with one finished and one not finished order is not deleted!");
//    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserCanBeRedirectingToClientsPageFromDashboardAndTurnBack(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        //Presents of ClientsPage
        Assert.assertTrue(adminClientsPage.checkIfClientsPageIsPresent(),"User is not on clients page!");
        adminClientsPage.turnBackToPreviousPage();
        //Presents of DashboardPage
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not on dashboard page!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserCanBeRedirectedToDashboardPageByLogoButton(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not DashboardPage by clicking on logo!");
    }
    //Id
    //Test(in the bottom of documentation
    @Test
    public void checkIfUserCanBeRedirectedToEmailsPageAndTurnBack(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        adminClientsPage.clickOnEmailsButton();
        Assert.assertTrue(adminClientsPage.checkIfEmailsPageIsPresent(),"User is not redirected to EmailsPage!");
        adminClientsPage.turnBackToPreviousPage();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not turn back to DashboardPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserCanBeRedirectedToDashboardPageByClickingOnLogoButtonFromEmailsPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnEmailsButton();
        //Check if user is on EmailsPage
        Assert.assertTrue(adminClientsPage.checkIfEmailsPageIsPresent(),"User is not on EmailsPage!");
        adminClientsPage.clickOnLogoButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(), "User is not on DashboardPage after clicking on LogoButton on EmailsPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserCanBeRedirectedToSeoPageByAndTurnBackToDashboardPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        adminClientsPage.clickOnSeoMenuButton();
        Assert.assertTrue(adminClientsPage.checkIfSeoPageIsPresent(),"User is not on SeoPage after clicking on SeoMenuButton!");
        adminClientsPage.turnBackToPreviousPage();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is turned back to DashboardPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserIsRedirectedToDashboardPageFromSeoPageByLogoButton(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnSeoMenuButton();
        Assert.assertTrue(adminClientsPage.checkIfSeoPageIsPresent(),"User is not on SeoPage!");
        adminClientsPage.clickOnLogoButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User in not redirected to DashboardPage after clicking on LogoButton on SeoPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserIsRedirectedToUsersPageAndTurnBackToDashboardPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        adminClientsPage.clickOnUsersMenuButton();
        Assert.assertTrue(adminClientsPage.checkIfUsersPageIsPresent(), "User is not redirected to UsersPage!");
        adminClientsPage.turnBackToPreviousPage();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not turned back to DashboardPage from UsersPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
        public void checkIfUserIsRedirectedToDashboardPageFromUsersPageByLogoButton(){
            AdminClientsBO adminClientsBO = new AdminClientsBO();
            AdminClientsPage adminClientsPage = new AdminClientsPage();
            adminClientsBO.goToClientsPage();
            adminClientsPage.clickOnUsersMenuButton();
            Assert.assertTrue(adminClientsPage.checkIfUsersPageIsPresent(), "User is not redirected to UsersPage!");
            adminClientsPage.clickOnLogoButton();
            Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(), "User is not redirected to DashboardPage from EmailsPage by LogoButton!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserCanBeRedirectedToSettingsPageAndTurnBackToDashboardPage(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        adminClientsPage.clickOnSettingsMenuButton();
        Assert.assertTrue(adminClientsPage.checkIfSettingsPageIsPresent(),"User is not redirected from DashboardPage to SettingsPage!");
        adminClientsPage.turnBackToPreviousPage();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not turned back from SettingsPage to DashboardPage!");
    }
    //Id
    //Test(in the bottom of documentation)
    @Test
    public void checkIfUserIsRedirectedToDashboardPageFromSettingsPageByLogoButton(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        AdminClientsPage adminClientsPage = new AdminClientsPage();
        adminClientsBO.goToClientsPage();
        adminClientsPage.clickOnLogoButton();
        adminClientsPage.clickOnSettingsMenuButton();
        Assert.assertTrue(adminClientsPage.checkIfSettingsPageIsPresent(), "User is not on SettingsPage!");
        adminClientsPage.clickOnLogoButton();
        Assert.assertTrue(adminClientsPage.checkIfDashboardPageIsPresent(),"User is not redirected to DashboardPage from SettingsPage by LogoButton!");
    }
    @Test
    public void test(){
        AdminClientsBO adminClientsBO = new AdminClientsBO();
        adminClientsBO.goToClientsPage();
        Assert.assertTrue(true);
    }

    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }
}
