package com.art.idl.admintestsuites;

import com.art.idl.adminlpanelbussinessobjects.AdminPanelLoginBO;
import com.art.idl.adminpanelpages.AdminOrdersPage;
import com.art.idl.singleton.SingletonDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Created by M.Malyus on 11/28/2017.
 */
public class LoginTest {
    @Test
    public void goToAdminPanel(){
        AdminPanelLoginBO adminPanelLoginBO = new AdminPanelLoginBO();
        AdminOrdersPage adminOrdersPage = new AdminOrdersPage();
        adminPanelLoginBO.loginToAdminPanel();
    }
    @AfterMethod
    public void quit(){
        SingletonDriver.quit();
    }

}
